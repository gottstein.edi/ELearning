--ROLE
INSERT INTO role(name) VALUES("ROLE_STUDENT")
INSERT INTO role(name) VALUES("ROLE_NASTAVNIK")
INSERT INTO role(name) VALUES("ROLE_ADMIN")


--ZVANJA
INSERT INTO zvanje (naziv,obrisano) VALUES ('redovni profesor',0);
INSERT INTO zvanje (naziv,obrisano) VALUES ('vanredni profesor',0);
INSERT INTO zvanje (naziv,obrisano) VALUES ('docent',0);
INSERT INTO zvanje (naziv,obrisano) VALUES ('asistent',0);

--SMEROVI
INSERT INTO smer (broj_semestra,naziv,oznaka,tip,obrisano) VALUES (8,'Arhitektura','ARH','akademski',0);
INSERT INTO smer (broj_semestra,naziv,oznaka,tip,obrisano) VALUES (8,'Biomedicinsko inzinjerstvo','BI','akademski',0);
INSERT INTO smer (broj_semestra,naziv,oznaka,tip,obrisano) VALUES (8,'Elektronika i telekomunikacije','ET','akademski',0);
INSERT INTO smer (broj_semestra,naziv,oznaka,tip,obrisano) VALUES (6,'Gradjevinarstvo','GR','strukovni',0);
INSERT INTO smer (broj_semestra,naziv,oznaka,tip,obrisano) VALUES (6,'Softverske i informacione tehnologije','SIT','strukovni',0);
INSERT INTO smer (broj_semestra,naziv,oznaka,tip,obrisano) VALUES (8,'Inzenjerski menadzment','IM','akademski',0);
INSERT INTO smer (broj_semestra,naziv,oznaka,tip,obrisano) VALUES (8,'Mehatronika','MH','akademski',0);

--STUDENTI Sifra:student--
INSERT INTO user(dtype,email,ime,korisnicko_ime,prezime,sifra,jmbg,adresa_stanovanja,broj_indeksa,datum_rodjenja,drzavljanstvo,godina_studije,godina_upisa,smer_id,obrisano,role_id) VALUES ('Student','milanpetrovic@gmail.com','Milan','stud1','Petrovic','$2a$10$ntFq.z5hijOXzAOVQj98wudYzNPLnb.vD/zFLp9kbJeEqBmdq906S','1234567432456','Cvecarska 18', 'SF40/2012', '1992-11-01 00:00:00.000', 'Srpsko',3, 2013, 1, 0,1);
INSERT INTO user(dtype,email,ime,korisnicko_ime,prezime,sifra,jmbg,adresa_stanovanja,broj_indeksa,datum_rodjenja,drzavljanstvo,godina_studije,godina_upisa,smer_id,obrisano,role_id) VALUES ('Student','jovanajovanovic@gmail.com','Jovana','stud2','Jovanovic','$2a$10$ntFq.z5hijOXzAOVQj98wudYzNPLnb.vD/zFLp9kbJeEqBmdq906S','3234567432456','Uspenska 18', 'SF20/2012', '1993-07-01 00:00:00.000', 'Srpsko',4, 2013, 2, 0,1);

--NASTAVNICI Sifra:nastavnik--
INSERT INTO user(dtype,email,ime,korisnicko_ime,obrisano,prezime,sifra,zvanje_id,role_id) VALUES ('Nastavnik','milan.milanovic@ftn.com','Milan','nast1',0,'Milanovic','$2a$10$7yelKbRoE.mBgB70GX0PAus9POVJ1dIokyVRbU362jbu8WFbomBJO',1,2)
INSERT INTO user(dtype,email,ime,korisnicko_ime,obrisano,prezime,sifra,zvanje_id,role_id) VALUES ('Nastavnik','milan.mitrovic@ftn.com','Milan','nast2',0,'Mitrovic','$2a$10$7yelKbRoE.mBgB70GX0PAus9POVJ1dIokyVRbU362jbu8WFbomBJO',2,2)
INSERT INTO user(dtype,email,ime,korisnicko_ime,obrisano,prezime,sifra,zvanje_id,role_id) VALUES ('Nastavnik','jelena.jeftic@ftn.com','Jelena','nast3',0,'Jeftic','$2a$10$7yelKbRoE.mBgB70GX0PAus9POVJ1dIokyVRbU362jbu8WFbomBJO',3,2)
INSERT INTO user(dtype,email,ime,korisnicko_ime,obrisano,prezime,sifra,zvanje_id,role_id) VALUES ('Nastavnik','miljanmacvan@gmail.com','Miljan','nast4',0,'Macvan','$2a$10$7yelKbRoE.mBgB70GX0PAus9POVJ1dIokyVRbU362jbu8WFbomBJO',1,2)
INSERT INTO user(dtype,email,ime,korisnicko_ime,obrisano,prezime,sifra,zvanje_id,role_id) VALUES ('Nastavnik','sreta.stevanovic@ftn.com','Sreta','nast5',0,'Stevanovic','$2a$10$7yelKbRoE.mBgB70GX0PAus9POVJ1dIokyVRbU362jbu8WFbomBJO',4,2)

--ADMIN Sifra:admin--
INSERT Into user(dtype,email,ime,korisnicko_ime,obrisano,prezime,sifra,role_id) VALUES ('Admin','admin@fakultet.com','Sef Sluzbe','admin',0,'Glavni','$2a$10$I4W93IvTZh69RwHfQO06cuydjdsgJ43tcnYQU0SgbcrauhuKSlen.',3)
--DOKUMENTI
--INSERT INTO dokument(mime,filename,naziv,obrisano,student_id) VALUES ('application/pdf','izvo/files','maticniizvod',0,1);
--INSERT INTO dokument(mime,filename,naziv,obrisano,student_id) VALUES ('application/pdf','izvo/files1','diploma',0,1);
--INSERT INTO dokument(mime,filename,naziv,obrisano,student_id) VALUES ('application/pdf','izvo/files2','diplomaf',0,1);
--INSERT INTO dokument(mime,filename,naziv,obrisano,student_id) VALUES ('application/pdf','izvo/files3','diplomas',0,2);
--INSERT INTO dokument(mime,filename,naziv,obrisano,student_id) VALUES ('application/pdf','izvo/files4','diplomay',0,2);

--PREDISPITNE OBAVEZE
INSERT INTO predispitna_obaveza(broj_bodova,naziv,obrisano) VALUES (20,'kolokvijum',0);
INSERT INTO predispitna_obaveza(broj_bodova,naziv,obrisano) VALUES (15,'seminarski',0);
INSERT INTO predispitna_obaveza(broj_bodova,naziv,obrisano) VALUES (30,'projekat',0);
INSERT INTO predispitna_obaveza(broj_bodova,naziv,obrisano) VALUES (10,'seminarski',0);
INSERT INTO predispitna_obaveza(broj_bodova,naziv,obrisano) VALUES (30,'domaci zadaci',0);

--PREDMETI
INSERT INTO predmet(espb,naziv,obrisano,oznaka,predispitna_obaveza_id) VALUES (6,'Matematika 1',0,'MT1',1);
INSERT INTO predmet(espb,naziv,obrisano,oznaka,predispitna_obaveza_id) VALUES (6,'Matematika 2',0,'MT2',1);
INSERT INTO predmet(espb,naziv,obrisano,oznaka,predispitna_obaveza_id) VALUES (7,'Osnove programiranja',0,'OP',3);
INSERT INTO predmet(espb,naziv,obrisano,oznaka,predispitna_obaveza_id) VALUES (6,'Osnove sistemskih arhitektura',0,'OSA',null);
INSERT INTO predmet(espb,naziv,obrisano,oznaka,predispitna_obaveza_id) VALUES (6,'Programiranje mobilnih sistema i uredjaja',0,'PMSU',5);
INSERT INTO predmet(espb,naziv,obrisano,oznaka,predispitna_obaveza_id) VALUES (7,'E Obrazovanje',0,'EO',3);
INSERT INTO predmet(espb,naziv,obrisano,oznaka,predispitna_obaveza_id) VALUES (7,'Osnove Web programiranja',0,'OWP',3);
INSERT INTO predmet(espb,naziv,obrisano,oznaka,predispitna_obaveza_id) VALUES (6,'Poslovna informatika',0,'PI',3);
INSERT INTO predmet(espb,naziv,obrisano,oznaka,predispitna_obaveza_id) VALUES (2,'Engleski',0,'ENG',4);
INSERT INTO predmet(espb,naziv,obrisano,oznaka,predispitna_obaveza_id) VALUES (4,'Upravljanje projektima',0,'OP',5);
INSERT INTO predmet(espb,naziv,obrisano,oznaka,predispitna_obaveza_id) VALUES (5,'Web dizajn',0,'WD',3);
INSERT INTO predmet(espb,naziv,obrisano,oznaka,predispitna_obaveza_id) VALUES (5,'Osnove baze podataka',0,'OBP',null);


--ISPITI
INSERT INTO ispit(datum_odrzavanja,obrisano,rok_za_prijavu,predmet_id) VALUES ('2018-11-01',0,'2018-10-28',1);
INSERT INTO ispit(datum_odrzavanja,obrisano,rok_za_prijavu,predmet_id) VALUES ('2018-12-01',0,'2018-11-27',4);
INSERT INTO ispit(datum_odrzavanja,obrisano,rok_za_prijavu,predmet_id) VALUES ('2018-07-01',0,'2018-06-22',3);

--PRIJAVE
--INSERT INTO prijava(datum_prijave,obrisano,ispit_id,student_id) VALUES ('2018-10-20',0,1,1);
--INSERT INTO prijava(datum_prijave,obrisano,ispit_id,student_id) VALUES ('2018-11-20',0,2,1);

--UPLATE
INSERT INTO uplata(datum_uplate,iznos,svrha_uplate,obrisano,student_id) VALUES ('2011-03-14',5000,'dobrotvorna svrha',0,1);
INSERT INTO uplata(datum_uplate,iznos,svrha_uplate,obrisano,student_id) VALUES ('2011-03-13',3000,'dobrotvorna svrha',0,1);
INSERT INTO uplata(datum_uplate,iznos,svrha_uplate,obrisano,student_id) VALUES ('2011-03-12',2000,'dobrotvorna svrha',0,1);
INSERT INTO uplata(datum_uplate,iznos,svrha_uplate,obrisano,student_id) VALUES ('2011-03-15',1000,'dobrotvorna svrha',0,2);
INSERT INTO uplata(datum_uplate,iznos,svrha_uplate,obrisano,student_id) VALUES ('2011-02-15',5000,'dobrotvorna svrha',0,2);
INSERT INTO uplata(datum_uplate,iznos,svrha_uplate,obrisano,student_id) VALUES ('2011-04-15',2000,'dobrotvorna svrha',0,2);
INSERT INTO uplata(datum_uplate,iznos,svrha_uplate,obrisano,student_id) VALUES ('2012-03-15',1000,'dobrotvorna svrha',0,3);
INSERT INTO uplata(datum_uplate,iznos,svrha_uplate,obrisano,student_id) VALUES ('2014-03-15',1000,'dobrotvorna svrha',0,3);
INSERT INTO uplata(datum_uplate,iznos,svrha_uplate,obrisano,student_id) VALUES ('2012-03-15',1000,'dobrotvorna svrha',0,3);



--SMEROVI I PREDMETI
INSERT INTO smer_predmet(smer_id,predmet_id) VALUES (1,1)
INSERT INTO smer_predmet(smer_id,predmet_id) VALUES (1,2)

INSERT INTO smer_predmet(smer_id,predmet_id) VALUES (2,1)
INSERT INTO smer_predmet(smer_id,predmet_id) VALUES (2,2)
INSERT INTO smer_predmet(smer_id,predmet_id) VALUES (2,3)
INSERT INTO smer_predmet(smer_id,predmet_id) VALUES (2,4)
INSERT INTO smer_predmet(smer_id,predmet_id) VALUES (2,5)


--NASTAVNICI PREDAJU
INSERT INTO nastavnik_predmet(nastavnik_id,predmet_id) VALUES (3,1);
INSERT INTO nastavnik_predmet(nastavnik_id,predmet_id) VALUES (4,2);
INSERT INTO nastavnik_predmet(nastavnik_id,predmet_id) VALUES (5,3);
INSERT INTO nastavnik_predmet(nastavnik_id,predmet_id) VALUES (6,4);
INSERT INTO nastavnik_predmet(nastavnik_id,predmet_id) VALUES (7,5);
INSERT INTO nastavnik_predmet(nastavnik_id,predmet_id) VALUES (4,6);
INSERT INTO nastavnik_predmet(nastavnik_id,predmet_id) VALUES (3,6);



