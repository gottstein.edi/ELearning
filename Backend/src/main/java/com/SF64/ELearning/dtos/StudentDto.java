package com.SF64.ELearning.dtos;

import com.SF64.ELearning.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Date;


public class StudentDto {


    private long id;
    private String ime;
    private String prezime;
    private String korisnickoIme;
    private String email;
    private String role;
    private Date datumRodjenja;
    private String adresaStanovanja;
    private String drzavljanstvo;
    private String brojIndeksa;
    private int godinaUpisa;
    private int godinaStudije;
    private String JMBG;
    private long smer;
    private String nazivSmera;
    private String sifra;

    public StudentDto() {
    }

    public StudentDto(Student student) {
        this.id = student.getId();
        this.ime = student.getIme();
        this.prezime = student.getPrezime();
        this.korisnickoIme = student.getKorisnickoIme();
        this.email = student.getEmail();
        this.datumRodjenja = student.getDatumRodjenja();
        this.adresaStanovanja = student.getAdresaStanovanja();
        this.drzavljanstvo = student.getDrzavljanstvo();
        this.brojIndeksa = student.getBrojIndeksa();
        this.godinaUpisa = student.getGodinaUpisa();
        this.godinaStudije = student.getGodinaStudije();
        this.JMBG = student.getJMBG();


        this.role = student.getRole().getName();
        if(student.getSmer() != null){
            this.smer = student.getSmer().getId();
            this.nazivSmera = student.getSmer().getNaziv();
        }

    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDatumRodjenja() {
        return datumRodjenja;
    }

    public void setDatumRodjenja(Date datumRodjenja) {
        this.datumRodjenja = datumRodjenja;
    }

    public String getAdresaStanovanja() {
        return adresaStanovanja;
    }

    public void setAdresaStanovanja(String adresaStanovanja) {
        this.adresaStanovanja = adresaStanovanja;
    }

    public String getDrzavljanstvo() {
        return drzavljanstvo;
    }

    public void setDrzavljanstvo(String drzavljanstvo) {
        this.drzavljanstvo = drzavljanstvo;
    }

    public String getBrojIndeksa() {
        return brojIndeksa;
    }

    public void setBrojIndeksa(String brojIndeksa) {
        this.brojIndeksa = brojIndeksa;
    }

    public int getGodinaUpisa() {
        return godinaUpisa;
    }

    public void setGodinaUpisa(int godinaUpisa) {
        this.godinaUpisa = godinaUpisa;
    }

    public int getGodinaStudije() {
        return godinaStudije;
    }

    public void setGodinaStudije(int godinaStudije) {
        this.godinaStudije = godinaStudije;
    }

    public String getJMBG() {
        return JMBG;
    }

    public void setJMBG(String JMBG) {
        this.JMBG = JMBG;
    }

    public long getSmer() {
        return smer;
    }

    public void setSmer(long smer) {
        this.smer = smer;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getNazivSmera() {
        return nazivSmera;
    }

    public void setNazivSmera(String nazivSmera) {
        this.nazivSmera = nazivSmera;
    }

    public String getSifra() {
        return sifra;
    }

    public void setSifra(String sifra) {
        this.sifra = sifra;
    }
}
