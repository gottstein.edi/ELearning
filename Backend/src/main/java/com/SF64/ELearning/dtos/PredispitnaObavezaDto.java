package com.SF64.ELearning.dtos;

import com.SF64.ELearning.entity.PredispitnaObaveza;

public class PredispitnaObavezaDto {

    private long id;
    private int brojBodova;
    private String naziv;

    public PredispitnaObavezaDto() {

    }

    public PredispitnaObavezaDto(PredispitnaObaveza predispitnaObaveza){
        this.id = predispitnaObaveza.getId();
        this.brojBodova = predispitnaObaveza.getBrojBodova();
        this.naziv = predispitnaObaveza.getNaziv();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getBrojBodova() {
        return brojBodova;
    }

    public void setBrojBodova(int brojBodova) {
        this.brojBodova = brojBodova;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }
}
