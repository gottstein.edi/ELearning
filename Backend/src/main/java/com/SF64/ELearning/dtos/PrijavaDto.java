package com.SF64.ELearning.dtos;


import com.SF64.ELearning.entity.Prijava;

import java.util.Date;

public class PrijavaDto {

    private long id;
    private Date datumPrijave;
    private long ispit;
    private long student;
    private String brojIndeksa;
    private boolean ocenjen;

    public PrijavaDto(){

    }

    public PrijavaDto(Prijava prijava){
        this.id = prijava.getId();
        this.datumPrijave = prijava.getDatumPrijave();
        this.ispit = prijava.getIspit().getId();
        this.student = prijava.getStudent().getId();
        this.brojIndeksa = prijava.getStudent().getBrojIndeksa();
        this.ocenjen = prijava.isOcenjen();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDatumPrijave() {
        return datumPrijave;
    }

    public void setDatumPrijave(Date datumPrijave) {
        this.datumPrijave = datumPrijave;
    }

    public long getIspit() {
        return ispit;
    }

    public void setIspit(long ispit) {
        this.ispit = ispit;
    }

    public long getStudent() {
        return student;
    }

    public void setStudent(long student) {
        this.student = student;
    }

    public String getBrojIndeksa() {
        return brojIndeksa;
    }

    public void setBrojIndeksa(String brojIndeksa) {
        this.brojIndeksa = brojIndeksa;
    }

    public boolean isOcenjen() {
        return ocenjen;
    }

    public void setOcenjen(boolean ocenjen) {
        this.ocenjen = ocenjen;
    }
}
