package com.SF64.ELearning.dtos;

import com.SF64.ELearning.entity.Nastavnik;

public class NastavnikDto {

    private long id;
    private String ime;
    private String prezime;
    private String korisnickoIme;
    private String email;
    private String sifra;
    private String role;
    private Long zvanje;

    public NastavnikDto() {

    }

    public NastavnikDto(Nastavnik nastavnik) {
        this.id = nastavnik.getId();
        this.ime = nastavnik.getIme();
        this.prezime = nastavnik.getPrezime();
        this.korisnickoIme = nastavnik.getKorisnickoIme();
        this.email = nastavnik.getEmail();
        this.zvanje = nastavnik.getZvanje().getId();
        this.role = nastavnik.getRole().getName();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getZvanje() {
        return zvanje;
    }

    public void setZvanje(Long zvanje) {
        this.zvanje = zvanje;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getSifra() {
        return sifra;
    }

    public void setSifra(String sifra) {
        this.sifra = sifra;
    }
}
