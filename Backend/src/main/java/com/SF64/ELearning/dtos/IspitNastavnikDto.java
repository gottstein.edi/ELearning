package com.SF64.ELearning.dtos;

import java.util.Date;

public class IspitNastavnikDto {

    private long id;
    private Date datumOdrzavanja;
    private Date rokZaPrijavu;
    private String predmet;

    public IspitNastavnikDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDatumOdrzavanja() {
        return datumOdrzavanja;
    }

    public void setDatumOdrzavanja(Date datumOdrzavanja) {
        this.datumOdrzavanja = datumOdrzavanja;
    }

    public Date getRokZaPrijavu() {
        return rokZaPrijavu;
    }

    public void setRokZaPrijavu(Date rokZaPrijavu) {
        this.rokZaPrijavu = rokZaPrijavu;
    }

    public String getPredmet() {
        return predmet;
    }

    public void setPredmet(String predmet) {
        this.predmet = predmet;
    }
}
