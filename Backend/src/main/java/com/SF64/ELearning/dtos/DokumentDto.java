package com.SF64.ELearning.dtos;

import com.SF64.ELearning.entity.Dokument;

public class DokumentDto {

    private long id;
    private String naziv;
    private String MIME;
    private String filename;
    private long student;

    public DokumentDto(){}

    public DokumentDto(Dokument dokument){
        this.id = dokument.getId();
        this.naziv = dokument.getNaziv();
        this.MIME = dokument.getMIME();
        this.filename = dokument.getFilename();
        this.student = dokument.getStudent().getId();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getMIME() {
        return MIME;
    }

    public void setMIME(String MIME) {
        this.MIME = MIME;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public long getStudent() {
        return student;
    }

    public void setStudent(long student) {
        this.student = student;
    }
}
