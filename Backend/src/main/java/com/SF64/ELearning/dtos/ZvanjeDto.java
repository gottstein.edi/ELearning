package com.SF64.ELearning.dtos;

import com.SF64.ELearning.entity.Zvanje;

public class ZvanjeDto {

    private long id;
    private String naziv;

    public ZvanjeDto(){}

    public ZvanjeDto(Zvanje zvanje) {
        this.id = zvanje.getId();
        this.naziv = zvanje.getNaziv();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }
}
