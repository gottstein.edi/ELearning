package com.SF64.ELearning.dtos;

import java.util.List;

public class ListPostWrapper {

    private List<Integer> listItemIds;


    public List<Integer> getListItemIds() {
        return listItemIds;
    }

    public void setListItemIds(List<Integer> listItemIds) {
        this.listItemIds = listItemIds;
    }
}
