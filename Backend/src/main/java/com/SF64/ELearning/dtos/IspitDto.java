package com.SF64.ELearning.dtos;

import com.SF64.ELearning.entity.Ispit;

import java.util.Date;

public class IspitDto {

    private long id;
    private Date datumOdrzavanja;
    private Date rokZaPrijavu;
    private long predmet;

    public IspitDto(){

    }

    public IspitDto(Ispit ispit){
        this.id = ispit.getId();
        this.datumOdrzavanja = ispit.getDatumOdrzavanja();
        this.rokZaPrijavu = ispit.getRokZaPrijavu();
        this.predmet = ispit.getPredmet().getId();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDatumOdrzavanja() {
        return datumOdrzavanja;
    }

    public void setDatumOdrzavanja(Date datumOdrzavanja) {
        this.datumOdrzavanja = datumOdrzavanja;
    }

    public Date getRokZaPrijavu() {
        return rokZaPrijavu;
    }

    public void setRokZaPrijavu(Date rokZaPrijavu) {
        this.rokZaPrijavu = rokZaPrijavu;
    }

    public long getPredmet() {
        return predmet;
    }

    public void setPredmet(long predmet) {
        this.predmet = predmet;
    }
}
