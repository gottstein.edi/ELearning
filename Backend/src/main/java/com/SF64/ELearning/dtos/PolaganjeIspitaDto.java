package com.SF64.ELearning.dtos;

import com.SF64.ELearning.entity.PolaganjeIspita;

import java.util.Date;

public class PolaganjeIspitaDto {

    private long id;
    private float predispitniBodovi;
    private float ispitniBodovi;
    private float ukupniBodovi;
    private int ocena;
    private boolean polozio;
    private long prijava;
    private long student;
    private long predmet;
    private Date rokOdrzavanja;

    public PolaganjeIspitaDto() {}

    public PolaganjeIspitaDto(PolaganjeIspita polaganjeIspita) {
        this.id = polaganjeIspita.getId();
        this.predispitniBodovi = polaganjeIspita.getPredispitniBodovi();
        this.ispitniBodovi = polaganjeIspita.getIspitniBodovi();
        this.ukupniBodovi = polaganjeIspita.getUkupniBodovi();
        this.ocena = polaganjeIspita.getOcena();
        this.polozio = polaganjeIspita.isPolozio();
        this.prijava = polaganjeIspita.getPrijava().getId();
        this.rokOdrzavanja = polaganjeIspita.getPrijava().getIspit().getDatumOdrzavanja();
    }

    public long getStudent() {
        return student;
    }

    public Date getRokOdrzavanja() {
        return rokOdrzavanja;
    }

    public void setRokOdrzavanja(Date rokOdrzavanja) {
        this.rokOdrzavanja = rokOdrzavanja;
    }

    public void setStudent(long student) {
        this.student = student;
    }

    public long getPredmet() {
        return predmet;
    }

    public void setPredmet(long predmet) {
        this.predmet = predmet;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public float getPredispitniBodovi() {
        return predispitniBodovi;
    }

    public void setPredispitniBodovi(float predispitniBodovi) {
        this.predispitniBodovi = predispitniBodovi;
    }

    public float getIspitniBodovi() {
        return ispitniBodovi;
    }

    public void setIspitniBodovi(float ispitniBodovi) {
        this.ispitniBodovi = ispitniBodovi;
    }

    public float getUkupniBodovi() {
        return ukupniBodovi;
    }

    public void setUkupniBodovi(float ukupniBodovi) {
        this.ukupniBodovi = ukupniBodovi;
    }

    public int getOcena() {
        return ocena;
    }

    public void setOcena(int ocena) {
        this.ocena = ocena;
    }

    public boolean isPolozio() {
        return polozio;
    }

    public void setPolozio(boolean polozio) {
        this.polozio = polozio;
    }


    public long getPrijava() {
        return prijava;
    }

    public void setPrijava(long prijava) {
        this.prijava = prijava;
    }
}
