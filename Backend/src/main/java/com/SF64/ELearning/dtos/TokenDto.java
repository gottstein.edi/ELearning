package com.SF64.ELearning.dtos;

public class TokenDto {

    public TokenDto() {
        super();
        // TODO Auto-generated constructor stub
    }

    public TokenDto(String token) {
        super();
        this.token = token;
    }

    String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
