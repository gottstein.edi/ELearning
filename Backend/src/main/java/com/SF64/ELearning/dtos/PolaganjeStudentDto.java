package com.SF64.ELearning.dtos;

public class PolaganjeStudentDto {

    private float brojPredispitnihBodova;
    private float brojIspitnihBodova;
    private float ukupanBrojBodova;
    private int ocena;
    private String predmet;


    public PolaganjeStudentDto() {
    }

    public float getBrojPredispitnihBodova() {
        return brojPredispitnihBodova;
    }

    public void setBrojPredispitnihBodova(float brojPredispitnihBodova) {
        this.brojPredispitnihBodova = brojPredispitnihBodova;
    }

    public float getBrojIspitnihBodova() {
        return brojIspitnihBodova;
    }

    public void setBrojIspitnihBodova(float brojIspitnihBodova) {
        this.brojIspitnihBodova = brojIspitnihBodova;
    }

    public float getUkupanBrojBodova() {
        return ukupanBrojBodova;
    }

    public void setUkupanBrojBodova(float ukupanBrojBodova) {
        this.ukupanBrojBodova = ukupanBrojBodova;
    }

    public int getOcena() {
        return ocena;
    }

    public void setOcena(int ocena) {
        this.ocena = ocena;
    }

    public String getPredmet() {
        return predmet;
    }

    public void setPredmet(String predmet) {
        this.predmet = predmet;
    }
}
