package com.SF64.ELearning.dtos;

import com.SF64.ELearning.entity.Uplata;

import java.util.Date;

public class UplataDto {

    private long id;
    private Date datumUplate;
    private String svrhaUplate;
    private int iznos;
    private long student;

    public UplataDto(){}

    public UplataDto(Uplata uplata){
        this.id = uplata.getId();
        this.datumUplate = uplata.getDatumUplate();
        this.iznos = uplata.getIznos();
        this.student = uplata.getStudent().getId();
        this.svrhaUplate = uplata.getSvrhaUplate();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDatumUplate() {
        return datumUplate;
    }

    public void setDatumUplate(Date datumUplate) {
        this.datumUplate = datumUplate;
    }

    public int getIznos() {
        return iznos;
    }

    public void setIznos(int iznos) {
        this.iznos = iznos;
    }

    public long getStudent() {
        return student;
    }

    public void setStudent(long student) {
        this.student = student;
    }

    public String getSvrhaUplate() {
        return svrhaUplate;
    }

    public void setSvrhaUplate(String svrhaUplate) {
        this.svrhaUplate = svrhaUplate;
    }
}
