package com.SF64.ELearning.dtos;

import com.SF64.ELearning.entity.PredispitnaObaveza;
import com.SF64.ELearning.entity.Predmet;

import javax.validation.constraints.NotNull;

public class PredmetDto {

    private long id;
    @NotNull
    private String naziv;
    @NotNull
    private String oznaka;
    @NotNull
    private int ESPB;

    private long predispitnaObaveza;

    public PredmetDto(){}

    public PredmetDto(Predmet predmet) {
        this.id = predmet.getId();
        this.naziv = predmet.getNaziv();
        this.oznaka = predmet.getOznaka();
        this.ESPB = predmet.getESPB();
        if(predmet.getPredispitnaObaveza() == null){
            this.predispitnaObaveza = 0;
        } else {
            this.predispitnaObaveza = predmet.getPredispitnaObaveza().getId();
        }


    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOznaka() {
        return oznaka;
    }

    public void setOznaka(String oznaka) {
        this.oznaka = oznaka;
    }

    public int getESPB() {
        return ESPB;
    }

    public void setESPB(int ESPB) {
        this.ESPB = ESPB;
    }

    public long getPredispitnaObaveza() {
        return predispitnaObaveza;
    }

    public void setPredispitnaObaveza(long predispitnaObaveza) {
        this.predispitnaObaveza = predispitnaObaveza;
    }
}
