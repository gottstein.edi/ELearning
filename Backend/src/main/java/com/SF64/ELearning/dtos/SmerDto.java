package com.SF64.ELearning.dtos;

import com.SF64.ELearning.entity.Smer;

public class SmerDto {

    private long id;
    private String naziv;
    private String oznaka;
    private String tip;
    private int brojSemestra;

    public SmerDto(){}

    public SmerDto(Smer smer){
        this.id = smer.getId();
        this.naziv = smer.getNaziv();
        this.tip = smer.getTip();
        this.oznaka = smer.getOznaka();
        this.brojSemestra = smer.getBrojSemestra();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOznaka() {
        return oznaka;
    }

    public void setOznaka(String oznaka) {
        this.oznaka = oznaka;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public int getBrojSemestra() {
        return brojSemestra;
    }

    public void setBrojSemestra(int brojSemestra) {
        this.brojSemestra = brojSemestra;
    }
}
