package com.SF64.ELearning.dtos;

import com.SF64.ELearning.entity.User;

public class UserDto {

    private long id;
    private String ime;
    private String prezime;
    private String korisnickoIme;
    private String email;
    private String role;

    public UserDto() {
    }

    public UserDto(User user) {
        this.id = user.getId();
        this.ime = user.getIme();
        this.prezime = user.getPrezime();
        this.korisnickoIme = user.getKorisnickoIme();
        this.email = user.getEmail();
        this.role = user.getRole().getName();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
