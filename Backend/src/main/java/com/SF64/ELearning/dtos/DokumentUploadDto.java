package com.SF64.ELearning.dtos;

import javax.validation.constraints.NotNull;

public class DokumentUploadDto {

    @NotNull
    private String naziv;
    @NotNull
    private long student;

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public long getStudent() {
        return student;
    }

    public void setStudent(long student) {
        this.student = student;
    }
}
