package com.SF64.ELearning.services;

import com.SF64.ELearning.entity.Student;
import com.SF64.ELearning.repository.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {

    @Autowired
    StudentRepo studentRepo;

    public List<Student> findAll() {
        return studentRepo.findAllByObrisano(false);
    }

    public List<Student> findAllSmerNull() { return studentRepo.findBySmerIsNullAndObrisano(false);}

    public Student findOne(Long id) {
        return studentRepo.findByObrisanoAndId(false,id);
    }

    public Student save(Student student) {
        studentRepo.save(student);
        return student;
    }

    public Boolean delete(Long id) {
        Student student = studentRepo.findOne(id);
        student.setObrisano(true);
        studentRepo.saveAndFlush(student);
        return true;
    }
}
