package com.SF64.ELearning.services;

import com.SF64.ELearning.entity.PredispitnaObaveza;
import com.SF64.ELearning.repository.PredispitnaObavezaRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PredispitnaObavezaService{

    @Autowired
    PredispitnaObavezaRepo predispitnaObavezaRepo;

    public List<PredispitnaObaveza> findAll() {
        return predispitnaObavezaRepo.findAllByObrisano(false);
    }

    public PredispitnaObaveza findOne(Long id) {
        return predispitnaObavezaRepo.findByObrisanoAndId(false,id);
    }

    public PredispitnaObaveza save(PredispitnaObaveza predispitnaObaveza) {
        predispitnaObavezaRepo.save(predispitnaObaveza);
        return predispitnaObaveza;
    }

    public Boolean delete(Long id) {
        PredispitnaObaveza predispitnaObaveza = predispitnaObavezaRepo.findOne(id);
        predispitnaObaveza.setObrisano(true);
        predispitnaObavezaRepo.saveAndFlush(predispitnaObaveza);
        return true;
    }
}
