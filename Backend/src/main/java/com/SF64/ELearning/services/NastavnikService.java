package com.SF64.ELearning.services;

import com.SF64.ELearning.entity.Nastavnik;
import com.SF64.ELearning.repository.NastavnikRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class NastavnikService {

    @Autowired
    NastavnikRepo nastavnikRepo;

    public List<Nastavnik> findAll() {
        return nastavnikRepo.findAllByObrisano(false);
    }

    public Nastavnik findOne(Long id) {
        return nastavnikRepo.findByObrisanoAndId(false,id);
    }

    public Nastavnik save(Nastavnik nastavnik) {
        nastavnikRepo.save(nastavnik);
        return nastavnik;
    }

    public Boolean delete(Long id) {
        Nastavnik nastavnik = nastavnikRepo.findOne(id);
        nastavnik.setObrisano(true);
        nastavnikRepo.saveAndFlush(nastavnik);
        return true;
    }

    public List<Nastavnik> findAllNotOnPredmet(List<Nastavnik> nastavniciNaPredmetu){
        List<Long> longs = nastavniciNaPredmetu.stream().map(nastavnik -> nastavnik.getId()).collect(Collectors.toList());
        return nastavnikRepo.findAllByIdNotInAndObrisano(longs, false);
    }


}
