package com.SF64.ELearning.services;

import com.SF64.ELearning.entity.PolaganjeIspita;
import com.SF64.ELearning.repository.PolaganjeIspitaRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PolaganjeIspitaService {

    @Autowired
    PolaganjeIspitaRepo polaganjeIspitaRepo;

    public List<PolaganjeIspita> findAll() {
        return polaganjeIspitaRepo.findAllByObrisano(false);
    }

    public PolaganjeIspita findOne(Long id) {
        return polaganjeIspitaRepo.findByObrisanoAndId(false,id);
    }

    public PolaganjeIspita save(PolaganjeIspita polaganjeIspita) {
        polaganjeIspitaRepo.save(polaganjeIspita);
        return polaganjeIspita;
    }

    public Boolean delete(Long id) {
        PolaganjeIspita polaganjeIspita = polaganjeIspitaRepo.findOne(id);
        polaganjeIspita.setObrisano(true);
        polaganjeIspitaRepo.saveAndFlush(polaganjeIspita);
        return true;
    }

    public List<PolaganjeIspita> findStudentPolozeniOrNepolozeniIspiti(Long id, boolean polozio) {
        return polaganjeIspitaRepo.findAllByStudent_idAndPolozio(id, polozio);
    }

    public PolaganjeIspita findByStudentAndPredmet(Long studentId,Long predmetId) {
        return polaganjeIspitaRepo.findByStudent_idAndPredmet_idAndPolozio(studentId,predmetId,true);
    }
}
