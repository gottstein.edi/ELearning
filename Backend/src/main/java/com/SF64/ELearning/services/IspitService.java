package com.SF64.ELearning.services;

import com.SF64.ELearning.entity.Ispit;
import com.SF64.ELearning.repository.IspitRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IspitService {

    @Autowired
    IspitRepo ispitRepo;

    public Page<Ispit> findAll(int brojStranice, int brojPrikazanih) {
        return ispitRepo.findAllByObrisano(false, new PageRequest(brojStranice,brojPrikazanih));
    }

    public Ispit findOne(Long id) {
        return ispitRepo.findByObrisanoAndId(false,id);
    }

    public Ispit save(Ispit ispit) {
        ispitRepo.save(ispit);
        return ispit;
    }

    public Boolean delete(Long id) {
        Ispit ispit = ispitRepo.findOne(id);
        ispit.setObrisano(true);
        ispitRepo.saveAndFlush(ispit);
        return true;
    }
}
