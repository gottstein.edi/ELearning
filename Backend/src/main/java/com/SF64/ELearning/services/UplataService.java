package com.SF64.ELearning.services;

import com.SF64.ELearning.entity.Uplata;
import com.SF64.ELearning.repository.UplataRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UplataService {

    @Autowired
    UplataRepo uplataRepo;

    public List<Uplata> findAll() {
        return uplataRepo.findAllByObrisano(false);
    }

    public Page<Uplata> findAllByStudent(Long id, Pageable pageable) {
        return uplataRepo.findAllByStudent_idAndObrisano(pageable, id,false);
    }

    public Uplata findOne(Long id) {
        return uplataRepo.findByObrisanoAndId(false,id);
    }

    public Uplata save(Uplata uplata) {
        uplataRepo.save(uplata);
        return uplata;
    }

    public Boolean delete(Long id) {
        Uplata uplata = uplataRepo.findOne(id);
        uplata.setObrisano(true);
        uplataRepo.saveAndFlush(uplata);
        return true;
    }
}
