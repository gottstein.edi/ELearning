package com.SF64.ELearning.services;


import com.SF64.ELearning.entity.Role;
import com.SF64.ELearning.repository.RoleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {

    @Autowired
    RoleRepo roleRepo;

    public Role findOne(Long id){
        return roleRepo.findOne(id);
    }
}
