package com.SF64.ELearning.services;

import com.SF64.ELearning.dtos.SifraDto;
import com.SF64.ELearning.entity.Role;
import com.SF64.ELearning.entity.User;
import com.SF64.ELearning.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    @Autowired
    UserRepo userRepo;

    public Page<User> findAll(String ime, Pageable pageable) {
        return userRepo.findAllByImeIgnoreCaseContainsAndObrisano(ime,pageable, false);
    }

    public User findOne(Long id) {
        return userRepo.findByObrisanoAndId(false, id);
    }

    public User save(User user){
        userRepo.save(user);
        return user;
    }


    public Boolean delete(Long id) {
        User user = userRepo.findOne(id);
        user.setObrisano(true);
        userRepo.saveAndFlush(user);
        return true;
    }

    public ResponseEntity updatePassword(User user, SifraDto sifraDto) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        if(!(bCryptPasswordEncoder.matches(sifraDto.getOldPassword(), user.getPassword())))
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        user.setSifra(bCryptPasswordEncoder.encode(sifraDto.getNewPassword()));
        save(user);
        return new ResponseEntity(HttpStatus.OK);
    }
}
