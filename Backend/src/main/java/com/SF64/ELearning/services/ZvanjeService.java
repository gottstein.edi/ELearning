package com.SF64.ELearning.services;

import com.SF64.ELearning.entity.Zvanje;
import com.SF64.ELearning.repository.ZvanjeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ZvanjeService {

    @Autowired
    ZvanjeRepo zvanjeRepo;

    public List<Zvanje> findAll() {
        return zvanjeRepo.findAllByObrisano(false);
    }

    public Zvanje findOne(Long id) {
        return zvanjeRepo.findByObrisanoAndId(false,id);
    }

    public Zvanje save(Zvanje zvanje) {
        zvanjeRepo.save(zvanje);
        return zvanje;
    }

    public Boolean delete(Long id) {
        Zvanje zvanje = zvanjeRepo.findOne(id);
        zvanje.setObrisano(true);
        zvanjeRepo.saveAndFlush(zvanje);
        return true;
    }
}
