package com.SF64.ELearning.services;

import com.SF64.ELearning.entity.Dokument;
import com.SF64.ELearning.repository.DokumentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DokumentService {

    @Autowired
    DokumentRepo dokumentRepo;

    public List<Dokument> findAll() {
        return dokumentRepo.findAllByObrisano(false);
    }

    public Dokument findOne(Long id) {
        return dokumentRepo.findByObrisanoAndId(false,id);
    }

    public List<Dokument> findAllByStudent(Long id) { return dokumentRepo.findAllByStudent_idAndObrisano(id, false);}

    public Dokument save(Dokument dokument) {
        dokumentRepo.save(dokument);
        return dokument;

    }

    public Boolean delete(Long id) {
        Dokument dokument = dokumentRepo.findOne(id);
        dokument.setObrisano(true);
        dokumentRepo.saveAndFlush(dokument);
        return true;
    }
}
