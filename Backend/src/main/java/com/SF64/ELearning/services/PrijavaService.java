package com.SF64.ELearning.services;

import com.SF64.ELearning.entity.Prijava;
import com.SF64.ELearning.repository.PrijavaRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PrijavaService {

    @Autowired
    PrijavaRepo prijavaRepo;

    public List<Prijava> findAll() {
        return prijavaRepo.findAllByObrisano(false);
    }

    public Prijava findOne(Long id) {
        return prijavaRepo.findByObrisanoAndId(false,id);
    }

    public List<Prijava> findAllByStudent(Long id) {
        return prijavaRepo.findAllByStudent_idAndObrisano(id,false);
    }

    public Prijava findByStudentAndIspit(Long studentId, Long ispitId) {
        return prijavaRepo.findByStudent_idAndIspit_id(studentId, ispitId);
    }

    public Prijava save(Prijava prijava) {
        prijavaRepo.save(prijava);
        return prijava;
    }

    public Boolean delete(Long id) {
        Prijava prijava = prijavaRepo.findOne(id);
        prijava.setObrisano(true);
        prijavaRepo.saveAndFlush(prijava);
        return true;
    }
}
