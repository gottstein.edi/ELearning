package com.SF64.ELearning.services;

import com.SF64.ELearning.entity.Predmet;
import com.SF64.ELearning.repository.PredmetRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class PredmetService{

    @Autowired
    PredmetRepo predmetRepo;

    public Page<Predmet> findAll(String naziv, Pageable pageable) {
        return predmetRepo.findAllByNazivIgnoreCaseContainsAndObrisano(naziv,pageable,false);
    }

    public Predmet findOne(Long id) {
        return predmetRepo.findByObrisanoAndId(false,id);
    }

    public Predmet save(Predmet predmet) {
        predmetRepo.save(predmet);
        return predmet;
    }

    public Boolean delete(Long id) {
        Predmet predmet = predmetRepo.findOne(id);
        predmet.setObrisano(true);
        predmetRepo.saveAndFlush(predmet);
        return true;
    }

    //Nalazi sve predmete koji nisu na smeru
    public List<Predmet> findAllNotInSmer(List<Predmet> predmetiNaSmeru) {
        List<Long> longs = predmetiNaSmeru.stream().map(predmet -> predmet.getId()).collect(Collectors.toList());
        return predmetRepo.findAllByIdNotInAndObrisano(longs, false);
    }



}
