package com.SF64.ELearning.services;

import com.SF64.ELearning.entity.User;
import com.SF64.ELearning.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    UserRepo userRepo;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepo.findByKorisnickoIme(s);
        if(user == null){
            throw new UsernameNotFoundException(String.format("No user found with username '%s' ", s));
        }
        return user;
    }
}
