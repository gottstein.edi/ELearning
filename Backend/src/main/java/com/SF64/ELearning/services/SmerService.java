package com.SF64.ELearning.services;

import com.SF64.ELearning.entity.Smer;
import com.SF64.ELearning.repository.SmerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SmerService {

    @Autowired
    SmerRepo smerRepo;

    public Page<Smer> findAll(String naziv, Pageable pageable) {
        return smerRepo.findAllByNazivIgnoreCaseContainsAndObrisano(naziv,pageable,false);
    }

    public Smer findOne(Long id) {
        return smerRepo.findByObrisanoAndId(false,id);
    }

    public Smer save(Smer smer) {
        smerRepo.save(smer);
        return smer;
    }

    public Boolean delete(Long id) {
        Smer smer = smerRepo.findOne(id);
        smer.setObrisano(true);
        smerRepo.saveAndFlush(smer);
        return true;
    }
}
