package com.SF64.ELearning.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Zvanje {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Size(min = 5, max = 30)
    private String naziv;

    @NotNull
    private boolean obrisano;

    @OneToMany(mappedBy = "zvanje", cascade = CascadeType.ALL)
    private Set<Nastavnik> nastavnici = new HashSet<>();

    public Zvanje() {
    }

    public long getId() {
        return id;
    }

    public String getNaziv() {
        return naziv;
    }

    public Set<Nastavnik> getNastavnici() {
        return nastavnici;
    }

    public boolean isObrisano() {
        return obrisano;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public void setNastavnici(Set<Nastavnik> nastavnici) {
        this.nastavnici = nastavnici;
    }

    public void setObrisano(boolean obrisano) {
        this.obrisano = obrisano;
    }
}
