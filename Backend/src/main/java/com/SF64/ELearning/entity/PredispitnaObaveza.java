package com.SF64.ELearning.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class PredispitnaObaveza {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private int brojBodova;

    @NotNull
    @Size(min = 3, max = 20)
    private String naziv;

    @NotNull
    private boolean obrisano;

    public PredispitnaObaveza() {
    }

    public long getId() {
        return id;
    }

    public int getBrojBodova() {
        return brojBodova;
    }

    public String getNaziv() {
        return naziv;
    }

    public boolean isObrisano() {
        return obrisano;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setBrojBodova(int brojBodova) {
        this.brojBodova = brojBodova;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public void setObrisano(boolean obrisano) {
        this.obrisano = obrisano;
    }
}
