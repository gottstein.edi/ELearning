package com.SF64.ELearning.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Dokument {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Size(min = 1, max = 50)
    private String naziv;

    @NotNull
    @Size(min = 1, max = 100)
    private String MIME;

    @NotNull
    @Size(min = 1, max = 200)
    @Column(unique = true)
    private String filename;

    @NotNull
    private boolean obrisano;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "student_id")
    private Student student;



    public Dokument() {
    }

    public long getId() {
        return id;
    }

    public String getNaziv() {
        return naziv;
    }

    public String getMIME() {
        return MIME;
    }

    public String getFilename() {
        return filename;
    }

    public Student getStudent() {
        return student;
    }

    public boolean isObrisano() {
        return obrisano;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public void setMIME(String MIME) {
        this.MIME = MIME;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public void setObrisano(boolean obrisano) {
        this.obrisano = obrisano;
    }
}
