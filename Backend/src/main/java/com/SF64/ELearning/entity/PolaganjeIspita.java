package com.SF64.ELearning.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class PolaganjeIspita {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private float predispitniBodovi;

    @NotNull
    private float ispitniBodovi;

    @NotNull
    private float ukupniBodovi;

    @NotNull
    private int ocena;

    @NotNull
    private boolean polozio;

    @NotNull
    private boolean obrisano;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "student_id")
    private Student student;

    @ManyToOne
    @JoinColumn(name = "predmet_id")
    private Predmet predmet;

    @OneToOne
    @JoinColumn(name = "prijava_id")
    private Prijava prijava;

    public PolaganjeIspita() {
    }

    public long getId() {
        return id;
    }

    public float getPredispitniBodovi() {
        return predispitniBodovi;
    }

    public float getIspitniBodovi() {
        return ispitniBodovi;
    }

    public float getUkupniBodovi() {
        return ukupniBodovi;
    }

    public int getOcena() {
        return ocena;
    }

    public boolean isPolozio() {
        return polozio;
    }

    public Prijava getPrijava() {
        return prijava;
    }

    public boolean isObrisano() {
        return obrisano;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setPredispitniBodovi(float predispitniBodovi) {
        this.predispitniBodovi = predispitniBodovi;
    }

    public void setIspitniBodovi(float ispitniBodovi) {
        this.ispitniBodovi = ispitniBodovi;
    }

    public void setUkupniBodovi(float ukupniBodovi) {
        this.ukupniBodovi = ukupniBodovi;
    }

    public void setOcena(int ocena) {
        this.ocena = ocena;
    }

    public void setPolozio(boolean polozio) {
        this.polozio = polozio;
    }

    public void setPrijava(Prijava prijava) {
        this.prijava = prijava;
    }

    public void setObrisano(boolean obrisano) {
        this.obrisano = obrisano;
    }



    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Predmet getPredmet() {
        return predmet;
    }

    public void setPredmet(Predmet predmet) {
        this.predmet = predmet;
    }
}
