package com.SF64.ELearning.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Smer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Size(min = 5, max = 50)
    private String naziv;

    @NotNull
    @Size(min = 1, max = 10)
    private String oznaka;

    @NotNull
    @Size(min = 1, max = 20)
    private String tip;

    @NotNull
    private int brojSemestra;

    @NotNull
    private boolean obrisano;

    @OneToMany(mappedBy = "smer", cascade = CascadeType.ALL)
    private Set<Student> studenti = new HashSet<>();

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "Smer_Predmet",
            joinColumns = @JoinColumn(name = "smer_id", referencedColumnName="id"),
            inverseJoinColumns = @JoinColumn(name = "predmet_id", referencedColumnName = "id"))
    private Set<Predmet> predmeti = new HashSet<>();

    public Smer() {
    }

    public long getId() {
        return id;
    }

    public String getNaziv() {
        return naziv;
    }

    public String getOznaka() {
        return oznaka;
    }

    public String getTip() {
        return tip;
    }

    public int getBrojSemestra() {
        return brojSemestra;
    }

    public Set<Student> getStudenti() {
        return studenti;
    }

    public boolean isObrisano() {
        return obrisano;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public void setOznaka(String oznaka) {
        this.oznaka = oznaka;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public void setBrojSemestra(int brojSemestra) {
        this.brojSemestra = brojSemestra;
    }

    public void setStudenti(Set<Student> studenti) {
        this.studenti = studenti;
    }

    public void setObrisano(boolean obrisano) {
        this.obrisano = obrisano;
    }

    public Set<Predmet> getPredmeti() {
        return predmeti;
    }

    public void setPredmeti(Set<Predmet> predmeti) {
        this.predmeti = predmeti;
    }
}
