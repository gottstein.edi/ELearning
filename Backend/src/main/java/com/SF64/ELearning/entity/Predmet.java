package com.SF64.ELearning.entity;

import com.sun.istack.internal.Nullable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Predmet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Size(min = 5, max = 50)
    private String naziv;

    @NotNull
    @Size(min = 1, max = 20)
    private String oznaka;

    @NotNull
    private int ESPB;

    @ManyToMany(mappedBy = "predmeti")
    private Set<Nastavnik> nastavnici = new HashSet<>();

    @ManyToMany(mappedBy = "predmeti")
    private Set<Smer> smerovi = new HashSet<>();

    @Nullable
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "predispitna_obaveza_id")
    private PredispitnaObaveza predispitnaObaveza;

    @NotNull
    private boolean obrisano;

    @OneToMany(mappedBy = "predmet")
    private Set<PolaganjeIspita> polaganja = new HashSet<>();

    @OneToMany(mappedBy = "predmet")
    private Set<Ispit> ispiti = new HashSet<>();

    public Predmet() {
    }

    public long getId() {
        return id;
    }

    public String getNaziv() {
        return naziv;
    }

    public String getOznaka() {
        return oznaka;
    }

    public int getESPB() {
        return ESPB;
    }

    public Set<Nastavnik> getNastavnici() {
        return nastavnici;
    }

    public Set<Smer> getSmerovi() {
        return smerovi;
    }

    public PredispitnaObaveza getPredispitnaObaveza() {
        return predispitnaObaveza;
    }

    public Set<Ispit> getIspiti() {
        return ispiti;
    }

    public boolean isObrisano() {
        return obrisano;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public void setOznaka(String oznaka) {
        this.oznaka = oznaka;
    }

    public void setESPB(int ESPB) {
        this.ESPB = ESPB;
    }

    public void setNastavnici(Set<Nastavnik> nastavnici) {
        this.nastavnici = nastavnici;
    }

    public void setSmerovi(Set<Smer> smerovi) {
        this.smerovi = smerovi;
    }

    public void setPredispitnaObaveza(PredispitnaObaveza predispitnaObaveza) {
        this.predispitnaObaveza = predispitnaObaveza;
    }

    public void setIspiti(Set<Ispit> ispiti) {
        this.ispiti = ispiti;
    }

    public void setObrisano(boolean obrisano) {
        this.obrisano = obrisano;
    }

    public Set<PolaganjeIspita> getPolaganja() {
        return polaganja;
    }

    public void setPolaganja(Set<PolaganjeIspita> polaganja) {
        this.polaganja = polaganja;
    }
}
