package com.SF64.ELearning.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public class Prijava {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private Date datumPrijave;

    @NotNull
    private boolean obrisano;

    @NotNull
    private boolean ocenjen;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ispit_id")
    private Ispit ispit;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "student_id")
    private Student student;

    @OneToOne(mappedBy = "prijava")
    private PolaganjeIspita polaganjeIspita;

    public Prijava() {
    }

    public long getId() {
        return id;
    }

    public Date getDatumPrijave() {
        return datumPrijave;
    }

    public Ispit getIspit() {
        return ispit;
    }

    public Student getStudent() {
        return student;
    }

    public PolaganjeIspita getPolaganjeIspita() {
        return polaganjeIspita;
    }

    public boolean isObrisano() {
        return obrisano;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setDatumPrijave(Date datumPrijave) {
        this.datumPrijave = datumPrijave;
    }

    public void setIspit(Ispit ispit) {
        this.ispit = ispit;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public void setPolaganjeIspita(PolaganjeIspita polaganjeIspita) {
        this.polaganjeIspita = polaganjeIspita;
    }

    public void setObrisano(boolean obrisano) {
        this.obrisano = obrisano;
    }

    public boolean isOcenjen() {
        return ocenjen;
    }

    public void setOcenjen(boolean ocenjen) {
        this.ocenjen = ocenjen;
    }
}
