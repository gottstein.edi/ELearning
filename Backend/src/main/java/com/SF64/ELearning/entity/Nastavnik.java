package com.SF64.ELearning.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Nastavnik extends User {

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "zvanje_id")
    private Zvanje zvanje;

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "Nastavnik_Predmet",
            joinColumns = { @JoinColumn(name = "nastavnik_id") },
            inverseJoinColumns = { @JoinColumn(name = "predmet_id") }
    )
    Set<Predmet> predmeti = new HashSet<>();


    public Nastavnik() {

    }

    public Zvanje getZvanje() {
        return zvanje;
    }

    public Set<Predmet> getPredmeti() {
        return predmeti;
    }

    public void setZvanje(Zvanje zvanje) {
        this.zvanje = zvanje;
    }

    public void setPredmeti(Set<Predmet> predmeti) {
        this.predmeti = predmeti;
    }
}
