package com.SF64.ELearning.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public class Uplata {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private Date datumUplate;

    @NotNull
    private int iznos;

    private String svrhaUplate;

    @NotNull
    private boolean obrisano;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "student_id")
    private Student student;

    public Uplata() {
    }

    public long getId() {
        return id;
    }

    public Date getDatumUplate() {
        return datumUplate;
    }

    public int getIznos() {
        return iznos;
    }

    public Student getStudent() {
        return student;
    }

    public boolean isObrisano() {
        return obrisano;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setDatumUplate(Date datumUplate) {
        this.datumUplate = datumUplate;
    }

    public void setIznos(int iznos) {
        this.iznos = iznos;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public void setObrisano(boolean obrisano) {
        this.obrisano = obrisano;
    }

    public String getSvrhaUplate() {
        return svrhaUplate;
    }

    public void setSvrhaUplate(String svrhaUplate) {
        this.svrhaUplate = svrhaUplate;
    }
}
