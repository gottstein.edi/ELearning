package com.SF64.ELearning.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Ispit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private Date datumOdrzavanja;

    @NotNull
    private Date rokZaPrijavu;

    @NotNull
    private boolean obrisano;


    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "predmet_id")
    private Predmet predmet;

    @OneToMany(mappedBy = "ispit", cascade = CascadeType.ALL)
    private Set<Prijava> prijave = new HashSet<>();





    public Ispit() {

    }

    public long getId() {
        return id;
    }

    public Date getDatumOdrzavanja() {
        return datumOdrzavanja;
    }

    public Date getRokZaPrijavu() {
        return rokZaPrijavu;
    }

    public Predmet getPredmet() {
        return predmet;
    }

    public Set<Prijava> getPrijave() {
        return prijave;
    }


    public boolean isObrisano() {
        return obrisano;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setDatumOdrzavanja(Date datumOdrzavanja) {
        this.datumOdrzavanja = datumOdrzavanja;
    }

    public void setRokZaPrijavu(Date rokZaPrijavu) {
        this.rokZaPrijavu = rokZaPrijavu;
    }

    public void setPredmet(Predmet predmet) {
        this.predmet = predmet;
    }

    public void setPrijave(Set<Prijava> prijave) {
        this.prijave = prijave;
    }


    public void setObrisano(boolean obrisano) {
        this.obrisano = obrisano;
    }


}
