package com.SF64.ELearning.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Student extends User {

    @NotNull
    private Date datumRodjenja;

    @NotNull
    @Size(min = 1, max = 50)
    private String adresaStanovanja;

    @NotNull
    @Size(min = 1, max = 30)
    private String drzavljanstvo;

    @NotNull
    @Size(min = 4, max = 12)
    private String brojIndeksa;

    @NotNull
    private int godinaUpisa;

    @NotNull
    private int godinaStudije;

    @NotNull
    @Size(min = 13, max = 13)
    private String JMBG;

    @OneToMany(mappedBy = "student", cascade = CascadeType.ALL)
    private Set<Uplata> uplate = new HashSet<>();

    @OneToMany(mappedBy = "student", cascade = CascadeType.ALL)
    private Set<Dokument> dokumenti = new HashSet<>();


    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "smer_id")
    private Smer smer;

    @OneToMany(mappedBy = "student")
    private Set<Prijava> prijave = new HashSet<>();

    @OneToMany(mappedBy = "student")
    private Set<PolaganjeIspita> polaganja = new HashSet<>();



    public Student() {
    }

    public Date getDatumRodjenja() {
        return datumRodjenja;
    }

    public String getAdresaStanovanja() {
        return adresaStanovanja;
    }

    public String getDrzavljanstvo() {
        return drzavljanstvo;
    }

    public String getBrojIndeksa() {
        return brojIndeksa;
    }

    public int getGodinaUpisa() {
        return godinaUpisa;
    }

    public int getGodinaStudije() {
        return godinaStudije;
    }

    public String getJMBG() {
        return JMBG;
    }

    public Set<Uplata> getUplate() {
        return uplate;
    }

    public Set<Dokument> getDokumenti() {
        return dokumenti;
    }



    public Smer getSmer() {
        return smer;
    }

    public Set<Prijava> getPrijave() {
        return prijave;
    }


    public void setDatumRodjenja(Date datumRodjenja) {
        this.datumRodjenja = datumRodjenja;
    }

    public void setAdresaStanovanja(String adresaStanovanja) {
        this.adresaStanovanja = adresaStanovanja;
    }

    public void setDrzavljanstvo(String drzavljanstvo) {
        this.drzavljanstvo = drzavljanstvo;
    }

    public void setBrojIndeksa(String brojIndeksa) {
        this.brojIndeksa = brojIndeksa;
    }

    public void setGodinaUpisa(int godinaUpisa) {
        this.godinaUpisa = godinaUpisa;
    }

    public void setGodinaStudije(int godinaStudije) {
        this.godinaStudije = godinaStudije;
    }

    public void setJMBG(String JMBG) {
        this.JMBG = JMBG;
    }

    public void setUplate(Set<Uplata> uplate) {
        this.uplate = uplate;
    }

    public void setDokumenti(Set<Dokument> dokumenti) {
        this.dokumenti = dokumenti;
    }



    public void setSmer(Smer smer) {
        this.smer = smer;
    }

    public void setPrijave(Set<Prijava> prijave) {
        this.prijave = prijave;
    }

    public Set<PolaganjeIspita> getPolaganja() {
        return polaganja;
    }

    public void setPolaganja(Set<PolaganjeIspita> polaganja) {
        this.polaganja = polaganja;
    }
}
