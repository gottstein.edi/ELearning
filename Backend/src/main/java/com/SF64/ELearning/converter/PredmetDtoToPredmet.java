package com.SF64.ELearning.converter;

import com.SF64.ELearning.dtos.PredmetDto;
import com.SF64.ELearning.entity.PredispitnaObaveza;
import com.SF64.ELearning.entity.Predmet;
import com.SF64.ELearning.services.PredispitnaObavezaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PredmetDtoToPredmet implements Converter<PredmetDto,Predmet> {

    @Autowired
    PredispitnaObavezaService predispitnaObavezaService;

    @Override
    public Predmet convert(PredmetDto predmetDto) {
        Predmet predmet = new Predmet();
        predmet.setId(predmetDto.getId());
        predmet.setESPB(predmetDto.getESPB());
        predmet.setNaziv(predmetDto.getNaziv());
        predmet.setOznaka(predmetDto.getOznaka());

        PredispitnaObaveza predispitnaObaveza = predispitnaObavezaService.findOne(predmetDto.getPredispitnaObaveza());
        if(predispitnaObaveza != null){
            predmet.setPredispitnaObaveza(predispitnaObaveza);
        }

        return predmet;

    }
}
