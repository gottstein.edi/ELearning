package com.SF64.ELearning.converter;

import com.SF64.ELearning.dtos.AdminDto;
import com.SF64.ELearning.entity.Admin;
import com.SF64.ELearning.services.RoleService;
import com.SF64.ELearning.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class AdminDtoToAdmin implements Converter<AdminDto, Admin> {

    @Autowired
    UserService userService;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    RoleService roleService;

    @Override
    public Admin convert(AdminDto adminDto) {

        Admin admin = new Admin();
        admin.setId(adminDto.getId());
        admin.setIme(adminDto.getIme());
        admin.setPrezime(adminDto.getPrezime());
        admin.setEmail(adminDto.getEmail());
        admin.setRole(roleService.findOne((long) 3));
        if(adminDto.getSifra() != null && adminDto.getSifra() != ""){
            admin.setSifra(encoder.encode(adminDto.getSifra()));
        } else {
            admin.setSifra(userService.findOne(adminDto.getId()).getSifra());
        }
        admin.setKorisnickoIme(adminDto.getKorisnickoIme());

        return admin;
    }
}
