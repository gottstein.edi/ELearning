package com.SF64.ELearning.converter;

import com.SF64.ELearning.dtos.PolaganjeStudentDto;
import com.SF64.ELearning.entity.PolaganjeIspita;
import com.SF64.ELearning.services.PredmetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
public class PolaganjeToPolaganjeStudentDto implements Converter<PolaganjeIspita, PolaganjeStudentDto> {

    @Autowired
    PredmetService predmetService;


    @Override
    public PolaganjeStudentDto convert(PolaganjeIspita polaganjeIspita) {
        PolaganjeStudentDto polaganjeStudentDto = new PolaganjeStudentDto();

        polaganjeStudentDto.setBrojIspitnihBodova(polaganjeIspita.getIspitniBodovi());
        polaganjeStudentDto.setBrojPredispitnihBodova(polaganjeIspita.getPredispitniBodovi());
        polaganjeStudentDto.setUkupanBrojBodova(polaganjeIspita.getUkupniBodovi());
        polaganjeStudentDto.setOcena(polaganjeIspita.getOcena());
        polaganjeStudentDto.setPredmet(predmetService.findOne(polaganjeIspita.getPredmet().getId()).getNaziv());

        return polaganjeStudentDto;
    }


    public List<PolaganjeStudentDto> convert(Set<PolaganjeIspita> listaPolaganja){

        List<PolaganjeStudentDto> polaganjaDtos = new ArrayList<>();
        for (PolaganjeIspita polaganje: listaPolaganja
             ) {
            PolaganjeStudentDto polaganjeStudentDto = new PolaganjeStudentDto();
            polaganjeStudentDto.setBrojIspitnihBodova(polaganje.getIspitniBodovi());
            polaganjeStudentDto.setBrojPredispitnihBodova(polaganje.getPredispitniBodovi());
            polaganjeStudentDto.setUkupanBrojBodova(polaganje.getUkupniBodovi());
            polaganjeStudentDto.setOcena(polaganje.getOcena());
            polaganjeStudentDto.setPredmet(predmetService.findOne(polaganje.getPredmet().getId()).getNaziv());

            polaganjaDtos.add(polaganjeStudentDto);

        }

        return polaganjaDtos;

    }
}
