package com.SF64.ELearning.converter;

import com.SF64.ELearning.dtos.ZvanjeDto;
import com.SF64.ELearning.entity.Zvanje;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ZvanjeDtoToZvanje implements Converter<ZvanjeDto, Zvanje> {

    @Override
    public Zvanje convert(ZvanjeDto zvanjeDto) {
        Zvanje zvanje = new Zvanje();
        zvanje.setId(zvanjeDto.getId());
        zvanje.setNaziv(zvanjeDto.getNaziv());
        return zvanje;
    }
}
