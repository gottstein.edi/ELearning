package com.SF64.ELearning.converter;

import com.SF64.ELearning.dtos.DokumentDto;
import com.SF64.ELearning.entity.Dokument;
import com.SF64.ELearning.entity.Student;
import com.SF64.ELearning.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class DokumentDtoToDokument implements Converter<DokumentDto,Dokument> {

    @Autowired
    StudentService studentService;


    @Override
    public Dokument convert(DokumentDto dokumentDto) {
        Dokument dokument = new Dokument();
        dokument.setId(dokumentDto.getId());
        dokument.setFilename(dokumentDto.getFilename());
        dokument.setMIME(dokumentDto.getMIME());
        dokument.setNaziv(dokumentDto.getNaziv());

        Student student = studentService.findOne(dokumentDto.getStudent());
        if(student != null){
            dokument.setStudent(student);
        }
        return dokument;


    }
}
