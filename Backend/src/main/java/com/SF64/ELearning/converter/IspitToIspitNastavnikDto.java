package com.SF64.ELearning.converter;

import com.SF64.ELearning.dtos.IspitNastavnikDto;
import com.SF64.ELearning.entity.Ispit;
import com.SF64.ELearning.entity.Predmet;
import com.SF64.ELearning.services.PredmetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@Component
public class IspitToIspitNastavnikDto implements Converter<Ispit, IspitNastavnikDto> {

    @Autowired
    PredmetService predmetService;

    @Override
    public IspitNastavnikDto convert(Ispit ispit) {
        IspitNastavnikDto ispitDto = new IspitNastavnikDto();


        ispitDto.setDatumOdrzavanja(ispit.getDatumOdrzavanja());
        ispitDto.setPredmet(predmetService.findOne(ispit.getPredmet().getId()).getNaziv());
        ispitDto.setRokZaPrijavu(ispit.getRokZaPrijavu());
        ispitDto.setId(ispit.getId());


        return ispitDto;
    }

    public List<IspitNastavnikDto> convert(Set<Predmet> predmetiNastavnika){
        List<IspitNastavnikDto> sviIspitiDto = new ArrayList<>();
        for(Predmet predmet: predmetiNastavnika){
            for(Ispit ispit: predmet.getIspiti()){
                sviIspitiDto.add(convert(ispit));
            }
        }
        return sviIspitiDto;
    }

}
