package com.SF64.ELearning.converter;

import com.SF64.ELearning.dtos.UplataDto;
import com.SF64.ELearning.entity.Student;
import com.SF64.ELearning.entity.Uplata;
import com.SF64.ELearning.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class UplataDtoToUplata implements Converter<UplataDto, Uplata> {

    @Autowired
    StudentService studentService;

    @Override
    public Uplata convert(UplataDto uplataDto) {
        Uplata uplata = new Uplata();
        uplata.setId(uplataDto.getId());
        uplata.setDatumUplate(new Date());
        uplata.setIznos(uplataDto.getIznos());
        uplata.setSvrhaUplate(uplataDto.getSvrhaUplate());

        Student student = studentService.findOne(uplataDto.getStudent());
        if(student != null) {
            uplata.setStudent(student);
        }

        return uplata;
    }
}
