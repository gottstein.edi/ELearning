package com.SF64.ELearning.converter;

import com.SF64.ELearning.dtos.SmerDto;
import com.SF64.ELearning.entity.Smer;
import com.SF64.ELearning.services.SmerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;


@Component
public class SmerDtoToSmer implements Converter<SmerDto, Smer> {

    @Autowired
    SmerService smerService;

    @Override
    public Smer convert(SmerDto smerDto) {
        Smer smer = new Smer();
        smer.setBrojSemestra(smerDto.getBrojSemestra());
        smer.setNaziv(smerDto.getNaziv());
        smer.setOznaka(smerDto.getOznaka());
        smer.setTip(smerDto.getTip());

        return smer;
    }

    public Smer update(SmerDto smerDto){
        Smer smer = smerService.findOne(smerDto.getId());
        smer.setBrojSemestra(smerDto.getBrojSemestra());
        smer.setNaziv(smerDto.getNaziv());
        smer.setOznaka(smerDto.getOznaka());
        smer.setTip(smerDto.getTip());

        return smer;
    }
}
