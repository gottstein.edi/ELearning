package com.SF64.ELearning.converter;

import com.SF64.ELearning.dtos.NastavnikDto;
import com.SF64.ELearning.entity.Nastavnik;
import com.SF64.ELearning.services.NastavnikService;
import com.SF64.ELearning.services.RoleService;
import com.SF64.ELearning.services.UserService;
import com.SF64.ELearning.services.ZvanjeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class NastavnikDtoToNastavnik implements Converter<NastavnikDto, Nastavnik> {

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    RoleService roleService;

    @Autowired
    ZvanjeService zvanjeService;

    @Autowired
    UserService userService;

    @Autowired
    NastavnikService nastavnikService;

    @Override
    public Nastavnik convert(NastavnikDto nastavnikDto) {
        Nastavnik nastavnik = new Nastavnik();
        nastavnik.setKorisnickoIme(nastavnikDto.getKorisnickoIme());
        nastavnik.setPrezime(nastavnikDto.getPrezime());
        nastavnik.setIme(nastavnikDto.getIme());
        nastavnik.setSifra(encoder.encode(nastavnikDto.getSifra()));
        nastavnik.setZvanje(zvanjeService.findOne(nastavnikDto.getZvanje()));
        nastavnik.setRole(roleService.findOne((long) 2));
        nastavnik.setEmail(nastavnikDto.getEmail());
        return nastavnik;
    }

    public Nastavnik update(NastavnikDto nastavnikDto){
        Nastavnik nastavnik = nastavnikService.findOne(nastavnikDto.getId());

        nastavnik.setKorisnickoIme(nastavnikDto.getKorisnickoIme());
        nastavnik.setPrezime(nastavnikDto.getPrezime());
        nastavnik.setIme(nastavnikDto.getIme());
        if(nastavnikDto.getSifra() != null && nastavnikDto.getSifra() != "" ){
            nastavnik.setSifra(encoder.encode(nastavnikDto.getSifra()));
        } else{
            nastavnik.setSifra(userService.findOne(nastavnikDto.getId()).getSifra());
        }

        nastavnik.setZvanje(zvanjeService.findOne(nastavnikDto.getZvanje()));
        nastavnik.setRole(roleService.findOne((long) 2));
        nastavnik.setEmail(nastavnikDto.getEmail());
        return nastavnik;
    }
}
