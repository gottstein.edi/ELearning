package com.SF64.ELearning.converter;

import com.SF64.ELearning.dtos.PolaganjeIspitaDto;
import com.SF64.ELearning.entity.Ispit;
import com.SF64.ELearning.entity.PolaganjeIspita;
import com.SF64.ELearning.entity.Prijava;
import com.SF64.ELearning.entity.Student;
import com.SF64.ELearning.services.IspitService;
import com.SF64.ELearning.services.PredmetService;
import com.SF64.ELearning.services.PrijavaService;
import com.SF64.ELearning.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PolaganjeDtoToPolaganje implements Converter<PolaganjeIspitaDto, PolaganjeIspita> {

    @Autowired
    PrijavaService prijavaService;

    @Autowired
    StudentService studentService;

    @Autowired
    PredmetService predmetService;



    @Override
    public PolaganjeIspita convert(PolaganjeIspitaDto polaganjeIspitaDto) {
        PolaganjeIspita polaganjeIspita = new PolaganjeIspita();
        polaganjeIspita.setId(polaganjeIspitaDto.getId());
        polaganjeIspita.setIspitniBodovi(polaganjeIspitaDto.getIspitniBodovi());
        polaganjeIspita.setOcena(polaganjeIspitaDto.getOcena());
        polaganjeIspita.setPolozio(polaganjeIspitaDto.isPolozio());
        polaganjeIspita.setPredispitniBodovi(polaganjeIspitaDto.getPredispitniBodovi());
        polaganjeIspita.setUkupniBodovi(polaganjeIspitaDto.getUkupniBodovi());

        Prijava prijava = prijavaService.findOne(polaganjeIspitaDto.getPrijava());
        if(prijava != null){
            polaganjeIspita.setPrijava(prijava);
            polaganjeIspita.setStudent(prijava.getStudent());
            polaganjeIspita.setPredmet(prijava.getIspit().getPredmet());
        }



        return polaganjeIspita;

    }
}
