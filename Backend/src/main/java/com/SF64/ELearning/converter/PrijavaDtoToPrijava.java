package com.SF64.ELearning.converter;

import com.SF64.ELearning.dtos.PrijavaDto;
import com.SF64.ELearning.entity.Ispit;
import com.SF64.ELearning.entity.Prijava;
import com.SF64.ELearning.entity.Student;
import com.SF64.ELearning.services.IspitService;
import com.SF64.ELearning.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PrijavaDtoToPrijava implements Converter<PrijavaDto, Prijava> {

    @Autowired
    StudentService studentService;

    @Autowired
    IspitService ispitService;

    @Override
    public Prijava convert(PrijavaDto prijavaDto) {
        Prijava prijava = new Prijava();
        prijava.setId(prijavaDto.getId());

        Ispit ispit = ispitService.findOne(prijavaDto.getIspit());
        if(ispit != null) {
            prijava.setIspit(ispit);
        }

        Student student = studentService.findOne(prijavaDto.getStudent());
        if(student != null){
            prijava.setStudent(student);
        }

        return prijava;

    }
}
