package com.SF64.ELearning.converter;

import com.SF64.ELearning.dtos.IspitDto;
import com.SF64.ELearning.entity.Ispit;
import com.SF64.ELearning.entity.Predmet;
import com.SF64.ELearning.services.PredmetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class IspitDtoToIspit implements Converter<IspitDto, Ispit> {

    @Autowired
    PredmetService predmetService;

    @Override
    public Ispit convert(IspitDto ispitDto) {
        Ispit ispit = new Ispit();
        ispit.setId(ispitDto.getId());
        ispit.setDatumOdrzavanja(ispitDto.getDatumOdrzavanja());
        ispit.setRokZaPrijavu(ispitDto.getRokZaPrijavu());

        Predmet predmet = predmetService.findOne(ispitDto.getPredmet());
        if(predmet != null) {
            ispit.setPredmet(predmet);
        }
        return ispit;
    }
}
