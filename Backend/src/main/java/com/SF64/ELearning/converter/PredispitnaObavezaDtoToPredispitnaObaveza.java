package com.SF64.ELearning.converter;

import com.SF64.ELearning.dtos.PredispitnaObavezaDto;
import com.SF64.ELearning.entity.PredispitnaObaveza;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PredispitnaObavezaDtoToPredispitnaObaveza implements Converter<PredispitnaObavezaDto, PredispitnaObaveza> {

    @Override
    public PredispitnaObaveza convert(PredispitnaObavezaDto predispitnaObavezaDto) {
        PredispitnaObaveza predispitnaObaveza = new PredispitnaObaveza();
        predispitnaObaveza.setId(predispitnaObavezaDto.getId());
        predispitnaObaveza.setBrojBodova(predispitnaObavezaDto.getBrojBodova());
        predispitnaObaveza.setNaziv(predispitnaObavezaDto.getNaziv());
        return predispitnaObaveza;
    }
}
