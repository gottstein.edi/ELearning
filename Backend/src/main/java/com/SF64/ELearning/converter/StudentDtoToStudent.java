package com.SF64.ELearning.converter;

import com.SF64.ELearning.dtos.StudentDto;
import com.SF64.ELearning.entity.Student;
import com.SF64.ELearning.services.RoleService;
import com.SF64.ELearning.services.SmerService;
import com.SF64.ELearning.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class StudentDtoToStudent implements Converter<StudentDto, Student> {

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    RoleService roleService;

    @Autowired
    SmerService smerService;

    @Autowired
    UserService userService;

    @Override
    public Student convert(StudentDto studentDto) {
        Student student = new Student();
        student.setId(studentDto.getId());
        student.setIme(studentDto.getIme());
        student.setPrezime(studentDto.getPrezime());
        student.setAdresaStanovanja(studentDto.getAdresaStanovanja());
        student.setBrojIndeksa(studentDto.getBrojIndeksa());
        student.setDatumRodjenja(studentDto.getDatumRodjenja());
        student.setDrzavljanstvo(studentDto.getDrzavljanstvo());
        student.setGodinaStudije(studentDto.getGodinaStudije());
        student.setJMBG(studentDto.getJMBG());
        student.setGodinaUpisa(studentDto.getGodinaUpisa());
        student.setSmer(smerService.findOne(studentDto.getSmer()));
        student.setRole(roleService.findOne((long) 1));
        student.setEmail(studentDto.getEmail());
        if(studentDto.getSifra() != null && studentDto.getSifra() != ""){
            student.setSifra(encoder.encode(studentDto.getSifra()));
        } else {
            student.setSifra(userService.findOne(studentDto.getId()).getSifra());
        }

        student.setKorisnickoIme(studentDto.getKorisnickoIme());

        return student;
    }
}
