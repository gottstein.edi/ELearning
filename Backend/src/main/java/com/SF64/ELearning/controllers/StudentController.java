package com.SF64.ELearning.controllers;

import com.SF64.ELearning.converter.StudentDtoToStudent;
import com.SF64.ELearning.dtos.*;
import com.SF64.ELearning.entity.*;
import com.SF64.ELearning.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Pageable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/studenti")
public class StudentController {

    @Autowired
    StudentService studentService;

    @Autowired
    DokumentService dokumentService;

    @Autowired
    PrijavaService prijavaService;

    @Autowired
    UplataService uplataService;

    @Autowired
    StudentDtoToStudent toStudent;

    @Autowired
    PolaganjeIspitaService polaganjeIspitaService;

    @Autowired
    PredmetService predmetService;

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity getAll(){
        List<Student> students = studentService.findAll();
        if(students == null || students.size() == 0){
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.ok().body(students.stream().map(StudentDto::new).collect(Collectors.toList()));
    }

    @GetMapping("/ne-upisani")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity getAllWhereSmerNull(){
        List<Student> students =studentService.findAllSmerNull();
        if(students == null || students.size() == 0) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.ok().body(students.stream().map(StudentDto::new).collect(Collectors.toList()));
    }

    @GetMapping("/{id}")
    public ResponseEntity getOne(@PathVariable("id") long id) {

        Student student = studentService.findOne(id);
        if(student == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok().body(new StudentDto(student));
    }

    @GetMapping("/{id}/predmeti")
    public ResponseEntity getAllPredmeti(@PathVariable("id") long id){
        Student student = studentService.findOne(id);
        if(student == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        if(student.getSmer() == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok().body(student.getSmer().getPredmeti().stream().map(PredmetDto::new).collect(Collectors.toList()));

    }

    @GetMapping("/{id}/uplate")
    public ResponseEntity getAllUplate(@PathVariable("id") long id, org.springframework.data.domain.Pageable pageable){
        Student student = studentService.findOne(id);
        if(student == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        Page<Uplata> uplate = uplataService.findAllByStudent(id, pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-Total", String.valueOf(uplate.getTotalElements()));

        return ResponseEntity.ok().headers(headers).body(uplate.getContent().stream().map(UplataDto::new).collect(Collectors.toList()));

    }

    @GetMapping("/{id}/prijave")
    public ResponseEntity getAllPrijave(@PathVariable("id") long id){
        Student student = studentService.findOne(id);
        if(student == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        List<Prijava> prijaveStudenta = prijavaService.findAllByStudent(id);

        return ResponseEntity.ok().body(prijaveStudenta.stream().map(PrijavaDto::new).collect(Collectors.toList()));
    }

    @GetMapping("/{id}/dokumenti")
    public ResponseEntity getAllDokumenti(@PathVariable("id") long id){
        Student student = studentService.findOne(id);
        if(student == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok().body(dokumentService.findAllByStudent(id).stream().map(DokumentDto::new).collect(Collectors.toList()));
    }

    @GetMapping("/{id}/predmeti/polozio")
    public ResponseEntity getAllPolozeno(@PathVariable("id") long id){
        Student student = studentService.findOne(id);
        if(student == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        List<PolaganjeIspita> polozenaPolaganja = polaganjeIspitaService.findStudentPolozeniOrNepolozeniIspiti(id, true);
        Set<Predmet> polozeniPredmeti = new HashSet<>();
        for(PolaganjeIspita polaganje: polozenaPolaganja){
            polozeniPredmeti.add(polaganje.getPredmet());
        }

        return ResponseEntity.ok().body(polozeniPredmeti.stream().map(PredmetDto::new).collect(Collectors.toList()));
    }

    @GetMapping("/{id}/predmeti/ne-polozio")
    public ResponseEntity getAllNePolozeno(@PathVariable("id") long id){
        Student student = studentService.findOne(id);
        if(student == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        List<PolaganjeIspita> polozenaPolaganja = polaganjeIspitaService.findStudentPolozeniOrNepolozeniIspiti(id, true);
        Set<Predmet> polozeniPredmeti = new HashSet<>();
        Set<Predmet> nePolozeniPredmeti = new HashSet<>();
        for(PolaganjeIspita polaganje: polozenaPolaganja){
            polozeniPredmeti.add(polaganje.getPredmet());
        }
        if(polozeniPredmeti.isEmpty()){
            return ResponseEntity.ok().body(student.getSmer().getPredmeti().stream().map(PredmetDto::new).collect(Collectors.toList()));
        }


        for(Predmet predmet: student.getSmer().getPredmeti()){
            for(Predmet polozeniPredmet: polozeniPredmeti){
                if(predmet.getId() != polozeniPredmet.getId()){
                    nePolozeniPredmeti.add(predmet);
                }
            }
        }

        return ResponseEntity.ok().body(nePolozeniPredmeti.stream().map(PredmetDto::new).collect(Collectors.toList()));

    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity postOne(@Validated @RequestBody StudentDto dto, Errors errors){
        if(errors.hasErrors()) {
            return new ResponseEntity(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
        }
        if(dto != null) {
            studentService.save(toStudent.convert(dto));
            return new ResponseEntity(HttpStatus.CREATED);
        }
        else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity updateOne(@PathVariable("id") long id,@Validated @RequestBody StudentDto studentDto, Errors errors){
        if(errors.hasErrors()){
            return new ResponseEntity(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
        }
        if(studentDto != null){
            if(studentDto.getId() == id && studentDto.getId() != 0){
                studentService.save(toStudent.convert(studentDto));
                return new ResponseEntity(HttpStatus.OK);
            }
        }

        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }





    @DeleteMapping("/{id}")
    public ResponseEntity deleteOne(@PathVariable long id) {
        Student student = studentService.findOne(id);
        if(student == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        studentService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
