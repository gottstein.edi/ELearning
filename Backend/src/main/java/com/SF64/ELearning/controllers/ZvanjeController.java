package com.SF64.ELearning.controllers;

import com.SF64.ELearning.converter.ZvanjeDtoToZvanje;
import com.SF64.ELearning.dtos.ZvanjeDto;
import com.SF64.ELearning.entity.Zvanje;
import com.SF64.ELearning.services.ZvanjeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/zvanja")
public class ZvanjeController {

    @Autowired
    ZvanjeService zvanjeService;

    @Autowired
    ZvanjeDtoToZvanje zvanjeDtoToZvanje;

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity getAll(){
        List<Zvanje> zvanja = zvanjeService.findAll();
        if(zvanja == null || zvanja.size() == 0){
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.ok().body(zvanja.stream().map(ZvanjeDto::new).collect(Collectors.toList()));
    }

    @GetMapping("/{id}")
    public ResponseEntity getOne(@PathVariable("id") long id) {

        Zvanje zvanje = zvanjeService.findOne(id);
        if(zvanje == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok().body(new ZvanjeDto(zvanje));
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity postOne(@Validated @RequestBody ZvanjeDto dto, Errors errors){
        if(errors.hasErrors()) {
            return new ResponseEntity(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
        }
        if(dto != null) {
            Zvanje zvanje = zvanjeDtoToZvanje.convert(dto);
            zvanjeService.save(zvanje);
            return new ResponseEntity(HttpStatus.CREATED);
        }
        else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }


    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteOne(@PathVariable long id) {
        Zvanje zvanje = zvanjeService.findOne(id);
        if(zvanje == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        zvanjeService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }


}
