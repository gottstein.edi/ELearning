package com.SF64.ELearning.controllers;

import com.SF64.ELearning.converter.PolaganjeDtoToPolaganje;
import com.SF64.ELearning.converter.PolaganjeToPolaganjeStudentDto;
import com.SF64.ELearning.dtos.PolaganjeIspitaDto;
import com.SF64.ELearning.entity.PolaganjeIspita;
import com.SF64.ELearning.entity.Prijava;
import com.SF64.ELearning.entity.Student;
import com.SF64.ELearning.services.PolaganjeIspitaService;
import com.SF64.ELearning.services.PrijavaService;
import com.SF64.ELearning.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/polaganja")
public class PolaganjeIspitaController {

    @Autowired
    PolaganjeIspitaService polaganjeIspitaService;

    @Autowired
    PolaganjeDtoToPolaganje polaganjeDtoToPolaganje;

    @Autowired
    PrijavaService prijavaService;

    @Autowired
    StudentService studentService;

    @Autowired
    PolaganjeToPolaganjeStudentDto toPolaganjeStudentDto;

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity getAll(){
        List<PolaganjeIspita> polaganja = polaganjeIspitaService.findAll();
        if(polaganja == null || polaganja.size() == 0){
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.ok().body(polaganja.stream().map(PolaganjeIspitaDto::new).collect(Collectors.toList()));
    }

    @GetMapping("/{id}")
    public ResponseEntity getOne(@PathVariable("id") long id) {

        PolaganjeIspita polaganjeIspita = polaganjeIspitaService.findOne(id);
        if (polaganjeIspita == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok().body(new PolaganjeIspitaDto(polaganjeIspita));
    }

    @GetMapping("/{studId}/{predmetId}")
    public ResponseEntity getPolaganjeByStudentAndPredmet(@PathVariable("studId") Long studentId, @PathVariable("predmetId") Long predmetId){
        PolaganjeIspita polaganjeIspita = polaganjeIspitaService.findByStudentAndPredmet(studentId, predmetId);
        if(polaganjeIspita == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok().body(new PolaganjeIspitaDto(polaganjeIspita));
    }

    @GetMapping("/polaganja-studenta/{id}")
    public ResponseEntity getPolaganjaByStudent(@PathVariable("id") Long studentId){
        Student student = studentService.findOne(studentId);
        if(student == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok().body(toPolaganjeStudentDto.convert(student.getPolaganja()));

    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ADMIN','NASTAVNIK')")
    public ResponseEntity postOne(@Validated @RequestBody PolaganjeIspitaDto dto, Errors errors){
        if(errors.hasErrors()) {
            return new ResponseEntity(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
        }
        if(dto != null) {
            PolaganjeIspita polaganjeIspita = polaganjeDtoToPolaganje.convert(dto);
            Prijava prijava = prijavaService.findOne(dto.getPrijava());
            prijava.setOcenjen(true);
            prijavaService.save(prijava);
            polaganjeIspitaService.save(polaganjeIspita);
            return new ResponseEntity(HttpStatus.CREATED);
        }
        else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }


    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteOne(@PathVariable long id) {
        PolaganjeIspita polaganjeIspita = polaganjeIspitaService.findOne(id);
        if (polaganjeIspita == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        polaganjeIspitaService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
