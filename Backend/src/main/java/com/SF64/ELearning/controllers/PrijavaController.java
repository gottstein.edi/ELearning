package com.SF64.ELearning.controllers;

import com.SF64.ELearning.converter.PrijavaDtoToPrijava;
import com.SF64.ELearning.dtos.PrijavaDto;
import com.SF64.ELearning.entity.Prijava;
import com.SF64.ELearning.services.PrijavaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/prijave")
public class PrijavaController {

    @Autowired
    PrijavaService prijavaService;

    @Autowired
    PrijavaDtoToPrijava prijavaDtoToPrijava;

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity getAll(){
        List<Prijava> prijave = prijavaService.findAll();
        if(prijave == null || prijave.size() == 0) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.ok().body(prijave.stream().map(PrijavaDto::new).collect(Collectors.toList()));
    }

    @GetMapping("/{id}")
    public ResponseEntity getOne(@PathVariable("id") long id) {

        Prijava prijava = prijavaService.findOne(id);
        if(prijava == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok().body(new PrijavaDto(prijava));
    }



    @PostMapping
    @PreAuthorize("hasAnyRole('ADMIN','STUDENT')")
    public ResponseEntity postOne(@Validated @RequestBody PrijavaDto dto, Errors errors){
        if(errors.hasErrors()) {
            return new ResponseEntity(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
        }
        if(dto != null) {
            Prijava prijavaCheck = prijavaService.findByStudentAndIspit(dto.getStudent(), dto.getIspit());
            if(prijavaCheck == null){
                Prijava prijava = prijavaDtoToPrijava.convert(dto);
                prijava.setDatumPrijave(new Date());
                prijava.setOcenjen(false);

                prijavaService.save(prijava);
                return new ResponseEntity(HttpStatus.CREATED);
            }
            return new ResponseEntity(HttpStatus.CONFLICT);

        }

        else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }


    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteOne(@PathVariable long id) {
        Prijava prijava = prijavaService.findOne(id);
        if(prijava == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        prijavaService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
