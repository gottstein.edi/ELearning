package com.SF64.ELearning.controllers;


import com.SF64.ELearning.converter.DokumentDtoToDokument;
import com.SF64.ELearning.dtos.DokumentDto;
import com.SF64.ELearning.dtos.DokumentUploadDto;
import com.SF64.ELearning.entity.Dokument;
import com.SF64.ELearning.entity.Student;
import com.SF64.ELearning.services.DokumentService;
import com.SF64.ELearning.services.StudentService;
import com.SF64.ELearning.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/dokumenti")
public class DokumentController {

    @Autowired
    DokumentService dokumentService;

    @Autowired
    StorageService storageService;

    @Autowired
    StudentService  studentService;

    @Autowired
    DokumentDtoToDokument dokumentDtoToDokument;





    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity getAll(){
        List<Dokument> dokuments = dokumentService.findAll();
        if(dokuments == null || dokuments.size() == 0) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.ok().body(dokuments.stream().map(DokumentDto::new).collect(Collectors.toList()));
    }

    @GetMapping("/{id}")
    public ResponseEntity getOne(@PathVariable ("id") long id) {

        Dokument dokument = dokumentService.findOne(id);
        if(dokument == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok().body(new DokumentDto(dokument));
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('STUDENT','ADMIN')")
    public ResponseEntity postOne(@RequestParam("file") MultipartFile file, @Validated DokumentUploadDto dto, Errors errors){
        if(errors.hasErrors()) {
            return new ResponseEntity(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
        }
        Student student = studentService.findOne(dto.getStudent());
        if(student == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        Dokument dokument = new Dokument();
        dokument.setNaziv(dto.getNaziv());
        dokument.setMIME(file.getContentType());
        dokument.setFilename("files/"+file.getOriginalFilename());
        dokument.setStudent(student);
        dokumentService.save(dokument);
        storageService.store(file);
        return new ResponseEntity(HttpStatus.CREATED);

    }


    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN','STUDENT')")
    public ResponseEntity putOne(@PathVariable("id") long id, @Validated @RequestBody DokumentDto dto, Errors errors) {
        if(errors.hasErrors()){
            return new ResponseEntity(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
        }
        if(dto.getId() != id) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        } else {
            Dokument dokument = dokumentDtoToDokument.convert(dto);
            dokumentService.save(dokument);

            return new ResponseEntity(HttpStatus.OK);
        }
    }



    @PutMapping("/{id}/delete")
    @PreAuthorize("hasAnyRole('ADMIN','STUDENT')")
    public ResponseEntity deleteOne(@PathVariable long id) {
        Dokument dokument = dokumentService.findOne(id);
        if(dokument == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        storageService.delete(dokument.getFilename());
        dokumentService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }


}
