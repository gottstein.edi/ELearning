package com.SF64.ELearning.controllers;

import com.SF64.ELearning.converter.PredispitnaObavezaDtoToPredispitnaObaveza;
import com.SF64.ELearning.dtos.PredispitnaObavezaDto;
import com.SF64.ELearning.entity.PredispitnaObaveza;
import com.SF64.ELearning.services.PredispitnaObavezaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/predispitne-obaveze")
public class PredispitnaObavezaController {

    @Autowired
    PredispitnaObavezaService predispitnaObavezaService;

    @Autowired
    PredispitnaObavezaDtoToPredispitnaObaveza predispitnaObavezaDtoToPredispitnaObaveza;

    @GetMapping
    public ResponseEntity getAll(){
        List<PredispitnaObaveza> predispitneObaveze = predispitnaObavezaService.findAll();
        if(predispitneObaveze == null || predispitneObaveze.size() == 0){
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.ok().body(predispitneObaveze.stream().map(PredispitnaObavezaDto::new).collect(Collectors.toList()));
    }

    @GetMapping("/{id}")
    public ResponseEntity getOne(@PathVariable("id") long id) {

        PredispitnaObaveza predispitnaObaveza = predispitnaObavezaService.findOne(id);
        if (predispitnaObaveza == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok().body(new PredispitnaObavezaDto(predispitnaObaveza));
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity postOne(@Validated @RequestBody PredispitnaObavezaDto dto, Errors errors){
        if(errors.hasErrors()) {
            return new ResponseEntity(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
        }
        if(dto != null) {
            PredispitnaObaveza predispitnaObaveza = predispitnaObavezaDtoToPredispitnaObaveza.convert(dto);
            predispitnaObavezaService.save(predispitnaObaveza);
            return new ResponseEntity(HttpStatus.CREATED);
        }
        else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }


    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteOne(@PathVariable long id) {
        PredispitnaObaveza predispitnaObaveza = predispitnaObavezaService.findOne(id);
        if (predispitnaObaveza == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        predispitnaObavezaService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
