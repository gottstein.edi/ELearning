package com.SF64.ELearning.controllers;

import com.SF64.ELearning.converter.PredmetDtoToPredmet;
import com.SF64.ELearning.dtos.*;
import com.SF64.ELearning.entity.Nastavnik;
import com.SF64.ELearning.entity.Predmet;
import com.SF64.ELearning.repository.NastavnikRepo;
import com.SF64.ELearning.services.NastavnikService;
import com.SF64.ELearning.services.PredmetService;
import com.SF64.ELearning.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/predmeti")
public class PredmetController {

    @Autowired
    PredmetService predmetService;

    @Autowired
    StudentService studentService;

    @Autowired
    NastavnikService nastavnikService;

    @Autowired
    NastavnikRepo nastavnikRepo;

    @Autowired
    PredmetDtoToPredmet predmetDtoToPredmet;


    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity getAll(@RequestParam(value = "naziv", defaultValue = "") String naziv, Pageable pageable){
        Page<Predmet> predmeti = predmetService.findAll(naziv, pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-Total", String.valueOf(predmeti.getTotalElements()));

        return ResponseEntity.ok().headers(headers).body(predmeti.getContent().stream().map(PredmetDto::new).collect(Collectors.toList()));
    }

    @GetMapping("/{id}")
    public ResponseEntity getOne(@PathVariable("id") long id) {

        Predmet predmet = predmetService.findOne(id);
        if(predmet == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok().body(new PredmetDto(predmet));
    }

    @GetMapping("/{id}/predispitna_obaveza")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity getPredispitnaObaveza(@PathVariable("id") long id) {

        Predmet predmet = predmetService.findOne(id);
        if (predmet == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok().body(new PredispitnaObavezaDto(predmet.getPredispitnaObaveza()));
    }

    @GetMapping("/{id}/ispiti")
    public ResponseEntity getIspiti(@PathVariable("id") long id){

        Predmet predmet = predmetService.findOne(id);
        if(predmet == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok().body(predmet.getIspiti().stream().map(IspitDto::new).collect((Collectors.toList())));

    }

    @GetMapping("/{id}/nastavnici")
    public ResponseEntity getNastavnici(@PathVariable("id") long id){
        Predmet predmet = predmetService.findOne(id);

        if(predmet == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok().body(predmet.getNastavnici().stream().map(NastavnikDto::new).collect((Collectors.toList())));
    }

    @GetMapping("/{id}/nastavnici-ne")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity getNastavniciPredmetNeSadrzi(@PathVariable("id") long id){
        Predmet predmet = predmetService.findOne(id);
        if(predmet == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        List<Nastavnik> nastavniciNaPredmetu = new ArrayList<>();
        nastavniciNaPredmetu.addAll(predmet.getNastavnici());
        if(nastavniciNaPredmetu.size() == 0){
            return ResponseEntity.ok().body(nastavnikRepo.findAllByObrisano(false).stream().map(NastavnikDto::new).collect(Collectors.toList()));
        }
        return ResponseEntity.ok().body(nastavnikService.findAllNotOnPredmet(nastavniciNaPredmetu).stream().map(NastavnikDto::new).collect(Collectors.toList()));
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity postOne(@Validated @RequestBody PredmetDto dto, Errors errors){
        if(errors.hasErrors()) {
            return new ResponseEntity(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
        }
        if(dto != null) {
            Predmet predmet = predmetDtoToPredmet.convert(dto);
            predmetService.save(predmet);
            return new ResponseEntity(HttpStatus.CREATED);

        }
        else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping("{id}/nastavnici")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity dodeljivanjeNastavnikaNaPredmet(@PathVariable("id") long id, @RequestBody ListPostWrapper dtoList){
        Predmet predmet = predmetService.findOne(id);
        if(predmet == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        for(Integer nastavnikId : dtoList.getListItemIds()){
            Nastavnik nastavnik = nastavnikService.findOne(nastavnikId.longValue());
            if(nastavnik != null){
                nastavnik.getPredmeti().add(predmet);
                predmet.getNastavnici().add(nastavnik);
            }
        }

        predmetService.save(predmet);
        return new ResponseEntity(HttpStatus.OK);
    }


    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity updateOne(@PathVariable("id") long id, @Validated @RequestBody PredmetDto predmetDto, Errors errors){
        if(errors.hasErrors()){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        if( predmetDto != null){
            if(predmetDto.getId() == id && predmetDto.getId() != 0){
                predmetService.save(predmetDtoToPredmet.convert(predmetDto));
                return new ResponseEntity(HttpStatus.OK);
            }
        }

        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteOne(@PathVariable long id) {
        Predmet predmet = predmetService.findOne(id);
        if(predmet == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        predmetService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}/nastavnici/{nastavnikId}")
    public ResponseEntity removeNastavnikFromPredmet(@PathVariable("id") long predmetId, @PathVariable("nastavnikId") long nastavnikId){
        Predmet predmet = predmetService.findOne(predmetId);
        if(predmet == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        Nastavnik nastavnik = nastavnikService.findOne(nastavnikId);
        if(nastavnik == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        nastavnik.getPredmeti().remove(predmet);
        predmet.getNastavnici().remove(nastavnik);
        nastavnikService.save(nastavnik);
        predmetService.save(predmet);
        return new ResponseEntity(HttpStatus.NO_CONTENT);

    }

}
