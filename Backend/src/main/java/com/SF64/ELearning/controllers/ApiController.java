package com.SF64.ELearning.controllers;

import com.SF64.ELearning.dtos.NastavnikDto;
import com.SF64.ELearning.dtos.SifraDto;
import com.SF64.ELearning.dtos.StudentDto;
import com.SF64.ELearning.dtos.UserDto;
import com.SF64.ELearning.entity.Nastavnik;
import com.SF64.ELearning.entity.Student;
import com.SF64.ELearning.entity.User;
import com.SF64.ELearning.repository.UserRepo;
import com.SF64.ELearning.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/api")
public class ApiController {

    @Autowired
    UserRepo userRepo;

    @Autowired
    UserService userService;



    @GetMapping("/me")
    public ResponseEntity getMe(Principal principal) {
        User user = userRepo.findByKorisnickoIme(principal.getName());
        if(user instanceof Student) {
            return ResponseEntity.ok(new StudentDto((Student) user));
        }
        if(user instanceof Nastavnik) {
            return ResponseEntity.ok(new NastavnikDto((Nastavnik) user));
        }
        return ResponseEntity.ok(new UserDto(user));
    }

    @PutMapping("/me/change-password")
    public ResponseEntity changePassword(Principal principal,@RequestBody SifraDto sifraDto, Errors errors) {
        User user = userRepo.findByKorisnickoIme(principal.getName());

        if(user == null) {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        return userService.updatePassword(user, sifraDto);



    }

}
