package com.SF64.ELearning.controllers;

import com.SF64.ELearning.converter.IspitDtoToIspit;
import com.SF64.ELearning.converter.IspitToIspitNastavnikDto;
import com.SF64.ELearning.dtos.IspitDto;
import com.SF64.ELearning.dtos.PrijavaDto;
import com.SF64.ELearning.entity.Ispit;
import com.SF64.ELearning.entity.Nastavnik;
import com.SF64.ELearning.entity.Predmet;
import com.SF64.ELearning.services.IspitService;
import com.SF64.ELearning.services.NastavnikService;
import com.SF64.ELearning.services.PredmetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/ispiti")
public class IspitController {

    @Autowired
    IspitService ispitService;

    @Autowired
    IspitDtoToIspit ispitDtoToIspit;

    @Autowired
    PredmetService predmetService;

    @Autowired
    NastavnikService nastavnikService;

    @Autowired
    IspitToIspitNastavnikDto toIspitNastavnikDto;

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity getAll(@RequestParam(value = "page", defaultValue = "0") int page,
                                 @RequestParam(value = "num", defaultValue = Integer.MAX_VALUE+"") int num) {
        Page<Ispit> ispiti = ispitService.findAll(page,num);
        if (ispiti == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.set("total", String.valueOf(ispiti.getTotalPages()));
        return ResponseEntity.ok().body(ispiti.getContent().stream().map(IspitDto::new).collect(Collectors.toList()));
    }

    @GetMapping("/{id}")
    public ResponseEntity getOne(@PathVariable("id") long id) {

        Ispit ispit = ispitService.findOne(id);
        if (ispit == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok().body(new IspitDto(ispit));
    }

    @GetMapping("/{id}/prijave")
    @PreAuthorize("hasAnyRole('ADMIN','NASTAVNIK')")
    public ResponseEntity getPrijave(@PathVariable("id") long id){
        Ispit ispit = ispitService.findOne(id);
        if (ispit == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok().body(ispit.getPrijave().stream().map(PrijavaDto::new).collect(Collectors.toList()));
    }

    @GetMapping("/nastavnik-ispiti/{id}")
    @PreAuthorize("hasAnyRole('ADMIN','NASTAVNIK')")
    public ResponseEntity getIspitiNastavnik(@PathVariable("id") long id){
        Nastavnik nastavnik = nastavnikService.findOne(id);
        if(nastavnik == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok().body(toIspitNastavnikDto.convert(nastavnik.getPredmeti()));

    }



    @PostMapping
    @PreAuthorize("hasAnyRole('ADMIN','NASTAVNIK')")
    public ResponseEntity postOne(@Validated @RequestBody IspitDto dto, Errors errors) {

        if (errors.hasErrors()) {
            return new ResponseEntity(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
        }

        if (dto != null) {
            Predmet predmet = predmetService.findOne(dto.getPredmet());
            if (predmet == null) {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
            Ispit ispit = ispitDtoToIspit.convert(dto);
            ispitService.save(ispit);
            return new ResponseEntity(HttpStatus.CREATED);
        }

        return new ResponseEntity(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN','NASTAVNIK')")
    public ResponseEntity putOne(@PathVariable("id") long id, @Validated @RequestBody IspitDto dto, Errors errors) {
        if(errors.hasErrors()){
            return new ResponseEntity(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
        }
        if(dto.getId() != id) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        } else {
            Ispit ispit = ispitDtoToIspit.convert(dto);
            ispitService.save(ispit);
            return new ResponseEntity(HttpStatus.OK);
        }
    }



    @DeleteMapping("/{id}")
    public ResponseEntity deleteOne(@PathVariable long id) {
        Ispit ispit = ispitService.findOne(id);
        if (ispit == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        ispitService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
