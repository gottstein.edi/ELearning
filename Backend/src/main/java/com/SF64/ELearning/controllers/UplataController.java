package com.SF64.ELearning.controllers;

import com.SF64.ELearning.converter.UplataDtoToUplata;
import com.SF64.ELearning.dtos.UplataDto;
import com.SF64.ELearning.entity.Uplata;
import com.SF64.ELearning.services.UplataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/uplate")
public class UplataController {

    @Autowired
    UplataService uplataService;

    @Autowired
    UplataDtoToUplata uplataDtoToUplata;

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity getAll(){
        List<Uplata> uplate = uplataService.findAll();
        if(uplate == null || uplate.size() == 0) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.ok().body(uplate.stream().map(UplataDto::new).collect(Collectors.toList()));
    }

    @GetMapping("/{id}")
    public ResponseEntity getOne(@PathVariable("id") long id) {

        Uplata uplata = uplataService.findOne(id);
        if(uplata == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok().body(new UplataDto(uplata));
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ADMIN','STUDENT')")
    public ResponseEntity postOne(@Validated @RequestBody UplataDto dto, Errors errors){
        if(errors.hasErrors()) {
            return new ResponseEntity(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
        }
        if(dto != null) {
            Uplata uplata = uplataDtoToUplata.convert(dto);
            if(uplata.getIznos() == 0){
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
            uplataService.save(uplata);
            return new ResponseEntity(HttpStatus.CREATED);
        }
        else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }


    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteOne(@PathVariable long id) {
        Uplata uplata = uplataService.findOne(id);
        if(uplata == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        uplataService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
