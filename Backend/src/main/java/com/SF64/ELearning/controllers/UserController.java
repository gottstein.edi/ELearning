package com.SF64.ELearning.controllers;

import com.SF64.ELearning.converter.AdminDtoToAdmin;
import com.SF64.ELearning.dtos.AdminDto;
import com.SF64.ELearning.dtos.NastavnikDto;
import com.SF64.ELearning.dtos.StudentDto;
import com.SF64.ELearning.dtos.UserDto;
import com.SF64.ELearning.entity.Admin;
import com.SF64.ELearning.entity.Nastavnik;
import com.SF64.ELearning.entity.Student;
import com.SF64.ELearning.entity.User;
import com.SF64.ELearning.repository.UserRepo;
import com.SF64.ELearning.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    UserRepo userRepo;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    AdminDtoToAdmin toAdmin;


    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity getAll(@RequestParam(value = "ime", defaultValue = "") String ime, Pageable pageable){
        Page<User> korisnici = userService.findAll(ime, pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-Total", String.valueOf(korisnici.getTotalElements()));

        return ResponseEntity.ok().headers(headers).body(korisnici.getContent().stream().map(UserDto::new).collect(Collectors.toList()));
    }

    @GetMapping("/{id}")
    public ResponseEntity getOne(@PathVariable("id") long id) {

        User user = userService.findOne(id);
        if(user == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        if(user instanceof Nastavnik){
            return ResponseEntity.ok().body(new NastavnikDto((Nastavnik) user));
        }
        if(user instanceof Student) {
            return ResponseEntity.ok().body(new StudentDto((Student) user));
        }
        return ResponseEntity.ok().body(new UserDto(user));
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity addNewUser(@RequestBody Admin admin) {
        if(admin == null){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        admin.setSifra(encoder.encode(admin.getSifra()));
        userService.save(admin);
        return new ResponseEntity(HttpStatus.CREATED);

    }

    @GetMapping("/username-check/{username}")
    public ResponseEntity checkUsername(@PathVariable("username") String username){
        if(username == null && username != ""){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok().body(!userRepo.existsByKorisnickoIme(username));

    }

    @PutMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity updateAdmin(@RequestBody AdminDto adminDto, Principal principal){
        User user = userRepo.findByKorisnickoIme(principal.getName());
        if(adminDto != null){
            if(adminDto.getId() != 0 && adminDto.getId() == user.getId()){
                userService.save(toAdmin.convert(adminDto));
                return new ResponseEntity(HttpStatus.OK);
            }
        }

        return new ResponseEntity(HttpStatus.BAD_REQUEST);

    }


    @DeleteMapping("/{id}")
    public ResponseEntity deleteOne(@PathVariable long id) {
        User user = userService.findOne(id);
        if(user == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        userService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);

    }

}
