package com.SF64.ELearning.controllers;

import com.SF64.ELearning.converter.SmerDtoToSmer;
import com.SF64.ELearning.dtos.PredmetDto;
import com.SF64.ELearning.dtos.ListPostWrapper;
import com.SF64.ELearning.dtos.SmerDto;
import com.SF64.ELearning.dtos.StudentDto;
import com.SF64.ELearning.entity.Predmet;
import com.SF64.ELearning.entity.Smer;
import com.SF64.ELearning.entity.Student;
import com.SF64.ELearning.repository.PredmetRepo;
import com.SF64.ELearning.services.PredmetService;
import com.SF64.ELearning.services.SmerService;
import com.SF64.ELearning.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/smerovi")
public class SmerController {

    @Autowired
    SmerService smerService;

    @Autowired
    PredmetService predmetService;

    @Autowired
    StudentService studentService;

    @Autowired
    PredmetRepo predmetRepo;

    @Autowired
    SmerDtoToSmer smerDtoToSmer;

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity getAll(@RequestParam(value = "naziv", defaultValue = "") String naziv, Pageable pageable) {
        Page<Smer> smerovi = smerService.findAll(naziv, pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-Total", String.valueOf(smerovi.getTotalElements()));

        return ResponseEntity.ok().headers(headers).body(smerovi.getContent().stream().map(SmerDto::new).collect(Collectors.toList()));
    }
    @GetMapping("/{id}")
    public ResponseEntity getOne(@PathVariable("id") long id) {

        Smer smer = smerService.findOne(id);
        if(smer == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok().body(new SmerDto(smer));
    }

    @GetMapping("/{id}/studenti")
    @PreAuthorize("hasAnyRole('ADMIN','NASTAVNIK')")
    public ResponseEntity getAllStudents(@PathVariable("id") long id) {

        Smer smer = smerService.findOne(id);
        if(smer == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok().body(smer.getStudenti().stream().map(StudentDto::new).collect(Collectors.toList()));
    }

    @GetMapping("/{id}/predmeti")
    public ResponseEntity getPredmeti(@PathVariable("id") long id) {
        Smer smer = smerService.findOne(id);
        if(smer == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok().body(smer.getPredmeti().stream().map(PredmetDto::new).collect(Collectors.toList()));

    }

    @GetMapping("/{id}/predmeti-ne")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity getPredmetiSmerNeSadrzi(@PathVariable("id") long id) {
        Smer smer = smerService.findOne(id);
        if(smer == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        List<Predmet> predmetiSmerNeSadrzi = new ArrayList<>();
        predmetiSmerNeSadrzi.addAll(smer.getPredmeti());
        if(predmetiSmerNeSadrzi.size() == 0){
            return ResponseEntity.ok().body(predmetRepo.findAllByObrisano(false).stream().map(PredmetDto::new).collect(Collectors.toList()));
        }
        return ResponseEntity.ok().body(predmetService.findAllNotInSmer(predmetiSmerNeSadrzi).stream().map(PredmetDto::new).collect(Collectors.toList()));

    }

//    Dodeljivanje predmeta smeru
    @PostMapping("/{id}/predmeti")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity dodeljivanjePredmetaSmeru(@PathVariable("id") long id,@RequestBody ListPostWrapper dtoList) {
        Smer smer = smerService.findOne(id);

        if (smer == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        for (Integer predmetId : dtoList.getListItemIds()) {
            Predmet predmet = predmetService.findOne(predmetId.longValue());
            if (predmet != null) {
                smer.getPredmeti().add(predmet);
            }
        }
        smerService.save(smer);
        return new ResponseEntity(HttpStatus.OK);
    }

// Upis studenta na smer
    @PostMapping("/{id}/studenti")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity upisStudenataNaPredmet(@PathVariable("id") long id,@RequestBody ListPostWrapper dtoList) {
        Smer smer = smerService.findOne(id);
        if(smer == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        for(Integer studentId : dtoList.getListItemIds()){
            Student student = studentService.findOne(studentId.longValue());

            if(student != null) {
                student.setSmer(smer);
                studentService.save(student);
            }
        }


        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity postOne(@Validated @RequestBody SmerDto dto, Errors errors){
        if(errors.hasErrors()) {
            return new ResponseEntity(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
        }
        if(dto != null) {
            Smer smer = smerDtoToSmer.convert(dto);
            smerService.save(smer);
            return new ResponseEntity(HttpStatus.CREATED);
        }
        else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity updateOne(@PathVariable("id") long id, @Validated @RequestBody SmerDto dto, Errors errors){
        if (errors.hasErrors()){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        if(dto != null) {
            if(dto.getId() ==  id && dto.getId() != 0){
                smerService.save(smerDtoToSmer.update(dto));
                return new ResponseEntity(HttpStatus.OK);
            }

        }

        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/{id}/studenti/{studId}")
    public ResponseEntity otklanjanjeStudentaSaSmera(@PathVariable("id") long smerId, @PathVariable("studId") long studentId){
        Smer smer = smerService.findOne(smerId);
        if(smer == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        Student student = studentService.findOne(studentId);
        if(student == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        student.setSmer(null);
        studentService.save(student);
        smer.getStudenti().remove(student);
        smerService.save(smer);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}/predmeti/{predmetId}")
    public ResponseEntity otklanjanjePredmetaSaSmera(@PathVariable("id") long smerId, @PathVariable("predmetId") long predmetId){
        Smer smer = smerService.findOne(smerId);
        if(smer == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        Predmet predmet = predmetService.findOne(predmetId);
        if(predmet == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        smer.getPredmeti().remove(predmet);
        smerService.save(smer);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteOne(@PathVariable long id) {
        Smer smer = smerService.findOne(id);
        if(smer == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        smerService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }


}
