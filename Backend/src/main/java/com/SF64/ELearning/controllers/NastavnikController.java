package com.SF64.ELearning.controllers;

import com.SF64.ELearning.converter.NastavnikDtoToNastavnik;
import com.SF64.ELearning.dtos.NastavnikDto;
import com.SF64.ELearning.dtos.PredmetDto;
import com.SF64.ELearning.entity.Nastavnik;
import com.SF64.ELearning.entity.Predmet;
import com.SF64.ELearning.services.NastavnikService;
import com.SF64.ELearning.services.PredmetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/nastavnici")
public class NastavnikController {

    @Autowired
    NastavnikService nastavnikService;

    @Autowired
    NastavnikDtoToNastavnik toNastavnik;



    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity getAll(){
        List<Nastavnik> nastavnici = nastavnikService.findAll();
        if(nastavnici == null || nastavnici.size() == 0) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.ok().body(nastavnici.stream().map(NastavnikDto::new).collect(Collectors.toList()));
    }


    @GetMapping("/{id}")
    public ResponseEntity getOne(@PathVariable("id") long id) {

        Nastavnik nastavnik = nastavnikService.findOne(id);
        if (nastavnik == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok().body(new NastavnikDto(nastavnik));
    }


    @GetMapping("/{id}/predmeti")
    @PreAuthorize("hasAnyRole('ADMIN','NASTAVNIK')")
    public ResponseEntity getAllPredmeti(@PathVariable("id") long id){
        Nastavnik nastavnik = nastavnikService.findOne(id);
        if(nastavnik == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok().body(nastavnik.getPredmeti().stream().map(PredmetDto::new).collect(Collectors.toList()));

    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity postOne(@Validated @RequestBody NastavnikDto dto, Errors errors){
        if(errors.hasErrors()) {
            return new ResponseEntity(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
        }
        if(dto != null) {

            nastavnikService.save(toNastavnik.convert(dto));
            return new ResponseEntity(HttpStatus.CREATED);
        }
        else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity updateOne(@PathVariable("id") long id, @Validated @RequestBody NastavnikDto nastavnikDto, Errors errors){
        if(errors.hasErrors()){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        if( nastavnikDto != null){
            if(nastavnikDto.getId() == id && nastavnikDto.getId() != 0){

                nastavnikService.save(toNastavnik.update(nastavnikDto));
                return new ResponseEntity(HttpStatus.OK);
            }
        }

        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }





    @DeleteMapping("/{id}")
    public ResponseEntity deleteOne(@PathVariable long id) {
        Nastavnik nastavnik = nastavnikService.findOne(id);
        if (nastavnik == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        nastavnikService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
