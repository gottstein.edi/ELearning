package com.SF64.ELearning.repository;

import com.SF64.ELearning.entity.Nastavnik;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NastavnikRepo extends JpaRepository<Nastavnik, Long> {
    List<Nastavnik> findAllByObrisano(boolean obrisano);
    Nastavnik findByObrisanoAndId(boolean obrisano,Long id);
    List<Nastavnik> findAllByIdNotInAndObrisano ( List<Long> idNastavnikaNaPredmetu, boolean obrisano);



}
