package com.SF64.ELearning.repository;

import com.SF64.ELearning.entity.Ispit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IspitRepo extends JpaRepository<Ispit, Long> {
    Page<Ispit> findAllByObrisano(boolean obrisano, Pageable pageable);
    Ispit findByObrisanoAndId(boolean obrisano,Long id);


}
