package com.SF64.ELearning.repository;

import com.SF64.ELearning.entity.Smer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SmerRepo extends JpaRepository<Smer, Long> {
    Page<Smer> findAllByNazivIgnoreCaseContainsAndObrisano(String naziv, Pageable pageable, boolean Obrisano);
    Smer findByObrisanoAndId(boolean obrisano,Long id);
}
