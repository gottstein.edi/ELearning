package com.SF64.ELearning.repository;

import com.SF64.ELearning.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {
    User findByKorisnickoIme(String korisnickoIme);
    User findByObrisanoAndId(boolean obrisano, Long id);
    Page<User> findAllByImeIgnoreCaseContainsAndObrisano(String ime, Pageable pageable, boolean obrisano);
    boolean existsByKorisnickoIme(String korisnickoIme);

}
