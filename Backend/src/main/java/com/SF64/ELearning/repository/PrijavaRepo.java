package com.SF64.ELearning.repository;

import com.SF64.ELearning.entity.Prijava;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PrijavaRepo extends JpaRepository<Prijava, Long> {
    List<Prijava> findAllByObrisano(boolean obrisano);
    Prijava findByObrisanoAndId(boolean obrisano,Long id);
    List<Prijava> findAllByStudent_idAndObrisano(Long id, boolean obriasno);
    Prijava findByStudent_idAndIspit_id(Long studentId, Long ispitId);
}
