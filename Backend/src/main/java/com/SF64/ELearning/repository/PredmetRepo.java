package com.SF64.ELearning.repository;

import com.SF64.ELearning.entity.Predmet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PredmetRepo extends JpaRepository<Predmet, Long> {
    Page<Predmet> findAllByNazivIgnoreCaseContainsAndObrisano(String naziv,Pageable pageable, boolean Obrisano);
    Predmet findByObrisanoAndId(boolean obrisano,Long id);
    List<Predmet> findAllByIdNotInAndObrisano(List<Long> idPredmetaNaSmeru, boolean obrisano);
    List<Predmet> findAllByObrisano(boolean obrisano);


}
