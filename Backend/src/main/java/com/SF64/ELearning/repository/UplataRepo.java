package com.SF64.ELearning.repository;

import com.SF64.ELearning.entity.Uplata;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UplataRepo extends JpaRepository<Uplata, Long> {
    Page<Uplata> findAllByStudent_idAndObrisano (Pageable pageable,Long id, boolean obrisano);
    List<Uplata> findAllByObrisano(boolean obrisano);
    Uplata findByObrisanoAndId(boolean obrisano,Long id);
}
