package com.SF64.ELearning.repository;

import com.SF64.ELearning.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepo extends JpaRepository<Role, Long> {
}
