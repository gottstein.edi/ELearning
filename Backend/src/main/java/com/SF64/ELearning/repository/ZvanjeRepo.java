package com.SF64.ELearning.repository;

import com.SF64.ELearning.entity.Zvanje;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ZvanjeRepo extends JpaRepository<Zvanje, Long> {
    List<Zvanje> findAllByObrisano(boolean obrisano);
    Zvanje findByObrisanoAndId(boolean obrisano,Long id);

}
