package com.SF64.ELearning.repository;

import com.SF64.ELearning.entity.PolaganjeIspita;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PolaganjeIspitaRepo extends JpaRepository<PolaganjeIspita, Long> {
    List<PolaganjeIspita> findAllByObrisano(boolean obrisano);
    PolaganjeIspita findByObrisanoAndId(boolean obrisano,Long id);
    List<PolaganjeIspita> findAllByStudent_idAndPolozio(Long id, boolean polozeno);
    PolaganjeIspita findByStudent_idAndPredmet_idAndPolozio(Long studentId,Long predmetId, boolean polozeno);


}
