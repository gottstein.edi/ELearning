package com.SF64.ELearning.repository;

import com.SF64.ELearning.entity.Dokument;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DokumentRepo extends JpaRepository<Dokument, Long> {
    List<Dokument> findAllByObrisano(boolean obrisano);
    List<Dokument> findAllByStudent_idAndObrisano(Long id, boolean obrisano);
    Dokument findByObrisanoAndId(boolean obrisano,Long id);

}
