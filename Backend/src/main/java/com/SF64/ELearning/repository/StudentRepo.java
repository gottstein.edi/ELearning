package com.SF64.ELearning.repository;

import com.SF64.ELearning.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepo extends JpaRepository<Student, Long> {
    List<Student> findAllByObrisano(boolean obrisano);
    Student findByObrisanoAndId(boolean obrisano,Long id);
    List<Student> findBySmerIsNullAndObrisano(boolean obrisano);
}
