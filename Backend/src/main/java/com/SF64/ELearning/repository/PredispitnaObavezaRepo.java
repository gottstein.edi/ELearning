package com.SF64.ELearning.repository;

import com.SF64.ELearning.entity.PredispitnaObaveza;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PredispitnaObavezaRepo extends JpaRepository<PredispitnaObaveza, Long> {
    List<PredispitnaObaveza> findAllByObrisano(boolean obrisano);
    PredispitnaObaveza findByObrisanoAndId(boolean obrisano,Long id);

}
