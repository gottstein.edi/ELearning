import { Component, OnInit } from '@angular/core';
import { Predmet } from '../../model/predmet.model';
import { PredmetService } from '../../services/predmet.service';
import { PredispitneObavezeInterface } from '../../model/predispitne-obaveze.model';
import { Router } from '../../../../node_modules/@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-add-predmet',
  templateUrl: './add-predmet.component.html',
  styleUrls: ['./add-predmet.component.css']
})
export class AddPredmetComponent implements OnInit {

  predmetPost: Predmet = {
    naziv: null,
    oznaka: null,
    espb: 2,
    predispitnaObaveza: 1,
  }

  predispitneObaveze: PredispitneObavezeInterface[] = []

  constructor(private _predmetService: PredmetService,private _router:Router, private _location:Location) { }

  ngOnInit() {
    this._predmetService.getPredispitneObaveze().subscribe(data => {
      this.predispitneObaveze = data;
      
    })
  }

  onBack(){
    this._location.back();
  }

  onAdd(){
    this._predmetService.addPredmet(this.predmetPost).subscribe( () => {
      
    });
  }

}
