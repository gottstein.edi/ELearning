import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '../../../node_modules/@angular/router';
import { PredmetService } from '../services/predmet.service';
import { Predmet } from '../model/predmet.model';
import { NgbModal } from '../../../node_modules/@ng-bootstrap/ng-bootstrap';
import { NastavnikInterface } from '../model/nastavnik.model';
import { Location } from '@angular/common';
import { AuthenticationService } from '../security/authentication.service';

@Component({
  selector: 'app-predmet-detail',
  templateUrl: './predmet-detail.component.html',
  styleUrls: ['./predmet-detail.component.css']
})
export class PredmetDetailComponent implements OnInit {

  predmetId;
  predmet:Predmet;
  nastavnici:NastavnikInterface[]= [];
  checkboxNastavnici:{nastavnik: NastavnikInterface, checked: boolean}[] = [];

  listaNastavnikaZaPrijaveu = {
    listItemIds: []
  }

  roleLoggedIn: string;
  predmetCopy:Predmet;

  constructor(private _router: Router, private route: ActivatedRoute, private predmetService: PredmetService, private modalService:NgbModal,private _location:Location, 
    private _authService:AuthenticationService) { }

  ngOnInit() {
    this.predmetId = +this.route.snapshot.paramMap.get('id');
    this.predmetService.getPredmet(this.predmetId).subscribe(predmet =>
    this.predmet = predmet);
    this.roleLoggedIn = this._authService.roleLogedIn();
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      
    }, (reason) => {
      
    });
  }

  onBack(){
    this._location.back();
  }

  deletePredmet(){
    this.predmetService.deletePredmet(this.predmetId).subscribe( () => {});
  
    this._router.navigate(['/predmeti']);
  }

  tabChanged(event: any){
    
    if(event.nextId == "predavaciPredmeta"){
      this.ucitajProfesoreNaPredmetu();
    }
    
  }

  ucitajProfesoreNaPredmetu(){
    this.predmetService.getPredmetNastavnici(this.predmetId).subscribe(data => {
      this.nastavnici = data;
    })
  }

  onDodajPredavaca(){
    this.checkboxNastavnici = [];
    this.predmetService.getPredmetNastavniciNe(this.predmetId).subscribe(data => {
      data.forEach(item => {
        
        this.checkboxNastavnici.push({nastavnik: item, checked: false});
      })
    })
  }

  dodajIzabraneNastavnike(){
    this.checkboxNastavnici.forEach(value => {
      if(value.checked){
        this.listaNastavnikaZaPrijaveu.listItemIds.push(value.nastavnik.id)
      }
    })

    this.predmetService.dodeljivanjeNastavnikaPredmetu(this.predmetId, this.listaNastavnikaZaPrijaveu).subscribe(data => {
      this.listaNastavnikaZaPrijaveu.listItemIds = [];
      this.ucitajProfesoreNaPredmetu();
    })
  }




  //UPDATE PREDMET

  onPredmetUpdate(){
    this.predmetCopy = Object.assign({}, this.predmet);
  }

  onUpdateCancel(){
    this.predmetCopy = null;
  }

  onUpdateConfirm(){
    this.predmetService.updatePredmet(this.predmetId, this.predmetCopy).subscribe(data => {
      this.predmetService.getPredmet(this.predmetId).subscribe(predmet =>
      this.predmet = predmet);
    })
  }


  //UPDATE PREDMET END
  
  onNastavnikRemove(nastavnikIndeks:number){
    this.predmetService.removeNastavnikFromPRedmet(this.predmetId, this.nastavnici[nastavnikIndeks].id).subscribe(data => {
      this.ucitajProfesoreNaPredmetu();
    })
  }


}
