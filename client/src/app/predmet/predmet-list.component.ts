import { Component, OnInit } from '@angular/core';
import { PredmetService } from '../services/predmet.service';
import { Predmet } from '../model/predmet.model';
import { Router } from '../../../node_modules/@angular/router';
import { AuthenticationService } from '../security/authentication.service';

@Component({
  selector: 'app-predmet-list',
  templateUrl: './predmet-list.component.html',
  styleUrls: ['./predmet-list.component.css']
})
export class PredmetListComponent implements OnInit {

  page:number = 1;
  size: number = 5;
  total: number;
  filter: string = "";

  nastavnikId:number;
  loggedInUserRole: string;
  

  predmeti: Predmet[] = [];

  constructor(private _predmetService: PredmetService, private _rotuer:Router) { }

  ngOnInit() {
    
    this.refreshData();
    
  }


  refreshData() {

    this._predmetService.getAll(this.filter, this.page-1, this.size).subscribe(data =>{
      this.total = Number(data.headers.get('X-Total'));
      this.predmeti = data.body;
    }, error => this.predmeti = null);
    
  }

  onPageChange(event: number){
    this.page = event;
    this.refreshData();
  }

  onSelectChange(){
    this.refreshData();
  }

  onSearchChange(){
    this.refreshData();
  }

  onAddPredmet(){
    this._rotuer.navigate(['/predmeti-add'])
  }

}
