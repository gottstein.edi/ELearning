import { Component, OnChanges, OnInit, Input, SimpleChanges } from '@angular/core';
import { Predmet } from '../../../model/predmet.model';
import { PredmetService } from '../../../services/predmet.service';
import { IspitInterface } from '../../../model/ispit.model';
import { IspitService } from '../../../services/ispit.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-nastavnik-predmet-details',
  templateUrl: './nastavnik-predmet-details.component.html',
  styleUrls: ['./nastavnik-predmet-details.component.css']
})
export class NastavnikPredmetDetailsComponent implements OnInit,OnChanges {
  @Input()
  predmetId: number = null;

  predmet:Predmet = null;
  ispiti: IspitInterface[] = [];

  ispitPost:IspitInterface = {
    predmet : null,
    datumOdrzavanja: null,
    rokZaPrijavu: null
    
  }

  constructor(private _predmetService: PredmetService, private modalService:NgbModal, private _ispitService:IspitService) { }

  ngOnInit() {

  }

  

  ngOnChanges(changes: SimpleChanges){
    if(this.predmetId != null){
      this.refreshPredmet();
      this.refreshIspiti();
    }
  }

  tabChanged(event: any){
    if(event.nextId = "ispiti"){
      this.refreshIspiti();
    }
  }

  refreshIspiti(){
    this._predmetService.getPredmetIspiti(this.predmetId).subscribe(data => {
      this.ispiti = data;
    })
  }

  refreshPredmet(){
    this._predmetService.getPredmet(this.predmetId).subscribe(data => {
      this.predmet = data;
    })
  }

  dodajIspit(){

  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      
    }, (reason) => {
      
    });
  }

  onDatumOdrzavanjaSelect(evt: any){
    this.ispitPost.datumOdrzavanja = new Date(evt.year,evt.month-1, evt.day);
  }

  onRokZaPrijavuSelect(evt: any){
    this.ispitPost.rokZaPrijavu = new Date(evt.year, evt.month-1, evt.day);
  }

  onDodajIspit(){
    this.ispitPost.predmet = this.predmetId;
    this._ispitService.addIspit(this.ispitPost).subscribe(data => {
      this.refreshIspiti();
    })
  }

}
