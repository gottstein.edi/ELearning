import { Component, OnInit } from '@angular/core';
import { Predmet } from '../../model/predmet.model';
import { PredmetService } from '../../services/predmet.service';
import { AuthenticationService } from '../../security/authentication.service';

@Component({
  selector: 'app-nastavnik-predmet-list',
  templateUrl: './nastavnik-predmet-list.component.html',
  styleUrls: ['./nastavnik-predmet-list.component.css']
})
export class NastavnikPredmetListComponent implements OnInit {

  nastavnikId: number;
  selectedPredmetId: number;

  predmeti: Predmet[] = [];

  constructor(private _predmetService: PredmetService, private _authService: AuthenticationService) { }

  ngOnInit() {
    this.nastavnikId = this._authService.loggedInUserId();
    this._predmetService.getNastavnikPredmeti(this.nastavnikId).subscribe( data => {
      this.predmeti = data;
    })
  }

  onPredmetClick(index: number) {
    this.selectedPredmetId = this.predmeti[index].id;
  }

}
