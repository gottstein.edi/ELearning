import { Component, OnInit } from '@angular/core';
import { PredmetService } from '../../services/predmet.service';
import { Predmet } from '../../model/predmet.model';
import { AuthenticationService } from '../../security/authentication.service';
import { StudentService } from '../../services/student.service';

@Component({
  selector: 'app-predmeti-student-view',
  templateUrl: './predmeti-student-view.component.html',
  styleUrls: ['./predmeti-student-view.component.css']
})
export class PredmetiStudentViewComponent implements OnInit {

  constructor(private _predmetService: PredmetService, private _authService: AuthenticationService, private _studentService: StudentService) { }

  studentId: number;
  selectedPredmetId: number;
  selectPredmetTip: number = 2;

  
  predmeti: Predmet[] = [];

  ngOnInit() {
    this.studentId = this._authService.loggedInUserId();
    this.refreshPredmetiData();
  }

  onPredmetClick(index: number) {
    this.selectedPredmetId = this.predmeti[index].id;
  }


  onSelectChange(tipPredmeta: number){
    this.selectPredmetTip = tipPredmeta;
    this.selectedPredmetId = undefined;
    this.refreshPredmetiData();
    
  }

  refreshPredmetiData(){
    if(this.selectPredmetTip == 2){
      this._studentService.getNePolozeniPredmeti(this.studentId).subscribe(data => {
        this.predmeti = data;
      })
    }
    else{
      this._studentService.getPolozeniPredmeti(this.studentId).subscribe(data => {
        this.predmeti = data;
      })
    }
  }


}
