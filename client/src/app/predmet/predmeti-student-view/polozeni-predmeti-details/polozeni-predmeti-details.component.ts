import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { PolaganjeService } from '../../../services/polaganje.service';
import { PolaganjePostInterface } from '../../../model/polaganje.model';
import { NastavnikInterface } from '../../../model/nastavnik.model';

@Component({
  selector: 'app-polozeni-predmeti-details',
  templateUrl: './polozeni-predmeti-details.component.html',
  styleUrls: ['./polozeni-predmeti-details.component.css']
})
export class PolozeniPredmetiDetailsComponent implements OnInit,OnChanges {

  @Input()
  predmetId: number = null;

  @Input()
  studentId: number;

  profesorIspita:NastavnikInterface;

  polaganje: PolaganjePostInterface = null;

  constructor(private _polaganjeService:PolaganjeService) { }

  ngOnInit() {
    if(this.predmetId != null){
      this.refreshData();
    }
    
  }

  ngOnChanges(changes: SimpleChanges){
    if(this.predmetId != null){
      this.refreshData();
    }

  }

  refreshData(){
    this._polaganjeService.getPolaganjeByStudentAndPredmet(this.studentId, this.predmetId).subscribe(data => {
      this.polaganje = data;
    })
  }
}
