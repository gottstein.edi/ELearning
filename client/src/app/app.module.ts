import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { MainpageComponent } from './mainpage/mainpage.component';
import { LoginComponent } from './login/login.component';
import { AuthenticationService } from './security/authentication.service';
import { SecurityGuard } from './security/security-guard.guard';
import { TokenInterceptorService } from './security/token-interceptor.service';
import { UserService } from './services/user.service';
import { NavigationComponent } from './navigation/navigation.component';
import { PredmetListComponent } from './predmet/predmet-list.component';
import { PredmetDetailComponent } from './predmet/predmet-detail.component';
import { UserListComponent } from './users/user-list.component';
import { UserDetailComponent } from './users/user-detail.component';
import { PredmetService } from './services/predmet.service';
import { SmerListComponent } from './smer/smer-list.component';
import { SmerDetailComponent } from './smer/smer-detail.component';
import { ProfileDetailComponent } from './profile/profile-detail.component';
import { SmerService } from './services/smer.service';
import { StudentDetailsComponent } from './users/student-details/student-details.component';
import { NastavnikDetailsComponent } from './users/nastavnik-details/nastavnik-details.component';
import { AdminDetailsComponent } from './users/admin-details/admin-details.component';
import { UplateComponent } from './uplate/uplate.component';
import { StudentService } from './services/student.service';
import { AddUserComponent } from './users/add-user/add-user.component';
import { AddSmerComponent } from './smer/add-smer/add-smer.component';
import { AddPredmetComponent } from './predmet/add-predmet/add-predmet.component';
import { NastavnikService } from './services/nastavnik.service';
import { PredmetiStudentViewComponent } from './predmet/predmeti-student-view/predmeti-student-view.component';
import { IspitListViewComponent } from './ispit-list-view/ispit-list-view.component';
import { IspitService } from './services/ispit.service';
import { AdminUserAddComponent } from './users/add-user/admin-user-add/admin-user-add.component';
import { StudentUserAddComponent } from './users/add-user/student-user-add/student-user-add.component';
import { NastavnikUserAddComponent } from './users/add-user/nastavnik-user-add/nastavnik-user-add.component';
import { DokumentListComponent } from './dokumenti/dokument-list/dokument-list.component';
import { DokumentService } from './services/dokument.service';
import { PrijavaService } from './services/prijava.service';
import { FileService } from './services/file.service';
import { NastavnikPredmetListComponent } from './predmet/nastavnik-predmet-list/nastavnik-predmet-list.component';
import { NastavnikPredmetDetailsComponent } from './predmet/nastavnik-predmet-list/nastavnik-predmet-details/nastavnik-predmet-details.component';
import { IspitPrijaveListComponent } from './ispit-list-view/ispit-prijave-list/ispit-prijave-list.component';
import { NastavnikStudentViewComponent } from './users/student-details/nastavnik-student-view/nastavnik-student-view.component';
import { PolaganjeService } from './services/polaganje.service';
import { PolozeniPredmetiDetailsComponent } from './predmet/predmeti-student-view/polozeni-predmeti-details/polozeni-predmeti-details.component';
import { UnauthorizedAccessComponent } from './unauthorized-access/unauthorized-access.component';
import { AppRoutingModule } from './app-routing.module';
import { AdminGuard } from './security/admin-guard.guard';
import { NastavnikOrAdminGuard } from './security/nastavnik-admin-guard.guard';
import { StudentOrAdminGuard } from './security/admin-student-guard.guard';
import { PolaganjaComponent } from './polaganja/polaganja.component';
import { IspitiListNastavnikComponent } from './ispiti-list-nastavnik/ispiti-list-nastavnik.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainpageComponent,
    NavigationComponent,
    PredmetListComponent,
    PredmetDetailComponent,
    UserListComponent,
    UserDetailComponent,
    SmerListComponent,
    SmerDetailComponent,
    ProfileDetailComponent,
    StudentDetailsComponent,
    NastavnikDetailsComponent,
    AdminDetailsComponent,
    UplateComponent,
    AddUserComponent,
    AddSmerComponent,
    AddPredmetComponent,
    PredmetiStudentViewComponent,
    IspitListViewComponent,
    AdminUserAddComponent,
    StudentUserAddComponent,
    NastavnikUserAddComponent,
    DokumentListComponent,
    NastavnikPredmetListComponent,
    NastavnikPredmetDetailsComponent,
    IspitPrijaveListComponent,
    NastavnikStudentViewComponent,
    PolozeniPredmetiDetailsComponent,
    UnauthorizedAccessComponent,
    PolaganjaComponent,
    IspitiListNastavnikComponent,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    NgbModule.forRoot(),
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [AuthenticationService,
    AdminGuard,
    NastavnikOrAdminGuard,
    SecurityGuard,
    StudentOrAdminGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    },
    UserService,
    PredmetService,
    SmerService,
    StudentService,
    NastavnikService,
    IspitService,
    PolaganjeService,
    DokumentService,
    PrijavaService,
    FileService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
