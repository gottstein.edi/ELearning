import { Component, OnInit } from '@angular/core';
import { DokumentInterface, DokumentPostInterface } from '../../model/dokument.model';
import { StudentService } from '../../services/student.service';
import { UserService } from '../../services/user.service';
import { NgbModal } from '../../../../node_modules/@ng-bootstrap/ng-bootstrap';
import { DokumentService } from '../../services/dokument.service';
import { FileService } from '../../services/file.service';

@Component({
  selector: 'app-dokument-list',
  templateUrl: './dokument-list.component.html',
  styleUrls: ['./dokument-list.component.css']
})
export class DokumentListComponent implements OnInit {

  dokumenti: DokumentInterface[] = [];
  studentId: number;
  fileList: FileList = null;
  dokumentPost: DokumentPostInterface = {
    naziv: null,
    student: null
  }

  constructor(private _studentService: StudentService, private _userService:UserService, private modalService:NgbModal, private _dokumentService: DokumentService,
  private _fileService:FileService) { }

  ngOnInit() {

    this._userService.me().subscribe(data => {
      this.studentId = data.id;
      this._studentService.getStudentDokumenti(this.studentId).subscribe(data =>{
        this.dokumenti = data;
      })
    })
    
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      
    }, (reason) => {
      
    });
  }



  uploadDokument(){
    if (this.fileList.length === 1){
      const file = this.fileList[0];
      this.dokumentPost.student = this.studentId;
      this._dokumentService.dodavanjeDokumenta(file, this.dokumentPost).subscribe(() => {
        this._studentService.getStudentDokumenti(this.studentId).subscribe(data =>{
          this.dokumenti = data;
        })
      })
      
    }
  }

  onFileSelected(e){
    this.fileList = e.target.files;
    if(this.fileList.length === 0){
      this.fileList = null;
    }
    
  }

  onDownloadDocument(filename:string){
    this._fileService.downloadFile(filename);
  }

  onDeleteDocument(docId: number){
    this._dokumentService.deleteDocument(docId).subscribe(()=>{
      this._studentService.getStudentDokumenti(this.studentId).subscribe(data =>{
        this.dokumenti = data;
      })
    })
  }

}
