import { Component, OnInit } from '@angular/core';
import { IspitNastavnikInterface } from '../model/ispitNastavnik.model';
import { AuthenticationService } from '../security/authentication.service';
import { IspitService } from '../services/ispit.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-ispiti-list-nastavnik',
  templateUrl: './ispiti-list-nastavnik.component.html',
  styleUrls: ['./ispiti-list-nastavnik.component.css']
})
export class IspitiListNastavnikComponent implements OnInit {

  ispiti: IspitNastavnikInterface[] = [];
  nastavnikId:number;

  constructor(private _authenticatoinService: AuthenticationService, private _ispitService: IspitService, private location: Location) { }

  ngOnInit() {
    this.nastavnikId = this._authenticatoinService.loggedInUserId();
    this._ispitService.getIspitiByNastavnik(this.nastavnikId).subscribe(data => {
      this.ispiti = data;
    })
  }

  onBack(){
    this.location.back();
  }

}
