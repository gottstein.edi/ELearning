import { Injectable } from "../../../node_modules/@angular/core";
import { HttpClient } from "../../../node_modules/@angular/common/http";
import { Observable } from "../../../node_modules/rxjs";
import { ZvanjeInterface } from "../model/zvanje.model";
import { NastavnikInterface } from "../model/nastavnik.model";

@Injectable()
export class NastavnikService{

    private readonly apiNastavnici = `api/nastavnici`;
    private readonly apiZvanja = `api/zvanja`;

    constructor(private _http:HttpClient) { }

    

    getZvanja():Observable<ZvanjeInterface[]>{
        return this._http.get<ZvanjeInterface[]>(this.apiZvanja);
    }

    getZvanje(zvanjeId: number):Observable<ZvanjeInterface> {
        return this._http.get<ZvanjeInterface>(`${this.apiZvanja}/${zvanjeId}`)
    }

    updateNastavnik(nastavnikId: number, nastavnik: NastavnikInterface):Observable<any>{
        return this._http.put(`${this.apiNastavnici}/${nastavnikId}`, nastavnik);
    }

}