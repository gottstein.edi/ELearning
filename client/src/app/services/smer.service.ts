import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from "rxjs/Observable";
import { Smer } from '../model/smer.mode';
import { Predmet } from '../model/predmet.model';
import { StudentInterface } from '../model/student.model';

@Injectable()
export class SmerService {

  private readonly apiSmerovi = `api/smerovi`;

  constructor(private _http:HttpClient) { }

  getAll(naziv: string, page: number, size: number):Observable<HttpResponse<Smer[]>>{
    naziv = naziv==undefined? '': naziv;
    page = page==undefined? 0: page;
    size = size==undefined? 5: size;
    return this._http.get<Smer[]>(`${this.apiSmerovi}?naziv=${naziv}&page=${page}&size=${size}`, 
    {observe: 'response'});
  }

  getSmer(smerId: number):Observable<Smer>{
    return this._http.get<Smer>(this.apiSmerovi + '/' + smerId);
  }

  getPredmeti(smerId: number):Observable<Predmet[]>{
    return this._http.get<Predmet[]>(this.apiSmerovi + '/' + smerId + '/predmeti');
  }

  getPredmetiSmerNeSadrzi(smerId: number):Observable<Predmet[]>{
    return this._http.get<Predmet[]>(`${this.apiSmerovi}/${smerId}/predmeti-ne`)
  }

  addSmer(smer: Smer):Observable<any>{
    return this._http.post(`${this.apiSmerovi}`, smer);
  }

  getStudentiNaSmeru(smerId: number):Observable<StudentInterface[]>{
    return this._http.get<StudentInterface[]>(this.apiSmerovi + '/' + smerId + '/studenti');
  }

  deleteSmer(smerId: number):Observable<any>{
    return this._http.delete(`${this.apiSmerovi}/${smerId}`)
  }

  dodavanjePredmeta(smerId: number, listaPredmetaId:any){
    return this._http.post(this.apiSmerovi + "/" + `${smerId}/predmeti`,listaPredmetaId);
  }

  upistStudenata(smerId: number, listaStudenataId:any) {
    return this._http.post(this.apiSmerovi + "/" + `${smerId}/studenti`, listaStudenataId);
  }

  updateSmer(smerId: number, smer:Smer):Observable<any> {
    return this._http.put(`${this.apiSmerovi}/${smerId}`, smer);
  }

  removeStudentFromSmer(smerId: number, studentId: number):Observable<any>{
    return this._http.delete(`${this.apiSmerovi}/${smerId}/studenti/${studentId}`);
  }

  removePredmetFromSmer(smerId: number, predmetId: number):Observable<any>{
    return this._http.delete(`${this.apiSmerovi}/${smerId}/predmeti/${predmetId}`);
  }

}
