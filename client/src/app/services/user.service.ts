import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from "rxjs/Observable";
import { UserInterface, UserPostInterface } from '../model/user.model';
import { NastavnikInterface, NastavnikPostInterface } from '../model/nastavnik.model';
import { StudentInterface, StudentPostInterface } from '../model/student.model';
import { Pagable } from './pagable.interface';
import { ZvanjeInterface } from '../model/zvanje.model';
import { PasswordInterface } from '../model/password.model';



@Injectable()
export class UserService {

  private readonly apiMe = `api/me`;
  private readonly usersApi = `api/users`;
  private readonly nastavniciApi = `api/nastavnici`;
  private readonly studentiApi = `/api/studenti`;
  private readonly zvanjaApi = `/api/zvanja`;


    constructor(private _http: HttpClient){

    }

  me():Observable<UserInterface | NastavnikInterface | StudentInterface>{
    return this._http.get<UserInterface | NastavnikInterface | StudentInterface>(this.apiMe);
  }

  getAll(ime: string, page: number, size: number):Observable<HttpResponse<UserInterface[]>>{
    ime = ime==undefined? '': ime;
    page = page==undefined? 0: page;
    size = size==undefined? 5: size;
    return this._http.get<UserInterface[]>(`${this.usersApi}?ime=${ime}&page=${page}&size=${size}`, 
    {observe: 'response'});

    
  }

  getUser(userId: number):Observable<UserInterface | StudentInterface | NastavnikInterface>{
    
    return this._http.get<UserInterface | StudentInterface | NastavnikInterface>(this.usersApi+'/'+userId);
  }

  getNastavnik(nastavnikId: number):Observable<NastavnikInterface>{
    return this._http.get<NastavnikInterface>(this.nastavniciApi+ '/' + nastavnikId);
  }

  getStudent(studentId: number):Observable<StudentInterface>{
    return this._http.get<StudentInterface>(this.studentiApi+ '/' + studentId);
  }

  addUser(user: UserPostInterface):Observable<any>{
    return this._http.post(`${this.usersApi}`, user);
  }

  deleteUser(userId: number):Observable<any>{
    return this._http.delete(`${this.usersApi}/${userId}`);
  }

  addNastavnik(nastavnik: NastavnikPostInterface):Observable<any>{
    return this._http.post(`${this.nastavniciApi}`, nastavnik);
  }

  addStudent(student: StudentPostInterface):Observable<any>{
    return this._http.post(`${this.studentiApi}`, student);
  }

  changePassword(passwordPut: PasswordInterface):Observable<any>{
    return this._http.put(`${this.apiMe}/change-password`, passwordPut);
  }
  

  checkIfUsernameIsUnique(username: string):Observable<boolean>{
    return this._http.get<boolean>(`${this.usersApi}/username-check/${username}`);
  }

  updateAdminProfile(admin: UserPostInterface):Observable<any> {
    return this._http.put(`${this.usersApi}`, admin);
  }

}

