import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from "rxjs/Observable";
import { Predmet } from '../model/predmet.model';
import { PredispitneObavezeInterface } from '../model/predispitne-obaveze.model';
import { IspitInterface } from '../model/ispit.model';
import { NastavnikInterface } from '../model/nastavnik.model';


@Injectable()
export class PredmetService {

  private readonly apiPredmeti = `api/predmeti`;
  private readonly apiStudenti = `api/studenti`;
  private readonly apiNastavnici = `api/nastavnici`;
  private readonly apiPredispitneObaveze = `api/predispitne-obaveze`

  constructor(private _http:HttpClient) { }

  getAll(naziv: string, page: number, size: number):Observable<HttpResponse<Predmet[]>>{
    naziv = naziv==undefined? '': naziv;
    page = page==undefined? 0: page;
    size = size==undefined? 5: size;
    return this._http.get<Predmet[]>(`${this.apiPredmeti}?naziv=${naziv}&page=${page}&size=${size}`, 
    {observe: 'response'});
  }

  getPredispitneObaveze():Observable<PredispitneObavezeInterface[]>{
    return this._http.get<PredispitneObavezeInterface[]>(this.apiPredispitneObaveze);
  }

  getPredmet(predmetId: number):Observable<Predmet>{
    return this._http.get<Predmet>(this.apiPredmeti+ '/' + predmetId);
  }

  
  getStudentPredmeti(studentId: number):Observable<Predmet[]>{
    return this._http.get<Predmet[]>(this.apiStudenti + '/' + studentId + '/predmeti');
  }

  getNastavnikPredmeti(nastavnikId: number):Observable<Predmet[]>{
    return this._http.get<Predmet[]>(this.apiNastavnici + '/' + nastavnikId + '/predmeti');
  }

  getPredmetIspiti(predmetId: number):Observable<IspitInterface[]>{
    return this._http.get<IspitInterface[]>(this.apiPredmeti + '/' + predmetId + '/ispiti');
    
  }

  getPredmetNastavnici(predmetId: number):Observable<NastavnikInterface[]>{
    return this._http.get<NastavnikInterface[]>(this.apiPredmeti + '/' + predmetId + '/nastavnici');
  }

  getPredmetNastavniciNe(predmetId: number):Observable<NastavnikInterface[]>{
    return this._http.get<NastavnikInterface[]>(`${this.apiPredmeti}/${predmetId}/nastavnici-ne`);
  }

  dodeljivanjeNastavnikaPredmetu(predmetId: number, listaNastavnikaId:any){
    return this._http.post(`${this.apiPredmeti}/${predmetId}/nastavnici`, listaNastavnikaId);
  }





  addPredmet(predmet: Predmet):Observable<any>{
    return this._http.post(this.apiPredmeti, predmet);
  }

  deletePredmet(predmetId: number):Observable<any>{
    return this._http.delete(`${this.apiPredmeti}/${predmetId}`);
  }

  updatePredmet(predmetId: number, predmet: Predmet):Observable<any>{
    return this._http.put(`${this.apiPredmeti}/${predmetId}`, predmet);
  }

  removeNastavnikFromPRedmet(predmetId: number, nastavnikId: number):Observable<any>{
    return this._http.delete(`${this.apiPredmeti}/${predmetId}/nastavnici/${nastavnikId}`);
  }

}
