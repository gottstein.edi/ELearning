import { Injectable } from "../../../node_modules/@angular/core";
import { HttpClient, HttpResponse } from "../../../node_modules/@angular/common/http";
import { Observable } from "../../../node_modules/rxjs";
import { Predmet } from "../model/predmet.model";
import { UplataInterface, PostUplataInterface } from "../model/uplata.model";
import { StudentInterface } from "../model/student.model";
import { DokumentInterface } from "../model/dokument.model";

@Injectable()
export class StudentService{

    private readonly apiStudenti = `api/studenti`;
    private readonly apiUplate = `api/uplate`;
    private readonly apiFiles = `api/files`;

    

    constructor(private _http:HttpClient){

    }

    getNeupisaniStudenti():Observable<StudentInterface[]>{
        return this._http.get<StudentInterface[]>(`${this.apiStudenti}/ne-upisani`);
    }

    getStudent(studentId:number):Observable<StudentInterface>{
        return this._http.get<StudentInterface>(`${this.apiStudenti}/${studentId}`);
    }
   

    getStudentUplate(studentId: number, page: number, size: number):Observable<HttpResponse<UplataInterface[]>>{
        return this._http.get<UplataInterface[]>(`${this.apiStudenti}/${studentId}/uplate?page=${page}&size=${size}`, 
        {observe: 'response'});
    }

    addUplata(uplata: PostUplataInterface):Observable<any>{
        return this._http.post<PostUplataInterface>(this.apiUplate, uplata);
    }

    getStudentDokumenti(studentId: number):Observable<any>{
        return this._http.get<DokumentInterface[]>(`${this.apiStudenti}/${studentId}/dokumenti`);
    }

    getPolozeniPredmeti(studentId: number):Observable<Predmet[]>{
        return this._http.get<Predmet[]>(`${this.apiStudenti}/${studentId}/predmeti/polozio`)
    }

    getNePolozeniPredmeti(studentId: number):Observable<Predmet[]>{
        return this._http.get<Predmet[]>(`${this.apiStudenti}/${studentId}/predmeti/ne-polozio`)
    }

    updateStudent(studentId: number, student: StudentInterface):Observable<any>{
        return this._http.put(`${this.apiStudenti}/${studentId}`, student);
    }

    

}