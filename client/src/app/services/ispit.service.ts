import { Injectable } from "../../../node_modules/@angular/core";
import { IspitInterface } from "../model/ispit.model";
import { PrijavaInterface, PrijavaPostInterface } from "../model/prijava.model";
import { Observable } from "../../../node_modules/rxjs";
import { HttpClient } from "../../../node_modules/@angular/common/http";
import { IspitNastavnikInterface } from "../model/ispitNastavnik.model";

@Injectable()
export class IspitService {

    private readonly apiIspiti = `api/ispiti`;
    private readonly apiPrijave = `api/prijave`;

    constructor(private _http:HttpClient){}

    addIspit(ispit: IspitInterface):Observable<any> {
        return this._http.post(this.apiIspiti, ispit);
    }

    prijaviIspit(prijava: PrijavaPostInterface):Observable<any>{
        return this._http.post(this.apiPrijave, prijava);
    }

    getIspit(ispitId: number):Observable<IspitInterface>{
        return this._http.get<IspitInterface>(`${this.apiIspiti}/${ispitId}`);
    }

    getPrijave(ispitId: number):Observable<PrijavaInterface[]>{
        return this._http.get<PrijavaInterface[]>(`${this.apiIspiti}/${ispitId}/prijave`);
    }

    getIspitiByNastavnik(nastavnikId: number):Observable<IspitNastavnikInterface[]>{
        return this._http.get<IspitNastavnikInterface[]>(`${this.apiIspiti}/nastavnik-ispiti/${nastavnikId}`)
    }

    

}