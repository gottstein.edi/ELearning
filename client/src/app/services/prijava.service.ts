import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { PrijavaInterface } from "../model/prijava.model";

@Injectable()
export class PrijavaService{

    private readonly studentiApi = 'api/studenti'

    constructor(private _http:HttpClient){}

    getStudentsPrijave(studentId: number):Observable<PrijavaInterface[]>{
        return this._http.get<PrijavaInterface[]>(`${this.studentiApi}/${studentId}/prijave`);
    }


}