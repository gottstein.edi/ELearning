import { Injectable } from "@angular/core";
import { PolaganjePostInterface } from "../model/polaganje.model";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { PolaganjeStudentaInterface } from "../model/polaganjeStudent.model";

@Injectable()
export class PolaganjeService {

    constructor(private _http:HttpClient){}

    private readonly apiPolaganja = `api/polaganja`


    addPolaganje(polaganje: PolaganjePostInterface):Observable<any>{
        return this._http.post(this.apiPolaganja, polaganje);
    }

    getPolaganjeByStudentAndPredmet(studentId:number, predmetId: number):Observable<any>{
        return this._http.get(`${this.apiPolaganja}/${studentId}/${predmetId}`)
    }

    getPolaganjeByStudent(studentId: number):Observable<PolaganjeStudentaInterface[]>{
        return this._http.get<PolaganjeStudentaInterface[]>(`${this.apiPolaganja}/polaganja-studenta/${studentId}`);
    }

}