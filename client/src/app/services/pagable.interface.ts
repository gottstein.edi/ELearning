export interface Pagable {
    maxSize: number;
    page: number;
}