import { Injectable } from "../../../node_modules/@angular/core";
import { HttpClient } from "../../../node_modules/@angular/common/http";
import { Observable } from "../../../node_modules/rxjs";
import { DokumentPostInterface } from "../model/dokument.model";

@Injectable()
export class DokumentService{

    apiDokumenti = 'api/dokumenti';
    apiDokumentDownload = 'api/files';

    constructor(private _http:HttpClient){

    }

    

    dodavanjeDokumenta(file: File, dokument:DokumentPostInterface):Observable<any> {
        const formData: FormData = new FormData();
        formData.append('file', file);
        formData.append('naziv', dokument.naziv);
        formData.append('student', String(dokument.student));
        return this._http.post<any>(this.apiDokumenti, formData);
    }

    downloadDokument(filename: string):Observable<File>{
        return this._http.get<File>(`${this.apiDokumentDownload}/${filename}`)

    }

    documentUpdate(dokument:DokumentPostInterface, dokumentId:number):Observable<any>{
        return this._http.put<any>(`${this.apiDokumenti}/${dokumentId}`, dokument);
    }

    deleteDocument(docId: number):Observable<any>{
        return this._http.put(`${this.apiDokumenti}/${docId}/delete`,null);
    }

}
