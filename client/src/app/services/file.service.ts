import { Injectable } from "../../../node_modules/@angular/core";

import { saveAs } from 'file-saver';
import { HttpClient, HttpHeaders } from "../../../node_modules/@angular/common/http";

@Injectable()
export class FileService {

    private readonly filesApi = '/files'
    

    private httpOptions = {
        headers: new HttpHeaders({
            'Accept': 'application/pdf'
        })
    }

    constructor(private _http:HttpClient){}

    downloadFile(filename: string){
        this._http.get(`${this.filesApi}/${filename}`, {headers: this.httpOptions.headers, responseType: 'blob'})
        .subscribe(data => {
            saveAs(data, filename.substr(6));
        }, () => {});
    }

    // downloadFile(filename: string) {
    //     this._http.
    //     public get value() : string {
    //         return 
    //     }
        
    // }
}