import { Component, OnInit } from '@angular/core';
import { SmerService } from '../services/smer.service';
import { Smer } from '../model/smer.mode';
import { Router } from '../../../node_modules/@angular/router';

@Component({
  selector: 'app-smer-list',
  templateUrl: './smer-list.component.html',
  styleUrls: ['./smer-list.component.css']
})
export class SmerListComponent implements OnInit {

  page:number = 1;
  size: number = 5;
  total: number;
  filter: string = "";

  smerovi:Smer[] = [];

  constructor(private _smerService:SmerService,private _router:Router) { }

  ngOnInit() {
    this.refreshData();

  }

  refreshData() {
    this._smerService.getAll(this.filter, this.page-1, this.size).subscribe(data =>{
      this.total = Number(data.headers.get('X-Total'));
      this.smerovi = data.body;
    }, error => this.smerovi = null);
  }

  onPageChange(event: number){
    this.page = event;
    this.refreshData();
  }

  onSelectChange(){
    this.refreshData();
  }

  onSearchChange(){
    this.refreshData();
  }

  onAddSmer(){
    this._router.navigate(['/smerovi-add']);
  }

}
