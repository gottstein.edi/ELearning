import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '../../../node_modules/@angular/router';
import { SmerService } from '../services/smer.service';
import { Smer } from '../model/smer.mode';
import { NgbModal } from '../../../node_modules/@ng-bootstrap/ng-bootstrap';
import { Predmet } from '../model/predmet.model';
import { PredmetService } from '../services/predmet.service';
import { StudentInterface } from '../model/student.model';
import { StudentService } from '../services/student.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-smer-detail',
  templateUrl: './smer-detail.component.html',
  styleUrls: ['./smer-detail.component.css']
})
export class SmerDetailComponent implements OnInit {

  smerId;
  smer:Smer;

  checkboxPredmeti:{predmet: Predmet, checked: boolean}[] = [];
  checkboxStudenti:{student: StudentInterface, checked: boolean}[] = [];
  predmeti:Predmet[] = [];
  
  listaPredmetaZaSlanje = {
    listItemIds :  []
  }

  listaStudentaZaPrijavu = {
    listItemIds : []
  }

  studentiNaSmeru: StudentInterface[] = [];
  smerCopy:Smer;
  


  

  constructor(private _router:Router,private route: ActivatedRoute, private smerService:SmerService, private modalService:NgbModal,
     private _predmetService:PredmetService, private _studentService: StudentService, private _location:Location) { }

  ngOnInit() {
    this.smerId = +this.route.snapshot.paramMap.get('id');
    this.smerService.getSmer(this.smerId).subscribe(smer =>
    this.smer = smer);
    
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      
    }, (reason) => {
      
    });
  }

  tabChanged(event: any){
    
    
    if(event.nextId == "predmetiNaSmeru"){
      this.refreshPredmeti();
    }

    if(event.nextId == "studentiNaSmeru"){
      this.refreshStudenti();
    }

  }


  refreshPredmeti(){
    this.smerService.getPredmeti(this.smerId).subscribe(data => {
      this.predmeti = data;
    })
  }

  ucitajNeUpisaneStudente() {
    this._studentService.getNeupisaniStudenti().subscribe(data => {
      if(data != null){
        data.forEach(item => {
          this.checkboxStudenti.push({student: item, checked: false});
        })
      }
    })
  }

  ucitajSvePredmete(){
    this.smerService.getPredmetiSmerNeSadrzi(this.smerId).subscribe(data => {
      if(data != null){
        data.forEach(item => {
          this.checkboxPredmeti.push({predmet: item, checked: false});
        })
      }
    })
  }

  refreshStudenti(){
    this.smerService.getStudentiNaSmeru(this.smerId).subscribe(data => {
      
      this.studentiNaSmeru = data;
    })
  }


  onDodajPredmet(){
    this.checkboxPredmeti = [];
    this.ucitajSvePredmete();
  }

  onUpistStudent(){
    this.checkboxStudenti = [];
    this.ucitajNeUpisaneStudente();
  }


  deleteSmer(){
    this.smerService.deleteSmer(this.smerId).subscribe(() => {});
    this._router.navigate(['/smerovi']);
  }


  onBack():void{
    this._location.back();
  }

  dodajIzabranePredmete(){
    this.checkboxPredmeti.forEach(value => {
      if(value.checked){
        this.listaPredmetaZaSlanje.listItemIds.push(value.predmet.id);
      }
    })

    this.smerService.dodavanjePredmeta(this.smerId, this.listaPredmetaZaSlanje).subscribe( () => {});
    this.refreshPredmeti();
  }

  dodajIzabraneStudente(){
    this.checkboxStudenti.forEach(value => {
      if(value.checked){
        this.listaStudentaZaPrijavu.listItemIds.push(value.student.id);
      }
    })



    this.smerService.upistStudenata(this.smerId, this.listaStudentaZaPrijavu).subscribe(data => {
      this.listaStudentaZaPrijavu.listItemIds = [];
      this.refreshStudenti();
    });

    
  }



  //UPDATE SMER

  updateSmerCancel(){
    this.smerCopy = null;
  }
  updateSmerConfirm(){
    this.smerService.updateSmer(this.smerId, this.smerCopy).subscribe(data => {
      this.smerService.getSmer(this.smerId).subscribe(smer =>
        this.smer = smer);
    })
  }
  onUpdateSmer(){
    this.smerCopy = Object.assign({}, this.smer);

  }

  //UPDATE SMER END


  onPredmetRemove(predmetIndex: number){
    this.smerService.removePredmetFromSmer(this.smerId, this.predmeti[predmetIndex].id).subscribe(data => {
      this.refreshPredmeti();
    })
  }

  onStudentRemove(studentIndex: number){
    this.smerService.removeStudentFromSmer(this.smerId, this.studentiNaSmeru[studentIndex].id).subscribe(data => {
      this.refreshStudenti();
    })
  }

  





}
