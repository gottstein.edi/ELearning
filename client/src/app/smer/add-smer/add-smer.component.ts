import { Component, OnInit } from '@angular/core';
import { Router } from '../../../../node_modules/@angular/router';
import { Smer } from '../../model/smer.mode';
import { SmerService } from '../../services/smer.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-add-smer',
  templateUrl: './add-smer.component.html',
  styleUrls: ['./add-smer.component.css']
})
export class AddSmerComponent implements OnInit {

  smerPost: Smer = {
    naziv: null,
    oznaka: null,
    brojSemestra: 6,
    tip: "akademski"
  }

  constructor(private _router:Router, private _smerService:SmerService, private _location: Location) { }

  ngOnInit() {
  }

  onBack(){
    this._location.back();
  }

  onAdd(){
    this._smerService.addSmer(this.smerPost).subscribe( () => {

    });
  }

}
