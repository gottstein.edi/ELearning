export interface PolaganjeStudentaInterface {

    brojPredispitnihBodova: number,
    brojIspitnihBodova: number,
    ukupanBrojBodova: number,
    ocena: number,
    predmet: string;
}