export interface PolaganjePostInterface {
    predispitniBodovi: number,
    ispitniBodovi:number,
    ukupniBodovi: number,
    ocena: number,
    polozio:boolean,
    prijava:number
}