export interface DokumentInterface{
    id?:number;
    naziv: string,
    filename: string,
    MIME: string,
    student: number,
}

export interface DokumentPostInterface {
    id?: number,
    naziv: string,
    student: number
}