export interface PasswordInterface {
    oldPassword: string,
    newPassword: string
}