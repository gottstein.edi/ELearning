export interface StudentInterface{
    id?: number;
    ime: string;
    prezime: string;
    korisnickoIme: string;
    email: string;
    role: string;
    drzavljanstvo: string,
    datumRodjenja: Date;
    adresaStanovanja: string;
    brojIndeksa: string;
    godinaUpisa: number;
    godinaStudije: number;
    sifra?: string;
    jmbg: string;
    smer: number;
}

export interface StudentPostInterface {
    ime: string;
    prezime: string;
    korisnickoIme: string;
    sifra?: string;
    email: string;
    datumRodjenja: Date;
    adresaStanovanja: string;
    brojIndeksa: string;
    godinaUpisa: number;
    godinaStudije: number;
    jmbg: string;
    smer: number;
    drzavljanstvo: string;
}