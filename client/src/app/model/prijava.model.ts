export interface PrijavaInterface {
    id?:number;
    datumPrijave: Date;
    ispit: number;
    brojIndeksa: String;
    student: number;
}

export interface PrijavaPostInterface {
    ispit: number;
    student: number;
}