export interface UplataInterface {
    id? :number;
    date : number;
    iznos : number;
    svrhaUplate? : string;
}

export interface PostUplataInterface {
    iznos: number;
    svrhaUplate? : string;
    student: number;
}