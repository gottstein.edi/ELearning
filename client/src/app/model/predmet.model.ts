export interface Predmet {
    id?: number;
    naziv: string;
    oznaka: string;
    espb: number;
    predispitnaObaveza?: number;

}