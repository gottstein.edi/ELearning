export interface UserInterface{
    id?: number;
    ime: string;
    prezime: string;
    korisnickoIme: string;
    email: string;
    role: string;
}

export interface UserPostInterface{
    id?: number;
    ime: string;
    prezime: string;
    korisnickoIme: string;
    email: string;
    sifra?: string;
    role?: {
        id: number
    };
}


