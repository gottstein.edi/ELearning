export interface IspitNastavnikInterface {
    id?: number,
    datumOdrzavanja: Date,
    rokZaPrijavu: Date,
    predmet: string
}