export interface NastavnikInterface{
    id?: number;
    ime: string;
    prezime: string;
    korisnickoIme: string;
    email: string;
    role: string;
    zvanje: number;
    sifra?: string;
}

export interface NastavnikPostInterface{
    id?: number;
    ime: string;
    prezime: string;
    korisnickoIme: string;
    sifra: string;
    email: string;
    zvanje: number;

}