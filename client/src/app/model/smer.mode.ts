export interface Smer {
    id?: number;
    naziv: string;
    oznaka: string;
    tip: string;
    brojSemestra: number
}