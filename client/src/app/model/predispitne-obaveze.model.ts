export interface PredispitneObavezeInterface {
    id?: number;
    brojBodova: number;
    naziv: string;
}