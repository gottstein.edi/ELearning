import { Injectable } from "@angular/core";
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { AuthenticationService } from "./authentication.service";
import { Observable } from "rxjs";

@Injectable()
export class StudentOrAdminGuard implements CanActivate {

    constructor(private _authenticationService: AuthenticationService, private _router:Router){}

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
            if(!this._authenticationService.isStudentOrAdmin()){
                this._router.navigate(['/unauthorized']);
                return false;
            } else {
                return true;
            }
      }
}