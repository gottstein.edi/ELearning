import { Injectable } from "../../../node_modules/@angular/core";
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "../../../node_modules/@angular/router";
import { Observable } from "../../../node_modules/rxjs";
import { AuthenticationService } from "./authentication.service";





@Injectable()
export class AdminGuard implements CanActivate {

    constructor(private _authenticationService: AuthenticationService, private _router:Router){}

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
            if(!this._authenticationService.isAdmin()){
                this._router.navigate(['/unauthorized']);
                return false;
            } else {
                return true;
            }
      }

}