import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { UserToken } from "./user.token";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { LoginUserInterface } from "../users/loginUser";

@Injectable()
export class AuthenticationService {

    private readonly loginPath = `/auth/login`;

    constructor(private _http: HttpClient){

    }

    login(user: LoginUserInterface): Observable<boolean> {
        const body = `username=${user.username}&password=${user.password}`;
    const httpOptions = {
      headers: new HttpHeaders(
        { 'Content-Type': 'application/x-www-form-urlencoded' })
            };
        return this._http.post<UserToken>(this.loginPath, body, httpOptions)
        .map((res: UserToken) => {
            let token = res.access_token;
            if (token) {
              localStorage.setItem('token', JSON.stringify({
                                        username: user.username,
                                        access_token: token,
                                        role: res.role,
                                        id: res.id
                                      }));

              return true;
            }
            else {
              return false;
            }
          })
          .catch((error: any) => {
            if (error.status === 400) {
              return Observable.throw('Ilegal login');
            }
            else {
              return Observable.throw(error.json().error || 'Server error');
            }
          });
    }

    getToken(): String {
      var token = JSON.parse(localStorage.getItem('token'));
      var access_token = token && token.access_token;
      return access_token ? access_token : "";
    }

    isLoggedIn(): boolean{
      if(this.getToken()!='') return true;
      else return false;
    }

    roleLogedIn():string {
      var token = JSON.parse(localStorage.getItem("token"));
      return token.role;
    }

    loggedInUserId():number {
      var token = JSON.parse(localStorage.getItem("token"))
      return token.id;
    }

    isAdmin():boolean {
      const token = JSON.parse(localStorage.getItem("token"));
      return token.role === "ADMIN";

    }

    isNastavnikOrAdmin():boolean {
      const token = JSON.parse(localStorage.getItem("token"));
      if(token.role === "NASTAVNIK" || token.role === "ADMIN"){
        return true;
      }else{
        return false;
      }
    }

    isStudentOrAdmin():boolean {
      const token = JSON.parse(localStorage.getItem("token"));
      if(token.role === "STUDENT" || token.role === "ADMIN"){
        return true;
      } else {
        return false;
      }
    }

}
