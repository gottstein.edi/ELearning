import { Component, OnInit, Input } from '@angular/core';
import { StudentInterface } from '../../model/student.model';
import { Router, ActivatedRoute } from '../../../../node_modules/@angular/router';
import { UplataInterface } from '../../model/uplata.model';
import { StudentService } from '../../services/student.service';
import { NgbModal } from '../../../../node_modules/@ng-bootstrap/ng-bootstrap';
import { UserService } from '../../services/user.service';
import { Predmet } from '../../model/predmet.model';
import { PredmetService } from '../../services/predmet.service';
import { DokumentInterface } from '../../model/dokument.model';
import { FileService } from '../../services/file.service';
import { DokumentService } from '../../services/dokument.service';
import { Smer } from '../../model/smer.mode';
import { SmerService } from '../../services/smer.service';

@Component({
  selector: 'app-student-details',
  templateUrl: './student-details.component.html',
  styleUrls: ['./student-details.component.css']
})
export class StudentDetailsComponent implements OnInit {

  

  studentId:number;
  student:StudentInterface;

  uplate:UplataInterface[] = [];
  predmeti:Predmet[] = [];
  dokumenti:DokumentInterface[]= [];

  loadPolozeniOrNepolozeni: number = 1;

  studentCopy: StudentInterface;
  documentCopy:DokumentInterface;
  selectedDocumentId: number;

  smerovi:Smer[] = [];

  constructor(private _router: Router, private _studentService: StudentService, private modalService:NgbModal, private _userService:UserService, 
    private _predmetService:PredmetService, private _fileService:FileService, private _documentService: DokumentService, private _smerService:SmerService, private route:ActivatedRoute) { }

  ngOnInit() {
    this.studentId = +this.route.snapshot.paramMap.get("id");
    this._userService.getStudent(this.studentId).subscribe(data => {
      this.student = data;
    })
  }

  onSelectChange(value:number){
    this.loadStudentPredmeti();
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      
    }, (reason) => {
      
    });
  }

  openLg(content){
    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      
    }, (reason) => {
      
    });
  }

  onBack():void{
    this._router.navigate(['/korisnici']);
  }

  tabChanged(event: any){
    
    
    if(event.nextId == "uplateStudenta"){
      this.loadStudentUplate();
    }
    if(event.nextId == "predmetiStudenta"){
      this.loadStudentPredmeti();
      
    }
    if(event.nextId == "dokumentiStudenta"){
      this._studentService.getStudentDokumenti(this.student.id).subscribe(data => {
        this.dokumenti = data;
      })
    }
  }

  loadStudentUplate(){
    this._studentService.getStudentUplate(this.student.id,0,0).subscribe(data => {
      this.uplate = data.body;
    })
  }


  loadStudentPredmeti(){
    if(this.loadPolozeniOrNepolozeni == 1){
      this._studentService.getPolozeniPredmeti(this.studentId).subscribe(data => {
        this.predmeti = data;
      })
    }else if(this.loadPolozeniOrNepolozeni == 2){
      this._studentService.getNePolozeniPredmeti(this.studentId).subscribe(data => {
        this.predmeti = data;
      })
    }
  }

  deleteStudent(){
    this._userService.deleteUser(this.student.id).subscribe(() => {});
    this._router.navigate(['/korisnici']);
  }


  // DOKUMENT UPDATE
  onDownloadDocument(filename:string){
    this._fileService.downloadFile(filename);
  }

  onEditClicked(docIndex: number){
    this.documentCopy = Object.assign({}, this.dokumenti[docIndex]);
    this.selectedDocumentId = this.dokumenti[docIndex].id;

  }

  updateDoc(){
    this._documentService.documentUpdate(this.documentCopy, this.selectedDocumentId).subscribe(data => {
      this._studentService.getStudentDokumenti(this.student.id).subscribe(data => {
        this.dokumenti = data;
      })
    })

  }

  cancelDocumentUpdate(){
    this.documentCopy = undefined;
    this.selectedDocumentId = undefined;
  }

  //DOKUMENT UPDATE END


  //STUDENT UPDATE
  onUpdateStudent(){
    this.studentCopy = Object.assign({},  this.student);
    
    this._smerService.getAll("",0,0).subscribe(data => {
      this.smerovi = data.body;
    })

    
  }

  updateStudentConfirm(){
    this._studentService.updateStudent(this.student.id, this.studentCopy).subscribe(data => {
      this._userService.getStudent(this.studentId).subscribe(data => {
        this.student = data;
      })
    })

  }

  cancelStudentUpdate(){
    this.studentCopy = undefined;
  }


  onDateSelect(evt: any) {
    this.studentCopy.datumRodjenja = new Date(evt.year,evt.month-1, evt.day);
  }

  //STUDENT UPDATE END

}
