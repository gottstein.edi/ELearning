import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StudentService } from '../../../services/student.service';
import { StudentInterface } from '../../../model/student.model';
import { UserService } from '../../../services/user.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-nastavnik-student-view',
  templateUrl: './nastavnik-student-view.component.html',
  styleUrls: ['./nastavnik-student-view.component.css']
})
export class NastavnikStudentViewComponent implements OnInit {

  studentId: number;
  student: StudentInterface

  constructor(private route:ActivatedRoute, private _userService: UserService, private _location: Location) { }

  ngOnInit() {
    this.studentId = +this.route.snapshot.paramMap.get("id");
    this._userService.getStudent(this.studentId).subscribe(data => {
      this.student = data;
    })

  }

  onBack(){
    this._location.back();
  }

}
