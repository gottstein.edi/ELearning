import { Component, OnInit, Input } from '@angular/core';
import { NastavnikInterface } from '../../model/nastavnik.model';
import { Router, ActivatedRoute} from '../../../../node_modules/@angular/router';
import { ZvanjeInterface } from '../../model/zvanje.model';
import { NastavnikService } from '../../services/nastavnik.service';
import { NgbModal } from '../../../../node_modules/@ng-bootstrap/ng-bootstrap';
import { UserService } from '../../services/user.service';
import { PredmetService } from '../../services/predmet.service';
import { Predmet } from '../../model/predmet.model';

@Component({
  selector: 'app-nastavnik-details',
  templateUrl: './nastavnik-details.component.html',
  styleUrls: ['./nastavnik-details.component.css']
})
export class NastavnikDetailsComponent implements OnInit {


  zvanje: ZvanjeInterface;
  nastavnikId: number;
  nastavnik:NastavnikInterface;
  nastavnikCopy:NastavnikInterface;

  predmeti:Predmet[] = [];
  zvanja:ZvanjeInterface[] = [];

  constructor(private _router: Router, private _nastavnikService: NastavnikService, private modalService: NgbModal, private _userService: UserService,
  private _predmetService: PredmetService, private route:ActivatedRoute) { }

  ngOnInit() {

    this.nastavnikId = +this.route.snapshot.paramMap.get("id");
    this._userService.getNastavnik(this.nastavnikId).subscribe(data => {
      this.nastavnik = data;
      this._nastavnikService.getZvanje(this.nastavnik.zvanje).subscribe( zvanje =>{
        this.zvanje = zvanje;
      }
      )

    })
    
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      
    }, (reason) => {
      
    });
  }

  tabChanged(event: any){
    
    if(event.nextId == "nastavnikPredaje"){
      this.loadNastavnikPredmeti();
    }
    
  }

  onBack(){
    this._router.navigate(['/korisnici']);
  }

  deleteNastavnik(){
    this._userService.deleteUser(this.nastavnik.id).subscribe(() => {});
    this._router.navigate(['/korisnici']);
  }

  loadNastavnikPredmeti(){
    this._predmetService.getNastavnikPredmeti(this.nastavnik.id).subscribe(data =>{
      this.predmeti = data;
    })
  }



  //UPDATE NASTAVNIK

  onUpdate(){
    this._nastavnikService.getZvanja().subscribe(data => {
      this.zvanja = data;
    })
    this.nastavnikCopy = Object.assign({}, this.nastavnik);
  }

  onUpdateConfirm(){
    this._nastavnikService.updateNastavnik(this.nastavnikId, this.nastavnikCopy).subscribe(data => {
      this._userService.getNastavnik(this.nastavnikId).subscribe(data => {
        this.nastavnik = data;
        this._nastavnikService.getZvanje(this.nastavnik.zvanje).subscribe( zvanje =>{
          this.zvanje = zvanje;
        }
        )
  
      })
    })

  }

  onUpdateCancel(){
    this.nastavnikCopy = null;
  }

  //UPDATE END

}
