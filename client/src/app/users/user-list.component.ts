import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { UserInterface } from '../model/user.model';
import { NumberValueAccessor } from '../../../node_modules/@angular/forms/src/directives';
import { Router } from '../../../node_modules/@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users: UserInterface[] = [];
  link:any;
  selectedUserRole: string;
 
  page:number = 1;
  size: number = 5;
  total: number;
  filter: string = "";

  constructor(private _userService: UserService, private _router: Router) { }


  refreshData() {
    this._userService.getAll(this.filter, this.page-1, this.size).subscribe(data => {
      this.total = Number(data.headers.get('X-Total'));
      this.users = data.body;
      
    }, error => this.users = null);
  }

  ngOnInit() {
    this.refreshData();
  }

  onPageChange(event: number){
    this.page = event;
    this.refreshData();
  }

  onSelectChange(){
    this.refreshData();
  }
  
  onSearchChange(){
    this.refreshData();
  }

  onAddUser(){
    this._router.navigate(["/korisnici-add"]);
  }

}

