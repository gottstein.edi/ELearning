import { Component, OnInit } from '@angular/core';
import { UserPostInterface } from '../../../model/user.model';
import { Router } from '../../../../../node_modules/@angular/router';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-admin-user-add',
  templateUrl: './admin-user-add.component.html',
  styleUrls: ['./admin-user-add.component.css']
})
export class AdminUserAddComponent implements OnInit {


  user: UserPostInterface = {
    ime: null,
    prezime: null,
    korisnickoIme: null,
    sifra: null,
    email: null,
    role: {
      id: 3
    },
  }
  userNameAvailable: boolean;

  constructor(private _router: Router, private _userService:UserService) { }

  ngOnInit() {
  }

  onAddClick(){
    this._userService.addUser(this.user).subscribe(() => {

    });
  }

  onBack(){
    this._router.navigate(['/korisnici']);
  }

  onUsernameChange() {
    this.userNameAvailable = undefined;
  }

  onUsernameCheck() {
    this._userService.checkIfUsernameIsUnique(this.user.korisnickoIme).subscribe(data => {
      if(data){
        this.userNameAvailable = true;
      } else {
        this.userNameAvailable = false;
      }
    })
  }

}
