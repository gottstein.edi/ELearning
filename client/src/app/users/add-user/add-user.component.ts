import { Component, OnInit } from '@angular/core';
import { Router } from '../../../../node_modules/@angular/router';
import { Smer } from '../../model/smer.mode';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  addUserType = 1;
  
  smerovi: Smer[] = [];

  constructor(private _router:Router) { }

  ngOnInit() {
    
  }

  onBack(){
    this._router.navigate(['/korisnici']);
  }

}
