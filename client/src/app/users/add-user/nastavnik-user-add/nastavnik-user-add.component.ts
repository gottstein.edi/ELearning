import { Component, OnInit } from '@angular/core';
import { NastavnikPostInterface } from '../../../model/nastavnik.model';
import { ZvanjeInterface } from '../../../model/zvanje.model';
import { NastavnikService } from '../../../services/nastavnik.service';
import { Router } from '../../../../../node_modules/@angular/router';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-nastavnik-user-add',
  templateUrl: './nastavnik-user-add.component.html',
  styleUrls: ['./nastavnik-user-add.component.css']
})
export class NastavnikUserAddComponent implements OnInit {


  nastavnik: NastavnikPostInterface = {
    ime: null,
    prezime: null,
    korisnickoIme: null,
    sifra: null,
    email: null,
    zvanje: 1,
    
  }

  userNameAvailable: boolean;

  zvanja: ZvanjeInterface[] = [];

  constructor(private _nastavnikService:NastavnikService,private _userService:UserService, private _router:Router) { }

  ngOnInit() {
    this._nastavnikService.getZvanja().subscribe(data => {
      this.zvanja = data;
    })
  }




  onAddClick(){
    this._userService.addNastavnik(this.nastavnik).subscribe(() => {});

  }

  onBack(){
    this._router.navigate(['/korisnici']);
  }

  onUsernameChange() {
    this.userNameAvailable = undefined;
  }

  onUsernameCheck() {
    this._userService.checkIfUsernameIsUnique(this.nastavnik.korisnickoIme).subscribe(data => {
      if(data){
        this.userNameAvailable = true;
      } else {
        this.userNameAvailable = false;
      }
    })
  }

}
