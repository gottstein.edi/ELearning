import { Component, OnInit } from '@angular/core';
import { StudentPostInterface } from '../../../model/student.model';
import { Router } from '../../../../../node_modules/@angular/router';
import { UserService } from '../../../services/user.service';
import { Smer } from '../../../model/smer.mode';
import { SmerService } from '../../../services/smer.service';

@Component({
  selector: 'app-student-user-add',
  templateUrl: './student-user-add.component.html',
  styleUrls: ['./student-user-add.component.css']
})
export class StudentUserAddComponent implements OnInit {

  smerovi:Smer[] = [];

  userNameAvailable: boolean;

  student: StudentPostInterface = {
    ime: null,
    prezime: null,
    korisnickoIme: null,
    sifra: null,
    email: null,
    jmbg: null,
    adresaStanovanja: null,
    datumRodjenja: null,
    godinaStudije: 1,
    brojIndeksa: null,
    godinaUpisa: 1,
    smer: 1,
    drzavljanstvo: null

  }

  constructor(private _router:Router,private _userService:UserService, private _smerService:SmerService) { }

  ngOnInit() {

    this._smerService.getAll("",0,0).subscribe( data => {
      this.smerovi = data.body;
    })
  }

  onDateSelect(evt: any) {
    this.student.datumRodjenja = new Date(evt.year,evt.month-1, evt.day);
  }

  onAddClick(){
    this._userService.addStudent(this.student).subscribe( () => {});
  }

  onBack(){
    this._router.navigate(['/korisnici']);
  }

  onUsernameChange(){
    this.userNameAvailable = undefined;

  }

  onUsernameCheck(){
    this._userService.checkIfUsernameIsUnique(this.student.korisnickoIme).subscribe(data => {
      if(data){
        this.userNameAvailable = true;
      } else {
        this.userNameAvailable = false;
      }
    })
  }


  

}
