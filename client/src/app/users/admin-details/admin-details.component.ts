import { Component, OnInit, Input } from '@angular/core';
import { UserInterface } from '../../model/user.model';
import { Router } from '../../../../node_modules/@angular/router';
import { NgbModal } from '../../../../node_modules/@ng-bootstrap/ng-bootstrap';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-admin-details',
  templateUrl: './admin-details.component.html',
  styleUrls: ['./admin-details.component.css']
})
export class AdminDetailsComponent implements OnInit {

  @Input() admin:UserInterface;

  constructor(private _router:Router, private modalService: NgbModal, private _userService:UserService) { }

  ngOnInit() {
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      
    }, (reason) => {
      
    });
  }

  onBack():void{
    this._router.navigate(['/korisnici']);
  }

  deleteAdmin(){
    this._userService.deleteUser(this.admin.id).subscribe(() => {});
    this._router.navigate(['/korisnici']);
  }
  

}
