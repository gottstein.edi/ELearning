import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserInterface } from '../model/user.model';
import { UserService } from '../services/user.service';
import { StudentInterface } from '../model/student.model';
import { NastavnikInterface } from '../model/nastavnik.model';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  userId;
  user:UserInterface | StudentInterface | NastavnikInterface;

  constructor(private _router: Router,private route: ActivatedRoute,private usersService:UserService) { }

  ngOnInit() {
    this.userId = +this.route.snapshot.paramMap.get('id');
    this.usersService.getUser(this.userId).subscribe(user => {
      this.user = user;
      
    }
      )
  }

  onBack():void{
    this._router.navigate(['/korisnici']);
  }


}
