import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { MainpageComponent } from "./mainpage/mainpage.component";
import { SecurityGuard } from "./security/security-guard.guard";
import { LoginComponent } from "./login/login.component";
import { PredmetListComponent } from "./predmet/predmet-list.component";
import { PredmetDetailComponent } from "./predmet/predmet-detail.component";
import { UserListComponent } from "./users/user-list.component";
import { UserDetailComponent } from "./users/user-detail.component";
import { IspitPrijaveListComponent } from "./ispit-list-view/ispit-prijave-list/ispit-prijave-list.component";
import { UplateComponent } from "./uplate/uplate.component";
import { AddUserComponent } from "./users/add-user/add-user.component";
import { AddSmerComponent } from "./smer/add-smer/add-smer.component";
import { AddPredmetComponent } from "./predmet/add-predmet/add-predmet.component";
import { PredmetiStudentViewComponent } from "./predmet/predmeti-student-view/predmeti-student-view.component";
import { DokumentListComponent } from "./dokumenti/dokument-list/dokument-list.component";
import { NastavnikPredmetListComponent } from "./predmet/nastavnik-predmet-list/nastavnik-predmet-list.component";
import { NastavnikStudentViewComponent } from "./users/student-details/nastavnik-student-view/nastavnik-student-view.component";
import { UnauthorizedAccessComponent } from "./unauthorized-access/unauthorized-access.component";
import { SmerListComponent } from "./smer/smer-list.component";
import { SmerDetailComponent } from "./smer/smer-detail.component";
import { ProfileDetailComponent } from "./profile/profile-detail.component";
import { AdminGuard } from "./security/admin-guard.guard";
import { NastavnikOrAdminGuard } from "./security/nastavnik-admin-guard.guard";
import { StudentOrAdminGuard } from "./security/admin-student-guard.guard";
import { PolaganjaComponent } from "./polaganja/polaganja.component";
import { IspitiListNastavnikComponent } from "./ispiti-list-nastavnik/ispiti-list-nastavnik.component";

const appRoutes: Routes = [
    {path: '', component: MainpageComponent, canActivate:[SecurityGuard]},
    {path: 'login', component: LoginComponent},
    {path: 'unauthorized', component: UnauthorizedAccessComponent},
    
    {path: 'predmeti/:id', component: PredmetDetailComponent, canActivate:[SecurityGuard]},
    
    

    {path: 'predmeti', component: PredmetListComponent, canActivate:[SecurityGuard, AdminGuard]},
    {path: 'korisnici', component: UserListComponent, canActivate:[SecurityGuard,AdminGuard]},
    {path: 'korisnici/:id', component: UserDetailComponent, canActivate:[SecurityGuard, AdminGuard]},
    {path: 'korisnici-add', component: AddUserComponent, canActivate:[SecurityGuard,AdminGuard]},
    {path: 'smerovi-add', component: AddSmerComponent, canActivate:[SecurityGuard,AdminGuard]},
    {path: 'predmeti-add', component: AddPredmetComponent, canActivate:[SecurityGuard,AdminGuard]},
    {path: 'smerovi', component: SmerListComponent, canActivate:[SecurityGuard, AdminGuard]},
    {path: 'smerovi/:id', component: SmerDetailComponent, canActivate:[SecurityGuard, AdminGuard]},


    {path: 'uplate', component: UplateComponent, canActivate:[SecurityGuard,StudentOrAdminGuard]},
    {path: 'polaganja', component: PolaganjaComponent, canActivate:[SecurityGuard,StudentOrAdminGuard]},
    {path: 'predmeti-student', component: PredmetiStudentViewComponent, canActivate:[SecurityGuard, StudentOrAdminGuard]},
    {path: 'dokumenti-student', component: DokumentListComponent, canActivate:[SecurityGuard, StudentOrAdminGuard]},


    {path: 'ispiti-nastavnik', component: IspitiListNastavnikComponent, canActivate:[SecurityGuard, NastavnikOrAdminGuard]},
    {path: 'ispiti/:id', component: IspitPrijaveListComponent, canActivate:[SecurityGuard, NastavnikOrAdminGuard]},
    {path: 'predmeti-nastavnik', component: NastavnikPredmetListComponent, canActivate:[SecurityGuard, NastavnikOrAdminGuard]},
    {path: 'studenti/:id', component: NastavnikStudentViewComponent, canActivate:[SecurityGuard, NastavnikOrAdminGuard]},
    

    
  
    
    {path: 'profil', component: ProfileDetailComponent, canActivate:[SecurityGuard]}
  ]


@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [RouterModule],
    declarations: []
})
export class AppRoutingModule {}