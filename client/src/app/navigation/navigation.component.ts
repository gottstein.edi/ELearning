import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../security/authentication.service';
import { Router } from '@angular/router';
import {Observable} from "rxjs/Observable";
import { Navel } from '../model/navel.model';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  role:string;
  navElements: Navel[] = []

  constructor(private _authService: AuthenticationService,  private _router:Router) { }

  ngOnInit() {
    this.role = this._authService.roleLogedIn();
    this.setupNavbar(this.role);
  }

  setupNavbar(role:string){
    if(this.role === 'ADMIN') {
      this.navElements.push({name: "Korisnici",path:  "korisnici"}, {name: "Predmeti", path:"predmeti"}, {name:"Smerovi",path: "smerovi"});
    } else if(this.role === 'NASTAVNIK'){
      this.navElements.push({name: "Predmeti",path: "predmeti-nastavnik"},{name: "Ispiti", path: "ispiti-nastavnik"});
    } else {
      this.navElements.push( {name:"Predmeti",path: "predmeti-student"}, {name: "Uplate",path: "uplate"},{name: "Dokumenti",path: "dokumenti-student"},
      {name:"Polaganja",path: "polaganja"});
    }
  }

  logout(){
    localStorage.clear();
    this._router.navigate(['/login']);

  }

}