import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../security/authentication.service';
import { Router } from '@angular/router';
import { LoginUserInterface } from '../users/loginUser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {



  user:LoginUserInterface = {
    username : '',
    password : ''
  }

  loginError: boolean = false;

  constructor(private _loginService: AuthenticationService, private _router:Router) { }

  ngOnInit() {
  }

  login(){
    this._loginService.login(this.user).subscribe(data => {
      
      if(data){
        this._router.navigate(["/"]);
      }
    }, error =>{
      this.loginError = true;
    } );
  }

  onLoginInput(){
    if(this.loginError){
      this.loginError = !this.loginError;
    }
  }

}
