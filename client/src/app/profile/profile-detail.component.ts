import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { UserInterface, UserPostInterface } from '../model/user.model';
import { StudentInterface } from '../model/student.model';
import { NastavnikInterface } from '../model/nastavnik.model';
import { Router } from '../../../node_modules/@angular/router';
import { NgbModal } from '../../../node_modules/@ng-bootstrap/ng-bootstrap';
import { PasswordInterface } from '../model/password.model';
import { StudentService } from '../services/student.service';
import { Location } from '@angular/common';
import { AuthenticationService } from '../security/authentication.service';
import { SmerService } from '../services/smer.service';

@Component({
  selector: 'app-profile-detail',
  templateUrl: './profile-detail.component.html',
  styleUrls: ['./profile-detail.component.css']
})
export class ProfileDetailComponent implements OnInit {

  constructor(private userService:UserService, private _router: Router, private modalService: NgbModal, private _studentService: StudentService, 
  private _location:Location, private _authenticationService: AuthenticationService, private _smerService:SmerService) { }

  loggedInUser: UserInterface | StudentInterface | NastavnikInterface;

  adminCopy:UserPostInterface = {
    email: null,
    id: null,
    ime: null,
    korisnickoIme: null,
    prezime: null,
    sifra: null
    
  }

  smerStudenta: string;

  passwordUpdate: PasswordInterface = {
    oldPassword: null,
    newPassword: null
  }

  newPassRepeated: string;
  oldPasswordError: boolean = false;

  ngOnInit() {
    this.userService.me().subscribe(user => {
      this.loggedInUser = user;
      if(this._authenticationService.roleLogedIn() == "STUDENT"){
        this._studentService.getStudent(this.loggedInUser.id).subscribe(data => {
          this._smerService.getSmer(data.smer).subscribe(data => {
            this.smerStudenta = data.naziv;
          })
        })
      }  
    })

    
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      
    }, (reason) => {
      
    });
  }

  onBack() {
    this._location.back();
  }

  potvrdaIzmene() {
    this.userService.changePassword(this.passwordUpdate).subscribe( data =>{

    }, error =>{
      alert("Pogresno ste uneli staru sifru");
    });
  }


  updateAdminConfirm(){

    this.userService.updateAdminProfile(this.adminCopy).subscribe(data => {
      this.userService.me().subscribe(user => {
        this.loggedInUser = user;
      })
    })

    this.adminCopy = {
      email: null,
      id: null,
      ime: null,
      korisnickoIme: null,
      prezime: null,
      sifra: null,
      
    }
  }

  onAdminUpdate(){
    this.adminCopy.id = this.loggedInUser.id;
    this.adminCopy.email = this.loggedInUser.email;
    this.adminCopy.korisnickoIme = this.loggedInUser.korisnickoIme;
    this.adminCopy.ime = this.loggedInUser.ime;
    this.adminCopy.prezime = this.loggedInUser.prezime;
    
  }

  

}
