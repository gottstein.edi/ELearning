import { Component, OnInit } from '@angular/core';
import { HttpClient } from '../../../node_modules/@angular/common/http';
import { UplataInterface, PostUplataInterface } from '../model/uplata.model';
import { StudentService } from '../services/student.service';
import { NgbModal } from '../../../node_modules/@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-uplate',
  templateUrl: './uplate.component.html',
  styleUrls: ['./uplate.component.css']
})
export class UplateComponent implements OnInit {

  page: number = 1;
  size: number = 5;
  total: number;

  greskaUplata = false;
  uplataIzvrsena = false;


  studentId: number;
  uplate: UplataInterface[] = [];

  uplataPost: PostUplataInterface = {
    iznos: null,
    svrhaUplate: null,
    student: null
  }

  constructor(private _studentService: StudentService, private modalService: NgbModal) {
    this.studentId = JSON.parse(localStorage.getItem("token")).id;
    this.uplataPost.student = this.studentId;

   }

  

  ngOnInit() {
   this.refreshData(); 
    
  }

  refreshData() {
    this._studentService.getStudentUplate(this.studentId, this.page-1, this.size).subscribe(data => {
      this.total = Number(data.headers.get('X-Total'));
      this.uplate = data.body;
    }, error => this.uplate = null);
  }

  onPageChange(event: number){
    this.page = event;
    this.refreshData();
  }

  onSelectChange(){
    this.refreshData();
  }



  // MODAL
  open(content) {
    this.modalService.open(content).result.then((result) => {
      
    }, (reason) => {
      
    });
  }

  uplataAdd(){
    this._studentService.addUplata(this.uplataPost).subscribe(data => {
      this.refreshData();
      this.uplataIzvrsena = true;
    }, errors => {
      this.greskaUplata = true;
    })
    
  }

}
