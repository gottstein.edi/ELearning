import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { PredmetService } from '../services/predmet.service';
import { IspitInterface } from '../model/ispit.model';
import { NgbModal } from '../../../node_modules/@ng-bootstrap/ng-bootstrap';
import { PrijavaPostInterface } from '../model/prijava.model';
import { AuthenticationService } from '../security/authentication.service';
import { IspitService } from '../services/ispit.service';

@Component({
  selector: 'app-ispit-list-view',
  templateUrl: './ispit-list-view.component.html',
  styleUrls: ['./ispit-list-view.component.css']
})
export class IspitListViewComponent implements OnInit,OnChanges {

  @Input()
  predmetId: number = null;

  ispitZaPrijavuId: number;

  prijava: PrijavaPostInterface = {
    ispit: null,
    student: null,
  }

  dateToday = new Date();

  greskaPrijavlivanja = false;
  ispitPrijavljen = false;
  

  

  ispiti: IspitInterface[] = [];

  constructor(private _predmetService: PredmetService, private modalService:NgbModal, private _authService: AuthenticationService,
    private _ispitService:IspitService) { }

  ngOnInit() {

  }

  ngOnChanges(changes: SimpleChanges){
      if(this.predmetId != null){
        this.refreshIspiti(this.predmetId);
      }
      
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      
    }, (reason) => {
      
    });
  }


  prijavaIspita(ispitId: number){
    this.ispitZaPrijavuId = ispitId
  }

  prijavaPotvrdjena(){
    this.prijava.ispit = this.ispitZaPrijavuId;
    this.prijava.student = this._authService.loggedInUserId();
    this._ispitService.prijaviIspit(this.prijava).subscribe(()=>{
      this.ispitPrijavljen = true;
    }, errors => {
      this.greskaPrijavlivanja = true;
    });
  }



  refreshIspiti(predmetId: number) {
    this._predmetService.getPredmetIspiti(predmetId).subscribe(data => {
      this.ispiti = data;
      
    })
  }
}
