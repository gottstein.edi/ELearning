import { Component, OnInit } from '@angular/core';
import { IspitService } from '../../services/ispit.service';
import { ActivatedRoute, Router } from '@angular/router';
import { IspitInterface } from '../../model/ispit.model';
import { Predmet } from '../../model/predmet.model';
import { PredmetService } from '../../services/predmet.service';
import { PrijavaInterface } from '../../model/prijava.model';
import { Location } from '@angular/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PolaganjePostInterface } from '../../model/polaganje.model';
import { PolaganjeService } from '../../services/polaganje.service';

@Component({
  selector: 'app-ispit-prijave-list',
  templateUrl: './ispit-prijave-list.component.html',
  styleUrls: ['./ispit-prijave-list.component.css']
})
export class IspitPrijaveListComponent implements OnInit {

  

  ispitId: number;
  selectedIspit: IspitInterface = {
    predmet: null,
    datumOdrzavanja: null,
    rokZaPrijavu: null

  }

  polaganjePost: PolaganjePostInterface = {
    ispitniBodovi:null,
    ocena:null,
    polozio:null,
    predispitniBodovi:null,
    ukupniBodovi:null,
    prijava: null
  }
  


  ispitPredmet:Predmet = {
    espb: null,
    naziv: null,
    oznaka: null
  }

  prijave: PrijavaInterface[] = [];

  constructor(private _ispitService: IspitService, private route:ActivatedRoute, private _predmetService: PredmetService,
    private _location: Location, private modalService: NgbModal , private _polaganjeService:PolaganjeService) { }

  ngOnInit() {
    this.ispitId = +this.route.snapshot.paramMap.get('id');
    
    this._ispitService.getIspit(this.ispitId).subscribe(data => {
      this.selectedIspit = data;
      this._predmetService.getPredmet(this.selectedIspit.predmet).subscribe(data => {
        this.ispitPredmet = data;
      })
      
    })

    this._ispitService.getPrijave(this.ispitId).subscribe(data => {
      this.prijave = data;
    })
  }

  open(content) {
    this.modalService.open(content, {size: 'lg'}).result.then((result) => {
      
    }, (reason) => {
      
    });
  }

  onOceni(){
    
    this.polaganjePost.ukupniBodovi = this.polaganjePost.predispitniBodovi + this.polaganjePost.ispitniBodovi;
    if(this.polaganjePost.ukupniBodovi < 51){
      this.polaganjePost.ocena = 5;
    } else if(this.polaganjePost.ukupniBodovi >= 51 && this.polaganjePost.ukupniBodovi < 61){
      this.polaganjePost.ocena = 6;
    } else if(this.polaganjePost.ukupniBodovi >= 61 && this.polaganjePost.ukupniBodovi < 71) {
      this.polaganjePost.ocena = 7;
    } else if(this.polaganjePost.ukupniBodovi >= 71 && this.polaganjePost.ukupniBodovi < 81) {
      this.polaganjePost.ocena = 8;
    } else if(this.polaganjePost.ukupniBodovi >= 81 && this.polaganjePost.ukupniBodovi < 91) {
      this.polaganjePost.ocena = 9;
    } else{
      this.polaganjePost.ocena = 10;
    }

    if(this.polaganjePost.ocena > 5){
      this.polaganjePost.polozio = true;
    } else {
      this.polaganjePost.polozio = false;
    }


    this._polaganjeService.addPolaganje(this.polaganjePost).subscribe(() => {
      this._ispitService.getPrijave(this.ispitId).subscribe(data => {
        this.prijave = data;
      })
    });


  }

  

  onBack(){
    this._location.back();
  }

  oceniModalOpened(id: number){
    this.polaganjePost.prijava = this.prijave[id].id;
  }

}
