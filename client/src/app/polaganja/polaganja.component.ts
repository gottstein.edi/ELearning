import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../security/authentication.service';
import { Location } from '@angular/common';
import { PolaganjeStudentaInterface } from '../model/polaganjeStudent.model';
import { PolaganjeService } from '../services/polaganje.service';

@Component({
  selector: 'app-polaganja',
  templateUrl: './polaganja.component.html',
  styleUrls: ['./polaganja.component.css']
})
export class PolaganjaComponent implements OnInit {

  constructor(private _authenticationService: AuthenticationService, private location:Location, private _polaganjeService: PolaganjeService) { }

  polaganjaStudenta: PolaganjeStudentaInterface[] = [];
  studentId: number;

  ngOnInit() {

    this.studentId = this._authenticationService.loggedInUserId();
    this._polaganjeService.getPolaganjeByStudent(this.studentId).subscribe(data => {
      this.polaganjaStudenta = data;
    })

    
  }

  onBack(){
    this.location.back();
  }

}
